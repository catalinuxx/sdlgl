#include "Util.h"
#include "FrameBufferObject.h"
#include "glInfo.h"
#include <assert.h>

FrameBufferObject::FrameBufferObject()
{
    m_nFrameBufferHandle = 0;
    m_nDepthBufferHandle = 0;
    m_nTexId = 0;
    m_nWidth = 0;
    m_nHeight = 0;
}

void FrameBufferObject::Destroy()
{
    if (m_nTexId)
    {
        GL_CHECK(glDeleteTextures(1, &m_nTexId));
        GL_CHECK(glDeleteFramebuffers(1, &m_nFrameBufferHandle));
        if (m_bUseDepthBuffer)
            GL_CHECK(glDeleteRenderbuffers(1, &m_nDepthBufferHandle));

        m_nTexId = 0;
        m_nWidth = 0;
        m_nHeight = 0;
    }
}

void FrameBufferObject::Begin(GLuint nFace) const
{
    assert(nFace < 6);
    GL_CHECK(glViewport(0, 0, m_nWidth, m_nHeight));

    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_nFrameBufferHandle));

    if (m_eTextureType == GL_TEXTURE_CUBE_MAP)
    {
        GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, m_eAttachment, GL_TEXTURE_CUBE_MAP_POSITIVE_X + nFace, m_nTexId, 0));
    }
}

void FrameBufferObject::End(GLuint nFace) const
{
    assert(nFace < 6);
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

void FrameBufferObject::Bind(GLint unit) const
{
    GL_CHECK(glActiveTexture(GL_TEXTURE0 + unit));
    //GL_CHECK(glEnable(m_eTextureType));
    GL_CHECK(glBindTexture(m_eTextureType, m_nTexId));
}

void FrameBufferObject::Unbind(GLint unit) const
{
    GL_CHECK(glActiveTexture(GL_TEXTURE0 + unit));
    GL_CHECK(glBindTexture(m_eTextureType, 0));
    //GL_CHECK(glDisable(m_eTextureType));
}


bool FrameBufferObject::Create(FBO_TYPE type, GLuint width, GLuint height, const std::string &name)
{
    Destroy();

#ifdef HAVE_GLES
    if (type == FBO_2D_DEPTH)
        type = FBO_2D_COLOR;
#endif // HAVE_GLES

    _name = name;
    m_nWidth = width;
    m_nHeight = height;
    //m_bUseFBO = true;//glInfo::get().isExtensionSupported("GL_EXT_framebuffer_object");
    m_bUseDepthBuffer = true;
    m_eTextureType = type == FBO_CUBE_COLOR ? GL_TEXTURE_CUBE_MAP : GL_TEXTURE_2D;

    if (type == FBO_2D_DEPTH)
        type = FBO_CUBE_COLOR;

    // Texture
    GL_CHECK(glGenTextures(1, &m_nTexId));
    GL_CHECK(glBindTexture(m_eTextureType, m_nTexId));
    GL_CHECK(glTexParameterf(m_eTextureType, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    GL_CHECK(glTexParameterf(m_eTextureType, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    GL_CHECK(glTexParameterf(m_eTextureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    GL_CHECK(glTexParameterf(m_eTextureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));

//	if(type==FBO_2D_DEPTH)
//		glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);

    GLenum eTarget;
    GLuint nFrames;
    if (type != FBO_CUBE_COLOR)
    {
        eTarget = GL_TEXTURE_2D;
        nFrames = 1;
    }
    else
    {
        eTarget = GL_TEXTURE_CUBE_MAP_POSITIVE_X;
        nFrames = 6;
    }

    for (GLuint i = 0; i < nFrames; i++)
    {
        if (type == FBO_2D_DEPTH)
        {
            GL_CHECK(glTexImage2D(eTarget + i, 0, GL_DEPTH_COMPONENT24, m_nWidth, m_nHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0));
            if (Util::get().LastGLError())
            {
                LOG_WARN("%s: FBO_2D_DEPTH glTexImage2D failed. width: %d, height: %d. i: %d", _name.c_str(), m_nWidth, m_nHeight, i);
                exit(1);
            }
        }
        else
        {
            GL_CHECK(glTexImage2D(eTarget + i, 0, GL_RGB, m_nWidth, m_nHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0));
        }
    }

    // Frame buffer
    GL_CHECK(glGenFramebuffers(1, &m_nFrameBufferHandle));
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_nFrameBufferHandle));

    if (m_bUseDepthBuffer)
    {
        // Depth buffer
        GL_CHECK(glGenRenderbuffers(1, &m_nDepthBufferHandle));
        GL_CHECK(glBindRenderbuffer(GL_RENDERBUFFER, m_nDepthBufferHandle));
        GL_CHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, m_nWidth, m_nHeight));
        if (Util::get().LastGLError())
        {
            LOG_WARN("%s: glRenderbufferStorage failed. width: %d, height: %d", _name.c_str(), m_nWidth, m_nHeight);
            exit(1);
        }

        // Display the frame with depth buffer
        GL_CHECK(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_nDepthBufferHandle));
    }


    //  attach the framebuffer to our texture, which may be a depth texture
    if (type == FBO_2D_DEPTH)
    {
        m_eAttachment = GL_DEPTH_ATTACHMENT;
        GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, eTarget, m_nTexId, 0));
    }
    else
    {
        m_eAttachment = GL_COLOR_ATTACHMENT0;
        GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, eTarget, m_nTexId, 0));
    }

    if (!CheckStatus())
        return false;
    GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));

    return true;
}

bool FrameBufferObject::CheckStatus()
{
    // check FBO status
    GLenum status = 0;
    GL_CHECK(status = glCheckFramebufferStatus(GL_FRAMEBUFFER));
    switch (status)
    {
    case GL_FRAMEBUFFER_COMPLETE:
        LOG_INFO("%s: Framebuffer complete.", _name.c_str());
        return true;

    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        LOG_WARN("%s: [ERROR] Framebuffer incomplete: Attachment is NOT complete.", _name.c_str());
        return false;

    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        LOG_WARN("%s: [ERROR] Framebuffer incomplete: No image is attached to FBO.", _name.c_str());
        return false;

#if 0
//#ifndef HAVE_GLES
    case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
        std::cerr << "[ERROR] Framebuffer incomplete: Attached images have different dimensions." << std::endl;
        return false;

    case GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
        std::cerr << "[ERROR] Framebuffer incomplete: Color attached images have different internal formats." << std::endl;
        return false;

    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
        std::cerr << "[ERROR] Framebuffer incomplete: Draw buffer." << std::endl;
        return false;

    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
        std::cerr << "[ERROR] Framebuffer incomplete: Read buffer." << std::endl;
        return false;
#endif

    case GL_FRAMEBUFFER_UNSUPPORTED:
        LOG_WARN("%s: [ERROR] Unsupported by FBO implementation.", _name.c_str());
        return false;

    default:
        LOG_WARN("%s: [ERROR] Unknow error. Status: %d", _name.c_str(), status);
        return false;
    }
}
