#pragma once
#include "Util.h"
#include <iostream>
#include "Mathlib.h"
#include "Singleton.h"


class Inputs  : public Singleton<Inputs>
{
public:
    // Function callbacks
    void MouseFunc(int button, int state, int x, int y);
    void MotionFunc(int x, int y);
    void PassiveMotionFunc(int x, int y);
    void KeyboardFunc(unsigned char key);
    void KeyboardUpFunc(unsigned char key);
    void SpecialFunc(int key);
    void SpecialUpFunc(int key);

    inline ivec2&		MousePos()
    {
        return vMouse;
    }
    inline GLboolean&	MouseButton(int button)
    {
        return tMouse[button];
    }
    inline GLboolean&	Key(unsigned char key)
    {
        return tBoard[key];
    }
    inline GLboolean&	SKey(int key)
    {
        return tSpecial[key];
    }

private:
    friend class Singleton<Inputs>;
    Inputs();

    ivec2 vMouse;				// position de la souris
    GLboolean tMouse[3];		// �tats de la souris
    GLboolean tBoard[256];		// �tats des touches clavier
    GLboolean tSpecial[256];	// �tats des commandes sp�ciales

};
