#include "Util.h"
#include "VarManager.h"
#include "ImageTools.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <assert.h>

#include <SDL_image.h>

namespace ImageTools
{

GLubyte* OpenImagePPM(const std::string& filename, unsigned int& w, unsigned int& h, unsigned int& d)
{

    char head[70];
    int i = 0;
    GLubyte * img = NULL;

    FILE * f = fopen(filename.c_str(), "rb");

    if (f == NULL)
    {
        return 0;
    }
    fgets(head, 70, f);

    if (!strncmp(head, "P6", 2))
    {
        i = 0;
        while (i < 3)
        {
            fgets(head, 70, f);

            if (head[0] == '#')
            {
                continue;
            }

            if (i == 0)
                i += sscanf(head, "%d %d %d", &w, &h, &d);
            else if (i == 1)
                i += sscanf(head, "%d %d", &h, &d);
            else if (i == 2)
                i += sscanf(head, "%d", &d);
        }

        img = new GLubyte[(size_t)(w) * (size_t)(h) * 3];
        if (img == NULL)
        {
            fclose(f);
            return 0;
        }

        fread(img, sizeof(GLubyte), (size_t)w * (size_t)h * 3, f);
        fclose(f);
    }
    else
    {
        fclose(f);
    }

    return img;
}

inline bool IsSDLFileSupported(const std::string &filename)
{
    VarManager &var = VarManager::get();
    for (auto ext : Util::Split(var.gets("sdl_img_extensions")))
         if (filename.rfind(ext) != std::string::npos)
            return true;
    return false;
}

GLubyte* OpenImage(const std::string& filename, unsigned int& w, unsigned int& h, unsigned int& d)
{
    VarManager &var = VarManager::get();
    if (filename.rfind(".ppm") != std::string::npos)
        return ImageTools::OpenImagePPM(filename, w, h, d);
    else if (var.getb("sdl_img_enabled") && IsSDLFileSupported(filename))
        return ImageTools::OpenImageSDL(filename, w, h, d);
    std::cerr << "WARN: Cannot load image " << filename << std::endl;
    return nullptr;
}

void OpenImage(const std::string& filename, ImageData& img)
{
    img.data = OpenImage(filename, img.w, img.h, img.d);
}

ImageData::~ImageData()
{
    if (data)
    {
        delete [] data;
        data = NULL;
    }
}

ivec3 ImageData::getColor(unsigned int x, unsigned int y) const
{
    int idx = (y * w + x) * d;
    return ivec3(data[idx + 0], data[idx + 1], data[idx + 2]);
}

GLubyte* OpenImageSDL(const std::string& filename, unsigned int& w, unsigned int& h, unsigned int& d)
{
    SDL_Surface* surface = NULL;

    if (filename.rfind(".tga") != std::string::npos) // tga not supported by SDL_RWFromFile
    {
        surface = IMG_Load(filename.c_str());
    }
    else
    {
        SDL_RWops *io = SDL_RWFromFile(filename.c_str(), "rb");
        if (!io)
        {
            LOG_WARN("Could not open file %s using SDL_RWFromFile", filename.c_str());
            return nullptr;
        }
        surface = IMG_Load_RW(io, 1);
    }
    surface = Util::ConvertSDLSurface(surface);
    if (!surface)
    {
        LOG_WARN("Could not open SDL image %s", filename.c_str());
        return nullptr;
    }

    w = surface->w;
    h = surface->h;
    d = surface->format->BytesPerPixel;
    LOG_INFO("Loaded SDL image %s w:%d h:%d d:%d", filename.c_str(), w, h, surface->format->BytesPerPixel);
    auto size = w * h * d;
    GLubyte *img = new GLubyte[size];
    SDL_LockSurface(surface);
    memset(img, 0, size);
    memcpy(img, surface->pixels, size);
    SDL_UnlockSurface(surface);
    SDL_FreeSurface(surface);
    return img;
}

}
