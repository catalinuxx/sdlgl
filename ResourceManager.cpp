#include "Util.h"
#include "ResourceManager.h"

void ResourceManager::Init()
{
    _defaultFont = dynamic_cast<Font *>(LoadResource(ResourceManager::FONT, "arial.ttf"));
    _defaultShader = dynamic_cast<Shader *>(LoadResource(ResourceManager::SHADER, "simple"));
}

void ResourceManager::Clear()
{
    if (m_ResDB.size())
    {
        for (std::map<std::string, IResource *>::iterator it = m_ResDB.begin(); it != m_ResDB.end(); it++)
            if (it->second)
            {
                delete it->second;
                it->second = NULL;
            }
        m_ResDB.clear();
        LOG_INFO("Done deleting resources");
    }
}

ResourceManager::~ResourceManager()
{
    Clear();
}

IResource* ResourceManager::LoadResource(RES_TYPE type, const std::string& name, GLenum usage)
{
    // La ressource est d�ja charg�e, on ne la recharge pas :
    if (m_ResDB.find(name) != m_ResDB.end())
        return m_ResDB.find(name)->second;

    bool isFilename = Util::IsFilename(name);

    // on charge la ressource
    IResource* ptr = NULL;
    switch (type)
    {
    case TEXTURE2D:
        LOG_INFO("Loading Texture2D resource %s\n", name.c_str());
        ptr = new Texture2D();
        if (!((Texture2D*)ptr)->Load(isFilename ? "textures/" + name : name))
        {
            delete ptr;
            ptr = nullptr;
        }
        break;
    case TEXTURECUBEMAP:
        LOG_INFO("Loading TextureCubeMap resource %s\n", name.c_str());
        ptr = new TextureCubemap();
        if (!((TextureCubemap*)ptr)->Load(name, "textures/"))
        {
            delete ptr;
            ptr = nullptr;
        }
        break;
    case SHADER:
        LOG_INFO("Loading shader resource %s\n", name.c_str());
        ptr = new Shader();
        if (!((Shader*)ptr)->Load(isFilename ? "shaders/" + name : name))
        {
            delete ptr;
            ptr = nullptr;
        }
        break;
    case MESH:
        LOG_INFO("Loading mesh resource %s\n", name.c_str());
        ptr = new Mesh();
        if (!((Mesh*)ptr)->Load(isFilename ? "meshs/" + name : name, usage))
        {
            delete ptr;
            ptr = nullptr;
        }
        break;
    case FONT:
        LOG_INFO("Loading font resource %s\n", name.c_str());
        ptr = new Font();
        if (!((Font*)ptr)->Load(isFilename ? "fonts/" + name : name))
        {
            delete ptr;
            ptr = nullptr;
        }
        break;
    }

    if (!ptr)
    {
        std::cerr << "WARN: Could not load resource " << name << std::endl;
        LOG_WARN("WARN: Could not load resource %s\n", name.c_str());
        return nullptr;
    }

    m_ResDB[name] = ptr;
    return ptr;
}

IResource* ResourceManager::NewResource(IResource* data, const std::string& name)
{
    if (!data)
        return NULL;

    // La ressource est d�ja charg�e, on ne la recharge pas :
    if (m_ResDB.find(name) != m_ResDB.end())
        return m_ResDB.find(name)->second;

    m_ResDB[name] = data;
    return data;
}
