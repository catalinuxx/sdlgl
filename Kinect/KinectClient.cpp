#include <thread>
#include <fstream>
#include <iomanip>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

#include "VarManager.h"
#include "Timer.h"

#include <iostream>
#include "KinectClient.h"

#ifndef ANDROID
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#else // Windows
#define SOCKET int
#define SOCKADDR_IN sockaddr_in
#define SOCKADDR sockaddr
//#define FD_SET fd_set
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#endif // ANDROID

KinectClient::KinectClient() : KinectInterface()
{
}

KinectClient::~KinectClient()
{
    if (_threadStarted)
    {
        _terminate = true;
        LOG_INFO("Waiting for thread to finish ...");
        _thread.join();
        LOG_INFO("Done.");
    }

    Disconnect();

    if (_parseBuffer)
    {
        delete [] _parseBuffer;
        _parseBuffer = nullptr;
    }
}

int KinectClient::Init()
{
    VarManager& var = VarManager::get();

    _server = var.gets("kinect_server", _server);
    _port = var.geti("kinect_server_port", _port);

    {
        auto &tPosition = _vbos[DepthKinectVBO].getPosition();
        auto &tColor = _vbos[DepthKinectVBO].getColor();
        tPosition.resize(640 * 480);
        tColor.resize(640 * 480);
        _vbos[DepthKinectVBO].Create(GL_DYNAMIC_DRAW);
    }
    for (int i = Skeleton1KinectVBO; i <= Skeleton2KinectVBO; ++i)
    {
        auto &tPosition = _vbos[i].getPosition();
        auto &tColor = _vbos[i].getColor();
        tPosition.resize(MaxJoints);
        tColor.resize(MaxJoints);
        _vbos[i].Create(GL_DYNAMIC_DRAW);
    }

    auto playFile = var.gets("kinect_device_uri");
    if (!playFile.empty())
    {
        _reader.reset(new Reader());
        if (!_reader->Open(playFile.c_str()))
            LOG_WARN("Could not open record file %s", playFile.c_str());
        else
            LOG_INFO("Playing kinect data from %s ...", playFile.c_str());
    }
    auto recFile = var.gets("kinect_rec_file");
    if (!recFile.empty())
    {
        _writer.reset(new Writer());
        if (!_writer->Create(recFile.c_str()))
            LOG_WARN("Could not record to file %s", recFile.c_str());
        else
            LOG_INFO("Recording kinect data to %s ...", recFile.c_str());
    }

    _thread = std::thread(&KinectClient::ThreadProc, this);

    return 0;
}

void KinectClient::ThreadProc()
{
    _threadStarted = true;

    while (!_terminate)
    {
        if (!_connected && !(_connected = Connect()))
        {
            LOG_WARN("Could not connect ... Retrying in 10 seconds ... tries:%d", _connectReTries);
            for (int i = 0; i < 10 && !_terminate; i++) // Sleep for 10 seconds by checking the terminate flag
                Util::SleepMS(1000);
            _connectReTries++;
            continue;
        }

        if (!_reader)
            SocketRead();
        else
            RecorderRead();
    }

    _threadStarted = false;
}

std::vector<VertexBufferObject> &KinectClient::GetVBOs()
{
    return _vbos;
}
std::mutex &KinectClient::GetLock()
{
    return _lock;
}

/*
 *
 * {TYPE(1 char)} {LEN (int)} {DATA of size LEN}
 */
void KinectClient::ParseHeader(char *buff)
{
    assert(_packetType == NoPacket);
    switch (*buff)
    {
    case ColorPacket:
    case DepthPacket:
    case SkeletonPacket:
    case AccelerometerPacket:
        _packetType = (PacketType)*buff;
        break;
    default:
        LOG_WARN("Received unknown packet type %d", *buff);
        _packetType = UnknownPacket;
        throw std::runtime_error("Received unknown packet type");
        break;
    }
    buff += sizeof(char);

    _packetSeconds = *(unsigned int *)(buff);
    buff += sizeof(unsigned int);

    _packetMilliSeconds = *(unsigned int *)(buff);
    buff += sizeof(unsigned int);

    int packetLen = *(int *)(buff);
    buff += sizeof(int);
    if (packetLen <= 0)
    {
        LOG_WARN("Received wrong packet size %d", packetLen);
        assert(_packetLen > 0);
        throw std::runtime_error("Received packet if negative or zero size");
    }
    _packetLen = packetLen;

    static auto lastLog = Timer::get().getCurrentTime();
    if (Timer::get().getCurrentTime() - lastLog > 4)
    {
        LOG_INFO("Received packet header '%c' of size %d. seconds:%d milli:%d", _packetType, _packetLen, _packetSeconds, _packetMilliSeconds);
        lastLog = Timer::get().getCurrentTime();
    }

    if (_reader) // Do not deliver frames at full speed
    {
        static int prevSecs = (int)_packetSeconds;
        static int prevMs = (int)_packetMilliSeconds;
        if ((int)_packetSeconds > prevSecs || (int)_packetMilliSeconds > prevMs)
        {
            auto ms = (_packetSeconds - prevSecs) * 1000 + (_packetMilliSeconds - prevMs);
            if (ms <= 10)
                Util::SleepMS(ms);
            else
            {
                // Sleep 10 ms part by checking the exit flag
                for (unsigned int i = 0; i < ms / 10 && !_terminate; i++)
                    Util::SleepMS(10);
                if (ms % 10)
                    Util::SleepMS(ms % 10);
            }
        }
        prevSecs = _packetSeconds;
        prevMs = _packetMilliSeconds;
    }

    // Realloc if needed
    ReallocParseBuffer(_packetLen);
}

void KinectClient::ParseBuffer(char *buff, int len)
{
    if (_packetType == NoPacket)
    {
        if (_parseLen < HeaderSize)
        {
            // Append HeaderSize - parseLen
            int toAppend = std::min(len, HeaderSize - _parseLen);
            memcpy(_parseBuffer + _parseLen, buff, toAppend);
            _parseLen += toAppend;
            len -= toAppend;
            buff += toAppend;

            if (_parseLen >= HeaderSize)
            {
                ParseHeader(_parseBuffer);
                _parseLen = 0;
            }
            else
            {
                //LOG_INFO("Waiting enough data to be received (at least header size). parseLen: %d, Remaining %d ... %d", _parseLen, HeaderSize - _parseLen, *_parseBuffer);
                return; // Wait for more data
            }
        }
    }

    assert(_packetType != NoPacket);
    while (len > 0 && !_terminate) // While we have data to consume
    {
        int toAppend = std::min(len, _packetLen);
        assert(_parseLen + toAppend <= _parseBufferSize);
        memcpy(_parseBuffer + _parseLen, buff, toAppend);
        len -= toAppend;
        buff += toAppend;
        _parseLen += toAppend;

        _packetLen -= toAppend;

        if (!_packetLen) // Finished
        {
            DecodeBuffer(_packetType, _parseBuffer, _parseLen);
            //LOG_INFO("Finished processing packet type '%c'", _packetType);
            _parseLen = 0;
            _packetType = NoPacket;

            if (len > 0)
            {
                //LOG_INFO("Recur len %d ... %d", len, *buff);
                ParseBuffer(buff, len);
            }
            return;
        }
    }
}

void KinectClient::ReallocParseBuffer(int size)
{
    if (size >= MaxParsePacketSize)
    {
        LOG_WARN("Packet type '%c'  has size %d bigger than maximum %d", _packetType, size, MaxParsePacketSize);
        assert(size < MaxParsePacketSize);
        throw std::runtime_error(std::string("Packet type '") + (char)_packetType + "' has length bigger than maximum ");
    }
    if (_parseBuffer == nullptr || _parseBufferSize < size)
    {
        if (_parseBuffer)
            delete [] _parseBuffer;
        _parseBuffer = new char[size];
        if (_parseBuffer == nullptr)
            throw std::runtime_error("Out of memory!");
        LOG_INFO("Parse buffer size adjusted to %d from %d", size, _parseBufferSize);
        _parseBufferSize = size;
    }
}

void KinectClient::DecodeBuffer(PacketType packetType, char *buff, int len)
{
    //std::lock_guard<std::mutex> lock(_lock);
    std::size_t numPoints = 0;

    switch (packetType)
    {
    case ColorPacket:
        break;
    case DepthPacket:
    {
        auto &tPosition = _vbos[DepthKinectVBO].getPosition();
        auto &tColor = _vbos[DepthKinectVBO].getColor();
        short *ptr = (short *)buff;
        for (int i = 0; i + 3 < len / (int)sizeof(short); i += 3)
        {
            assert(numPoints < tPosition.size());
            tPosition[numPoints].set(ptr[i], ptr[i+1], ptr[i+2]);
            tColor[numPoints].set(0,1,0,1);
            numPoints++;
            //LOG_INFO("x:%d y:%d z:%d", ptr[i], ptr[i+1], ptr[i+2]);
        }
        _vbos[DepthKinectVBO].NumPoints() = numPoints;
        break;
    }
    case SkeletonPacket:
    {
        int *ptr = (int *)buff;
        int id = *ptr; ptr++;
        int type = *ptr; ptr++;

        if (id >= 0 && (std::size_t)id < _skeletons.size())
        {
            // Convert skeleton id to vbos index
            int vboID = Skeleton1KinectVBO + id;

            auto &tPosition = _vbos[vboID].getPosition();
            auto &tColor = _vbos[vboID].getColor();

            if (type == 'T') // Tracked
            {
                for (int i = 0; i + 4 < len / (int)sizeof(int); i += 4)
                {
                    JointType jointType = (JointType)ptr[i + 0];

                    assert(numPoints < tPosition.size());
                    tPosition[numPoints].set(
                        (float)ptr[i + 1] / 100,
                        (float)ptr[i + 2] / 100,
                        (float)ptr[i + 3] / 100);

                    _skeletons[id].SetJoint(jointType, vec3(
                        (float)ptr[i + 1] / 100,
                        (float)ptr[i + 2] / 100,
                        (float)ptr[i + 3] / 100));

                    switch (jointType)
                    {
                    case Head:
                        tColor[numPoints].set(1,0,0,1);
                        break;
                    case FootLeft:
                    case FootRight:
                        tColor[numPoints].set(0,1,0,1);
                        break;
                    case HandLeft:
                    case HandRight:
                        tColor[numPoints].set(0,1,1,1);
                        break;
                    case ShoulderRight:
                    case ShoulderLeft:
                        tColor[numPoints].set(0.5,0.5,0.5,1);
                        break;
                    default:
                        tColor[numPoints].set(0,0,1,1);
                        break;
                    }

                    numPoints++;

                    //LOG_INFO("TRACKED: id:%d type:%d jointType:%d x:%d y:%d z:%d", id, type, jointType, ptr[i + 1], ptr[i + 2], ptr[i + 3]);
                }
                _vbos[vboID].NumPoints() = numPoints;
            }
            else if (type == 'P') // Position
            {
                _skeletons[id].SetPosition(vec3(ptr[0], ptr[1], ptr[2]));
                LOG_INFO("POSITION: id:%d type:%d x:%d y:%d z:%d", id, type, ptr[0], ptr[1], ptr[2]);
            }
        }
        break;
    }
    case AccelerometerPacket:
        break;
    case UnknownPacket: // Do nothing
        break;
    }
}

void KinectClient::Disconnect()
{
    if (_sock >= 0)
    {
#ifndef ANDROID
        closesocket(_sock); //Shut down socket
#else
        close (_sock);
#endif
        _sock = -1;
    }
    _connected = false;
}

bool KinectClient::Connect()
{
    if (_reader)
        return true;

    SOCKADDR_IN target; //Information about host

    // Reset parsing ...
    _packetType = NoPacket;
    _packetLen = 0;
    _packetSeconds = 0;
    _packetMilliSeconds = 0;
    _parseLen = 0;

    // Close existing connection
    Disconnect();

#ifndef ANDROID
    WSADATA w; //Winsock startup info
    int error = WSAStartup (0x0202, &w);   // Fill in WSA info
    if (error)
    {
        // there was an error
        LOG_WARN("WSAStartup failed with %d", error);
        return false;
    }
    if (w.wVersion != 0x0202)
    {
        LOG_WARN("wrong WinSock version!");
        WSACleanup (); // unload ws2_32.dll
        return false;
    }
#endif // ANDROID

    struct hostent *host = gethostbyname(_server.c_str());
    if (!host)
    {
        LOG_WARN("ERROR, no such host %s", _server.c_str());
        return false;
    }
    _sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); // Create socket
#ifndef ANDROID
    if (_sock == INVALID_SOCKET)
#else
    if (_sock < 0)
#endif
    {
        LOG_WARN("Invalid TCP socket!");
        return false;
    }

    target.sin_family = AF_INET;           // address family Internet
    target.sin_port = htons(_port);        // set server�s port number
    memcpy(&target.sin_addr.s_addr, host->h_addr, host->h_length); // set server�s IP

    //Try connecting...
    if (connect(_sock, (SOCKADDR *)&target, sizeof(target)) < 0) //Try binding
    {
        LOG_WARN("Could not connect to %s:%d", _server.c_str(), _port);
        return false;
    }

    LOG_INFO("Successfully connected to %s:%d", _server.c_str(), _port);
    return true;
}

const std::string KinectClient::GetStats()
{
    if (_connected)
        return "Connected";
    return "Connecting ... ";
}

void KinectClient::SocketRead()
{
    fd_set WriteSet;
    fd_set ReadSet;

    // We should be connected here
    assert(_sock >= 0);

    // Prepare the Read and Write socket sets for network I/O notification
    FD_ZERO(&ReadSet);
    FD_ZERO(&WriteSet);

    // Always look for connection attempts
    FD_SET(_sock, &ReadSet);
    FD_SET(_sock, &WriteSet);

    int total = 0;
    if ((total = select(1, &ReadSet, &WriteSet, NULL, NULL)) < 0)
    {
        LOG_WARN("select() returned with error %d", total);
        return;
    }

    // Check for arriving connections on the listening socket.
    if (FD_ISSET(_sock, &ReadSet))
    {
        total--;

        int recvRet = 0;
#ifndef ANDROID
        WSABUF buff;
        buff.buf = _buff;
        buff.len = BufferSize;

        long unsigned int flags = 0;
        long unsigned int recvBytes = 0;
        if ((recvRet = WSARecv(_sock, &buff, 1, &recvBytes, &flags, NULL, NULL)) == SOCKET_ERROR)
        {
            if (WSAGetLastError() != WSAEWOULDBLOCK)
#else
        int recvBytes = 0;
        if ((recvBytes = read(_sock, _buff, BufferSize)) < 0)
        {
            if (recvBytes == EAGAIN)
#endif
            {
                LOG_WARN("read failed with error %d", recvBytes);
                _connected = false;
            }
        }
        else
        {
            if (!recvBytes)
            {
                LOG_INFO("zero bytes received ... Reconnecting");
                _connected = false;
            }
            else
            {
                //int *ptr = (int *)(_buff+1);
                //LOG_INFO("read is OK! recvBytes:%d ... %d %d %d %d %d", recvBytes, _buff[0], ptr[0], ptr[1], ptr[2], ptr[3]);

                if (_writer)
                    _writer->Write(_buff, recvBytes);

                ParseBuffer(_buff, recvBytes);
            }
        }
    }
}

void KinectClient::RecorderRead()
{
    assert(_reader);
    int readRet = _reader->Read(_buff, BufferSize);
    if (readRet > 0)
    {
        ParseBuffer(_buff, readRet);
        if (_writer)
            _writer->Write(_buff, readRet);
    }
    else if (readRet == 0)
    {
        LOG_INFO("Reached the end of the playback recording file, rewinding ...");
        _reader->Rewind();
    }
    else
        LOG_WARN("Error reading from recording file. err:%d", readRet);
}
