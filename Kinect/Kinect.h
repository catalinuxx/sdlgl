#pragma once

#include <thread>
#include <mutex>

#include <stdio.h>
#include "Kinect.h"

#include "KinectClient.h"
#ifndef ANDROID
#include "KinectOpenNI.h"
#endif // ANDROID

class Kinect
{
public:
    Kinect();
    virtual ~Kinect();

    int Init();
    std::vector<VertexBufferObject> &GetVBOs();
    std::vector<Skeleton> &GetSkeletons();
    std::mutex &GetLock();
    std::string GetStats();

private:
    std::unique_ptr<KinectInterface> _client;
};
