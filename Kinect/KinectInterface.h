#pragma once

#include <vector>
#include <mutex>

#include "Util.h"
#include "Skeleton.h"

enum KinectVBOTypes
{
    DepthKinectVBO,
    Skeleton1KinectVBO,
    Skeleton2KinectVBO,
    KinectMaxVBO,
};

enum PacketType
{
    NoPacket = 0,
    UnknownPacket = 0,
    ColorPacket = 'C',
    DepthPacket = 'D',
    SkeletonPacket = 'S',
    AccelerometerPacket = 'A',
};

class KinectInterface
{
public:
    KinectInterface() {}
    virtual ~KinectInterface() {}

    virtual int Init() = 0;
    virtual std::vector<VertexBufferObject> &GetVBOs() = 0;
    virtual std::vector<Skeleton> &GetSkeletons() = 0;
    virtual std::mutex &GetLock() = 0;
    virtual const std::string GetStats() = 0;
};
