#include <thread>
#include <fstream>

#include "VarManager.h"

#include <iostream>
#include "Kinect.h"

Kinect::Kinect()
{
}

Kinect::~Kinect()
{
}

int Kinect::Init()
{
    VarManager& var = VarManager::get();

    std::string type = var.gets("kinect_type", "tcpip");

#ifndef ANDROID
    if (type == "openni")
        _client.reset(new KinectOpenNI());
    else
#endif // ANDROID
    if (type == "tcpip")
        _client.reset(new KinectClient());

    if (!_client)
        throw std::runtime_error("Could not create any kinect client instance");

    return _client->Init();
}

std::vector<VertexBufferObject> &Kinect::GetVBOs()
{
    assert(_client);
    return _client->GetVBOs();
}

std::vector<Skeleton> &Kinect::GetSkeletons()
{
    assert(_client);
    return _client->GetSkeletons();
}

std::mutex &Kinect::GetLock()
{
    assert(_client);
    return _client->GetLock();
}

std::string Kinect::GetStats()
{
    assert(_client);
    return _client->GetStats();
}
