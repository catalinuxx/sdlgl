#pragma once

#include "KinectInterface.h"

#include <thread>

#include <stdio.h>
#include <windows.h>
#define RC_INVOKED
#include <OpenNI.h>

class KinectOpenNI : public KinectInterface
{
public:
    KinectOpenNI();
    virtual ~KinectOpenNI();

    int Init();
    std::vector<VertexBufferObject> &GetVBOs();
    std::vector<Skeleton> &GetSkeletons() { return _skeletons; }
    std::mutex &GetLock();
    const std::string GetStats() { return ""; };

    enum DisplayModes
    {
        DISPLAY_MODE_OVERLAY,
        DISPLAY_MODE_DEPTH,
        DISPLAY_MODE_IMAGE
    };

private:
    void ThreadProc();

    openni::VideoStream** _streams = nullptr;

    openni::Device _device;
    openni::VideoStream _depthStream;
    openni::VideoStream _colorStream;
    openni::VideoFrameRef _depthFrame;
    openni::VideoFrameRef _colorFrame;
    openni::Recorder _recorder;

    int _numStreams = 0;

    int	_width = 0;
    int	_height = 0;

    std::thread _thread;
    std::mutex _lock;
    bool _threadStarted = false;
    volatile bool _terminate = false;

    std::vector<VertexBufferObject> _vbos = std::vector<VertexBufferObject>(1);
    std::vector<Skeleton> _skeletons;
};
