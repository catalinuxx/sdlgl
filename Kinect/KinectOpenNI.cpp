#include <thread>
#include <fstream>
#include <iomanip>

#include "VarManager.h"

#include <iostream>
#include "KinectOpenNI.h"

#define MAX_DEPTH 320*240

//float			_pDepthHist[MAX_DEPTH];
//char			_strSampleName[ONI_MAX_STR];
//unsigned int		_nTexMapX;
//unsigned int		_nTexMapY;
//DisplayModes		_eViewState;
//openni::RGB888Pixel*	_pTexMap;

KinectOpenNI::KinectOpenNI() : KinectInterface()
{
}

KinectOpenNI::~KinectOpenNI()
{
    if (_threadStarted)
    {
        _terminate = true;
        LOG_INFO("Waiting for thread to finish ...");
        _thread.join();
        LOG_INFO("Done.");
    }
    if (_streams)
        delete [] _streams;
    if (_recorder.isValid())
        _recorder.stop();
    //openni::OpenNI::shutdown();
}

int KinectOpenNI::Init()
{
    VarManager& var = VarManager::get();
    openni::Status rc = openni::STATUS_OK;

	const char* deviceURI = openni::ANY_DEVICE;
	if (!var.gets("kinect_device_uri").empty())
		deviceURI = var.gets("kinect_device_uri").c_str();

	rc = openni::OpenNI::initialize();

	LOG_INFO("After initialization: %s", openni::OpenNI::getExtendedError());

	rc = _device.open(deviceURI);
	if (rc != openni::STATUS_OK)
	{
		LOG_WARN("Device open failed: . rc:%d / %s", rc, openni::OpenNI::getExtendedError());
		openni::OpenNI::shutdown();
		return 1;
	}

	rc = _depthStream.create(_device, openni::SENSOR_DEPTH);
	if (rc == openni::STATUS_OK)
	{
		rc = _depthStream.start();
		if (rc != openni::STATUS_OK)
		{
			LOG_WARN("Couldn't start depth stream: rc: %d. err: %s", rc, openni::OpenNI::getExtendedError());
			_depthStream.destroy();
		}
	}
	else
	{
		LOG_WARN("Couldn't find depth stream: rc:%d / %s", rc, openni::OpenNI::getExtendedError());
	}

	rc = _colorStream.create(_device, openni::SENSOR_COLOR);
	if (rc == openni::STATUS_OK)
	{
		rc = _colorStream.start();
		if (rc != openni::STATUS_OK)
		{
			LOG_WARN("Couldn't start color stream: rc:%d / %s", rc, openni::OpenNI::getExtendedError());
			_colorStream.destroy();
		}
	}
	else
	{
		LOG_WARN("Couldn't find color stream: rc:%d / %s", rc, openni::OpenNI::getExtendedError());
	}

	auto recFile = var.gets("kinect_rec_file");
    if (!recFile.empty())
    {
        rc = _recorder.create(recFile.c_str());
        if (rc != openni::STATUS_OK)
        {
            LOG_WARN("Could not record to file %s: rc:%d / %s", rc, recFile.c_str(), openni::OpenNI::getExtendedError());
        }
        else
        {
            LOG_INFO("Recording kinect data to %s ...", recFile.c_str());
        }
    }

	if (!_depthStream.isValid() || !_colorStream.isValid())
	{
		LOG_WARN("WARN: No valid streams. Exiting");
		openni::OpenNI::shutdown();
		return 2;
	}

    openni::VideoMode depthVideoMode;
	openni::VideoMode colorVideoMode;

	if (_depthStream.isValid() && _colorStream.isValid())
	{
	    _numStreams = 2;
	    _streams = new openni::VideoStream*[_numStreams];
        _streams[0] = &_depthStream;
        _streams[1] = &_colorStream;

		depthVideoMode = _depthStream.getVideoMode();
		colorVideoMode = _colorStream.getVideoMode();

		int depthWidth = depthVideoMode.getResolutionX();
		int depthHeight = depthVideoMode.getResolutionY();
		int colorWidth = colorVideoMode.getResolutionX();
		int colorHeight = colorVideoMode.getResolutionY();

		if (depthWidth == colorWidth &&
			depthHeight == colorHeight)
		{
			_width = depthWidth;
			_height = depthHeight;

            if (_recorder.isValid())
            {
                _recorder.attach(_depthStream);
                _recorder.attach(_colorStream);
            }
		}
		else
		{
			LOG_WARN("WARN: Expected color and depth to be in same resolution: D: %dx%d, C: %dx%d",
				depthWidth, depthHeight,
				colorWidth, colorHeight);
			return openni::STATUS_ERROR;
		}
	}
	else if (_depthStream.isValid())
	{
	    _numStreams = 1;
	    _streams = new openni::VideoStream*[_numStreams];
	    _streams[0] = &_depthStream;

		depthVideoMode = _depthStream.getVideoMode();
		_width = depthVideoMode.getResolutionX();
		_height = depthVideoMode.getResolutionY();

        if (_recorder.isValid())
            _recorder.attach(_depthStream);
	}
	else if (_colorStream.isValid())
	{
	    _numStreams = 1;
	    _streams = new openni::VideoStream*[_numStreams];
        _streams[0] = &_colorStream;

		colorVideoMode = _colorStream.getVideoMode();
		_width = colorVideoMode.getResolutionX();
		_height = colorVideoMode.getResolutionY();

        if (_recorder.isValid())
            _recorder.attach(_colorStream);
	}
	else
	{
		LOG_WARN("WARN: Expects at least one of the streams to be valid...");
		return openni::STATUS_ERROR;
	}

	// Allocate depth pixels

	//auto size = std::min(_width * _height, MAX_DEPTH);
	auto size = _width * _height;

    _vbos[0].getPosition().resize(size);
    _vbos[0].getColor().resize(size);
    _vbos[0].Create(GL_DYNAMIC_DRAW);
    LOG_INFO("VBO initialized. size: %d, WH: %d, MAX: %d", size, _width * _height, MAX_DEPTH);

	// Texture map init
	//_nTexMapX = MIN_CHUNKS_SIZE(_width, TEXTURE_SIZE);
	//_nTexMapY = MIN_CHUNKS_SIZE(_height, TEXTURE_SIZE);
	//_pTexMap = new openni::RGB888Pixel[_nTexMapX * _nTexMapY];

    if (_recorder.isValid())
    {
        rc = _recorder.start();
        if (rc != openni::STATUS_OK)
        {
            LOG_WARN("Could not start recording to file %s: %s. rc: %d", recFile.c_str(), openni::OpenNI::getExtendedError(), rc);
        }
    }

	_thread = std::thread(&KinectOpenNI::ThreadProc, this);
	_threadStarted = true;

	LOG_INFO("Success! numStreams:%d W:%d H:%d", _numStreams, _width, _height);

    return 0;
}

void KinectOpenNI::ThreadProc()
{
    if (!_streams)
    {
        assert(_streams);
        return;
    }

    auto &tPosition = _vbos[0].getPosition();
    auto &tColor = _vbos[0].getColor();

	int changedIndex = 0;
	openni::CoordinateConverter cc;

    std::ofstream ofs("recordings/x");

	while (!_terminate)
    {
        openni::Status rc = openni::OpenNI::waitForAnyStream(_streams, _numStreams, &changedIndex);
        if (rc != openni::STATUS_OK)
        {
            LOG_WARN("WARN: Wait failed. rc: %d", rc);
            continue;
        }

        switch (changedIndex)
        {
        case 0:
            assert(_depthStream.isValid());
            _depthStream.readFrame(&_depthFrame);
            break;
        case 1:
            assert(_colorStream.isValid());
            _colorStream.readFrame(&_colorFrame);
            break;
        default:
            LOG_WARN("Error in wait");
            break;
        }

        if (changedIndex == 0) // depth
        {
            if (!_depthFrame.isValid())
            {
                LOG_WARN("WARN: Depth frame is not valid");
                continue;
            }
            const openni::DepthPixel* pDepthRow = (const openni::DepthPixel*)_depthFrame.getData();
            int rowSize = _depthFrame.getStrideInBytes() / sizeof(openni::DepthPixel);

            std::lock_guard<std::mutex> lock(_lock);
            std::size_t numPoints = 0;

            bool stop = false;
            int colorRowSize = 0;
            if (_colorFrame.isValid())
                colorRowSize = _colorFrame.getStrideInBytes() / sizeof(openni::RGB888Pixel);

            for (std::size_t y = 0; (int)y < _depthFrame.getHeight() && !_terminate && !stop; ++y)
            {
                const openni::DepthPixel* pDepth = pDepthRow;

                for (std::size_t x = 0; (int)x < _depthFrame.getWidth() && !_terminate && !stop; ++x, ++pDepth)
                {
                    if (*pDepth != 0)
                    {
                        float wX = 0.0, wY = 0.0, wZ = 0.0;
                        //wZ = 0.3 * (*pDepth);
                        //wX = x;
                        //wY = -y + _height;
                        rc = cc.convertDepthToWorld(_depthStream, x, y, *pDepth, &wX, &wY, &wZ);
                        if (rc != openni::STATUS_OK)
                        {
                            LOG_WARN("WARN: Could not convert depth pixel. rc: %d", rc);
                            continue;
                        }
                        if (numPoints >= tPosition.size())
                        {
                            stop = true;
                            break;
                        }

                        if (_colorFrame.isValid())
                        {
                            int colorX = x, colorY = y;
                            //rc = cc.convertDepthToColor(_depthStream, _colorStream, x, y, *pDepth, &colorX, &colorY);
                            if (rc != openni::STATUS_OK)
                            {
                                LOG_WARN("WARN: Could not convert depth to color. rc: %d", rc);
                                tColor[numPoints].set(1.0f, 0.0f, 0.0f, 1.0f); // red
                            }
                            else
                            {
                                const openni::RGB888Pixel* pImage = (const openni::RGB888Pixel*)_colorFrame.getData();
                                pImage += colorRowSize * colorY + colorX;
                                tColor[numPoints].set((float)pImage->r / 255, (float)pImage->g / 255, (float)pImage->b / 255, 1.0f);
                                //printf("r:%.2f g:%.2f b:%.2f\n", 255.0f / pImage->r, 255.0f / pImage->g, 255.0f / pImage->b);
                                //printf("r:%d g:%d b:%d\n", pImage->r, pImage->g, pImage->b);
                            }
                        }

                        //ofs << "[" << (int)wX << ", " << (int)wY << ", " << wZ << "]" << std::endl;
                        assert(numPoints < tPosition.size());
                        tPosition[numPoints].set(wX, wY, wZ);
                        numPoints++;
                    }
                }

                pDepthRow += rowSize;
            }
            _vbos[0].NumPoints() = numPoints;

            //printf("depth numPoints:%d size:%d\n", _numPoints, tPosition.size());
        } // depth frame
    }

	/*glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, GL_WIN_SIZE_X, GL_WIN_SIZE_Y, 0, -1.0, 1.0);

	if (_depthFrame.isValid())
	{
		calculateHistogram(_pDepthHist, MAX_DEPTH, _depthFrame);
	}

	memset(_pTexMap, 0, _nTexMapX*_nTexMapY*sizeof(openni::RGB888Pixel));

	// check if we need to draw image frame to texture
	if ((_eViewState == DISPLAY_MODE_OVERLAY ||
		_eViewState == DISPLAY_MODE_IMAGE) && _colorFrame.isValid())
	{
		const openni::RGB888Pixel* pImageRow = (const openni::RGB888Pixel*)_colorFrame.getData();
		openni::RGB888Pixel* pTexRow = _pTexMap + _colorFrame.getCropOriginY() * _nTexMapX;
		int rowSize = _colorFrame.getStrideInBytes() / sizeof(openni::RGB888Pixel);

		for (int y = 0; y < _colorFrame.getHeight(); ++y)
		{
			const openni::RGB888Pixel* pImage = pImageRow;
			openni::RGB888Pixel* pTex = pTexRow + _colorFrame.getCropOriginX();

			for (int x = 0; x < _colorFrame.getWidth(); ++x, ++pImage, ++pTex)
			{
				*pTex = *pImage;
			}

			pImageRow += rowSize;
			pTexRow += _nTexMapX;
		}
	}

	// check if we need to draw depth frame to texture
	if ((_eViewState == DISPLAY_MODE_OVERLAY ||
		_eViewState == DISPLAY_MODE_DEPTH) && _depthFrame.isValid())
	{
		const openni::DepthPixel* pDepthRow = (const openni::DepthPixel*)_depthFrame.getData();
		openni::RGB888Pixel* pTexRow = _pTexMap + _depthFrame.getCropOriginY() * _nTexMapX;
		int rowSize = _depthFrame.getStrideInBytes() / sizeof(openni::DepthPixel);

		for (int y = 0; y < _depthFrame.getHeight(); ++y)
		{
			const openni::DepthPixel* pDepth = pDepthRow;
			openni::RGB888Pixel* pTex = pTexRow + _depthFrame.getCropOriginX();

			for (int x = 0; x < _depthFrame.getWidth(); ++x, ++pDepth, ++pTex)
			{
				if (*pDepth != 0)
				{
					int nHistValue = _pDepthHist[*pDepth];
					pTex->r = nHistValue;
					pTex->g = nHistValue;
					pTex->b = 0;
				}
			}

			pDepthRow += rowSize;
			pTexRow += _nTexMapX;
		}
	}

	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _nTexMapX, _nTexMapY, 0, GL_RGB, GL_UNSIGNED_BYTEGL_UNSIGNED_BYTE, _pTexMap);

	// Display the OpenGL texture map
	glColor4f(1,1,1,1);

	glBegin(GL_QUADS);

	int nXRes = _width;
	int nYRes = _height;

	// upper left
	glTexCoord2f(0, 0);
	glVertex2f(0, 0);
	// upper right
	glTexCoord2f((float)nXRes/(float)_nTexMapX, 0);
	glVertex2f(GL_WIN_SIZE_X, 0);
	// bottom right
	glTexCoord2f((float)nXRes/(float)_nTexMapX, (float)nYRes/(float)_nTexMapY);
	glVertex2f(GL_WIN_SIZE_X, GL_WIN_SIZE_Y);
	// bottom left
	glTexCoord2f(0, (float)nYRes/(float)_nTexMapY);
	glVertex2f(0, GL_WIN_SIZE_Y);

	glEnd();*/
}

#if 0
void SampleViewer::onKey(unsigned char key, int /*x*/, int /*y*/)
{
	switch (key)
	{
	case 27:
		_depthStream.stop();
		_colorStream.stop();
		_depthStream.destroy();
		_colorStream.destroy();
		_device.close();
		openni::OpenNI::shutdown();

		exit (1);
	case '1':
		_eViewState = DISPLAY_MODE_OVERLAY;
		_device.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
		break;
	case '2':
		_eViewState = DISPLAY_MODE_DEPTH;
		_device.setImageRegistrationMode(openni::IMAGE_REGISTRATION_OFF);
		break;
	case '3':
		_eViewState = DISPLAY_MODE_IMAGE;
		_device.setImageRegistrationMode(openni::IMAGE_REGISTRATION_OFF);
		break;
	case 'm':
		_depthStream.setMirroringEnabled(!_depthStream.getMirroringEnabled());
		_colorStream.setMirroringEnabled(!_colorStream.getMirroringEnabled());
		break;
	}

}
#endif // 0

std::vector<VertexBufferObject> &KinectOpenNI::GetVBOs()
{
    return _vbos;
}
std::mutex &KinectOpenNI::GetLock()
{
    return _lock;
}
