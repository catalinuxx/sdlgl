﻿/*
 * Created by SharpDevelop.
 * User: catalinux
 * Date: 11/16/2014
 * Time: 12:33 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Microsoft.Kinect;
using System.Diagnostics;

namespace NuiKinectServer
{
	/// <summary>
	/// Description of Image.
	/// </summary>
	public class Kinect
	{
        public static bool ColorEnabled = false;
        public static bool DepthEnabled = false;
        public static bool SkeletonEnabled = true;

		KinectSensor _sensor = null;
		
		private byte[] _colorPixels;
		private byte[] _colorBytes;
		private short[] _depthPixels;
		private byte[] _depthBytes;
		private Skeleton[] _skeletonData;
		private byte[] _skeletonBytes;
		private int[] _skeletonPos;
		AsynchronousSocketListener _server;
		Stopwatch _stopWatch = new Stopwatch();
		
		// Logging activity
		long _colorMilli = 0;
		long _depthMilli = 0;
		long _skelMilli = 0;
		
		public Kinect()
		{
			_stopWatch.Start();
		}
		
		public bool Start(AsynchronousSocketListener server)
		{	
			foreach (var potentialSensor in KinectSensor.KinectSensors)
			{
				if (potentialSensor.Status == KinectStatus.Connected)
				{
					this._sensor = potentialSensor;
					break;
				}
			}
			if (_sensor == null)
			{
				Console.WriteLine("ERROR: Could not find any kinect sensor availble!");
				return false;
			}
			
			_sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
			_sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
			_sensor.SkeletonStream.Enable();
			_sensor.ColorStream.Enable(ColorImageFormat.InfraredResolution640x480Fps30);
			
			_colorPixels = new byte[_sensor.ColorStream.FramePixelDataLength];
			_depthPixels = new short[_sensor.DepthStream.FramePixelDataLength];
			_skeletonData = new Skeleton[_sensor.SkeletonStream.FrameSkeletonArrayLength];
			Console.WriteLine("skeleton data: " + _sensor.SkeletonStream.FrameSkeletonArrayLength);
			
			_sensor.ColorFrameReady += this.SensorColorFrameReady;
			_sensor.DepthFrameReady += this.SensorDepthFrameReady;
			_sensor.SkeletonFrameReady += this.SensorSkeletonFrameReady;
			
			_sensor.Start();
			
			_server = server;
			
			Console.WriteLine("Kinect started successfully");
			
			return true;
		}
		
		public bool Stop()
		{
			if (_sensor != null)
			{
				_sensor.Stop();
				_sensor = null;
			}
			return true;
		}
		
		private void SensorColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
		{
            if (!ColorEnabled)
                return;
			using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
			{
				if (colorFrame != null)
				{
					colorFrame.CopyPixelDataTo(_colorPixels);
					if (_server != null)
					{
						int numBytes = colorFrame.PixelDataLength;
						if (_colorBytes == null || _colorBytes.Length < numBytes)
							_colorBytes = new byte[numBytes];
						Buffer.BlockCopy(_colorPixels, 0, _colorBytes, 0, numBytes);
                        _server.SendColor((uint)(_stopWatch.ElapsedMilliseconds / 1000), (uint)(_stopWatch.ElapsedMilliseconds % 1000), _colorBytes, numBytes);
                    	if (_stopWatch.ElapsedMilliseconds - _colorMilli > 3000)
                    	{
                    		Console.WriteLine("Color works");
                    		_colorMilli = _stopWatch.ElapsedMilliseconds;
                    	}
					}
				}
			}  
		}
		
		private void SensorDepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
		{
            if (!DepthEnabled)
                return;
			using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
			{
				if (depthFrame != null)
				{
					depthFrame.CopyPixelDataTo(_depthPixels);
					if (_server != null)
					{
						int numBytes = (sizeof(short) / sizeof(byte)) * depthFrame.PixelDataLength;
						if (_depthBytes == null || _depthBytes.Length < numBytes)
							_depthBytes = new byte[numBytes];
						Buffer.BlockCopy(_depthPixels, 0, _depthBytes, 0, numBytes);
                        _server.SendDepth((uint)(_stopWatch.ElapsedMilliseconds / 1000), (uint)(_stopWatch.ElapsedMilliseconds % 1000), _depthBytes, numBytes);
                    	
                    	if (_stopWatch.ElapsedMilliseconds - _depthMilli > 3000)
                    	{
                    		Console.WriteLine("Depth works");
                    		_depthMilli = _stopWatch.ElapsedMilliseconds;
                    	}
					}
				}
			}  
		}
			
		private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
		{
            if (!SkeletonEnabled)
                return;
			using (SkeletonFrame frame = e.OpenSkeletonFrame())
			{
				if (frame != null)
				{
					frame.CopySkeletonDataTo(_skeletonData);

                    int id = 0;
					foreach (Skeleton skeleton in _skeletonData)
		            {
		                // skeleton` is an actively tracked skeleton
		                //Console.WriteLine("skeleton` is an actively tracked skeleton");
		                if (_server != null)
						{
                            int idx = 0;
                            if (SkeletonTrackingState.Tracked == skeleton.TrackingState)
                            {
                                int numInts = skeleton.Joints.Count * 4; // type, x, y, z
                                if (_skeletonPos == null || _skeletonPos.Length < numInts)
                                    _skeletonPos = new int[numInts];
                                foreach (Joint joint in skeleton.Joints)
                                {
                                    _skeletonPos[idx++] = (int)joint.JointType;
                                    _skeletonPos[idx++] = (int)(joint.Position.X * 100);
                                    _skeletonPos[idx++] = (int)(joint.Position.Y * 100);
                                    _skeletonPos[idx++] = (int)(joint.Position.Z * 100);
                                }
                                int numBytes = (sizeof(int) / sizeof(byte)) * numInts;
                                if (_skeletonBytes == null || _skeletonBytes.Length < numBytes)
                                    _skeletonBytes = new byte[numBytes];
                                Buffer.BlockCopy(_skeletonPos, 0, _skeletonBytes, 0, numBytes);
                                _server.SendSkeleton((uint)(_stopWatch.ElapsedMilliseconds / 1000), (uint)(_stopWatch.ElapsedMilliseconds % 1000), id++, 'T', _skeletonBytes, numBytes);
                                id++;

                                if (_stopWatch.ElapsedMilliseconds - _skelMilli > 3000)
                                {
                                    Console.WriteLine("Skeleton tracking works");
                                    _skelMilli = _stopWatch.ElapsedMilliseconds;
                                }
                            }
                            else if (SkeletonTrackingState.PositionOnly == skeleton.TrackingState)
                            {
                                const int numInts = 3; // x, y, z
                                if (_skeletonPos == null || _skeletonPos.Length < numInts)
                                    _skeletonPos = new int[numInts];
                                _skeletonPos[idx++] = (int)(skeleton.Position.X * 100);
                                _skeletonPos[idx++] = (int)(skeleton.Position.Y * 100);
                                _skeletonPos[idx++] = (int)(skeleton.Position.Z * 100);
                                const int numBytes = (sizeof(int) / sizeof(byte)) * numInts;
                                if (_skeletonBytes == null || _skeletonBytes.Length < numBytes)
                                    _skeletonBytes = new byte[numBytes];
                                Buffer.BlockCopy(_skeletonPos, 0, _skeletonBytes, 0, numBytes);
                                _server.SendSkeleton((uint)(_stopWatch.ElapsedMilliseconds / 1000), (uint)(_stopWatch.ElapsedMilliseconds % 1000), id, 'P', _skeletonBytes, numBytes);
                                id++;
                            }
						}
		            }
				}
			}
		}
	}
}
