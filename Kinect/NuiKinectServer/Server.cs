﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

// State object for reading client data asynchronously
public class StateObject {
    // Client  socket.
    public Socket workSocket = null;
    // Size of receive buffer.
    public const int BufferSize = 10240;
    // Receive buffer.
    public byte[] readBuffer = new byte[BufferSize];
	public byte[] writeBuffer = new byte[BufferSize];
    // Received data string.
    public StringBuilder sb = new StringBuilder();
    
    public int bytesSent;
    public int bytesToSend;
    public AutoResetEvent sendEvent;
        
    ~StateObject()
    {
    	if (workSocket != null)
    	{
    		try
    		{
		    	workSocket.Shutdown(SocketShutdown.Both);
		        workSocket.Close();
    		}
	        catch (System.Net.Sockets.SocketException ex)
	        {
	        	Console.WriteLine("Caught client socket shutdown exception: " + ex);
	        }
	        workSocket = null;
    	}
    }
}

public class AsynchronousSocketListener {
    // Thread signal.
    List<StateObject> _clients;
    Socket _listener;

    public AsynchronousSocketListener() {
    }

    public void StartListening() {
        // Data buffer for incoming data.
        byte[] bytes = new Byte[1024];

		IPAddress ipAddress = IPAddress.Any;
        
        int port = 1234;
        IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);

        // Create a TCP/IP socket.
        _listener = new Socket(AddressFamily.InterNetwork,
            SocketType.Stream, ProtocolType.Tcp );
        
        _clients = new List<StateObject>();

        // Bind the socket to the local endpoint and listen for incoming connections.
        try {
            _listener.Bind(localEndPoint);
            _listener.Listen(100);
            
            Console.WriteLine("Server successfully started on " + ipAddress + ":" + port);
            
           CallAccept();

        } catch (Exception e) {
        	Console.WriteLine("Server listenning exception: " + e.ToString());
        }
        
    }

    public void AcceptCallback(IAsyncResult ar) {
        // Get the socket that handles the client request.
        Socket listener = (Socket) ar.AsyncState;
        Socket handler = listener.EndAccept(ar);
        
        Console.WriteLine("Socket accepted from " + handler.RemoteEndPoint);

        // Create the state object.
        StateObject state = new StateObject();
        state.workSocket = handler;
        state.sendEvent = new AutoResetEvent(true);
        	
        handler.BeginReceive( state.readBuffer, 0, StateObject.BufferSize, 0,
            new AsyncCallback(ReadCallback), state);
        
        lock (_clients)
        	_clients.Add(state);
        
        CallAccept();
    }

    public void ReadCallback(IAsyncResult ar) {
        String content = String.Empty;
        
        // Retrieve the state object and the handler socket
        // from the asynchronous state object.
        StateObject state = (StateObject) ar.AsyncState;
        Socket handler = state.workSocket;

        try
        {
	        // Read data from the client socket. 
	        int bytesRead = handler.EndReceive(ar);
	
	        if (bytesRead > 0) {
	            // There  might be more data, so store the data received so far.
	            state.sb.Append(Encoding.ASCII.GetString(
	                state.readBuffer,0,bytesRead));
	
	            // Check for end-of-file tag. If it is not there, read 
	            // more data.
	            content = state.sb.ToString();
	            if (content.IndexOf("<EOF>") > -1)
	            {
	            	lock (_clients)
	            		_clients.Remove(state);
	            } else {
	                // Not all data received. Get more.
	                handler.BeginReceive(state.readBuffer, 0, StateObject.BufferSize, 0,
	                new AsyncCallback(ReadCallback), state);
	            }
	        }
        }
        catch (System.Net.Sockets.SocketException ex)
        {
        	Console.WriteLine("Caught client socket exception: " + ex);
        	state.sendEvent.Set();
        	lock (_clients)
        		_clients.Remove(state);
        }
    }
    
    private byte[] Combine( params byte[][] arrays )
	{
	    byte[] rv = new byte[ arrays.Sum( a => a.Length ) ];
	    int offset = 0;
	    foreach ( byte[] array in arrays ) {
	        System.Buffer.BlockCopy( array, 0, rv, offset, array.Length );
	        offset += array.Length;
	    }
	    return rv;
	}
    
    public void SendColor(uint seconds, uint milliseconds, byte[] byteData, int len)
    {
    	lock (_clients)
			foreach (StateObject client in _clients)
			{
    			try
    			{
	    			byte[] header = Combine(
			        	Encoding.ASCII.GetBytes("C"),
			        	BitConverter.GetBytes(seconds),
			        	BitConverter.GetBytes(milliseconds),
			        	BitConverter.GetBytes(len)
			        );
	    			SendData(client, header, header.Length);
			        SendData(client, byteData, len);
    			}
    			catch (System.Net.Sockets.SocketException ex)
				{
					Console.WriteLine("Caught exception while sending color: " + ex);
					_clients.Remove(client);
					return;
				}
	    	}
    }

    public void SendDepth(uint seconds, uint milliseconds, byte[] byteData, int len)
    {
    	lock (_clients)
			foreach (StateObject client in _clients)
			{
    			try
    			{
			        byte[] header = Combine(
			        	Encoding.ASCII.GetBytes("D"),
			        	BitConverter.GetBytes(seconds),
			        	BitConverter.GetBytes(milliseconds),
			        	BitConverter.GetBytes(len)
			        );
			        SendData(client, header, header.Length);
			        SendData(client, byteData, len);
    			}
    			catch (System.Net.Sockets.SocketException ex)
				{
					Console.WriteLine("Caught exception while sending depth: " + ex);
					_clients.Remove(client);
					return;
				}
	    	}
    }

    public void SendSkeleton(uint seconds, uint milliseconds, int id, int type, byte[] byteData, int len)
    {
    	//Console.WriteLine("S: id:{0} type:{1} sec:{2} milli{3} len:{4}", id, type, seconds, milliseconds, len);
    	
    	lock (_clients)
			foreach (StateObject client in _clients)
			{
	    		try
	    		{
			        byte[] header = Combine(
			        	Encoding.ASCII.GetBytes("S"),
			        	BitConverter.GetBytes(seconds),
			        	BitConverter.GetBytes(milliseconds),
			        	BitConverter.GetBytes(len + sizeof(int) * 2), // Two ints follow
			        	BitConverter.GetBytes(id),
			        	BitConverter.GetBytes(type)
			        );
	    			SendData(client, header, header.Length);
			        SendData(client, byteData, len);
	    		}
				catch (System.Net.Sockets.SocketException ex)
				{
					Console.WriteLine("Caught exception while sending skeleton: " + ex);
					_clients.Remove(client);
					return;
				}
    		}
    }
    
    private void SendData(StateObject client, byte[] byteData, int len)
    {
		//Console.WriteLine("send event wait begin");
		client.sendEvent.WaitOne();
		//Console.WriteLine("send event wait end");
		
		if (client.writeBuffer == null || client.writeBuffer.Length < len)
			client.writeBuffer = new byte[len];
		Array.Copy(byteData, client.writeBuffer, len);
		client.bytesSent = 0;
		client.bytesToSend = len;
		
	    // Begin sending the data to the remote device.
	    client.workSocket.BeginSend(client.writeBuffer, client.bytesSent, client.bytesToSend, 0,
	        new AsyncCallback(SendCallback), client);
    }

    private void SendCallback(IAsyncResult ar) {
    	// Retrieve the socket from the state object.
        StateObject state = (StateObject) ar.AsyncState;
        
        try
        {
            // Complete sending the data to the remote device.
            int bytesSent = state.workSocket.EndSend(ar);
                        
            if (state.bytesSent + bytesSent < state.bytesToSend)
            {
            	//Console.WriteLine("Sent {0} bytes to client.", bytesSent);
            	state.bytesSent += bytesSent;
            	state.workSocket.BeginSend(state.writeBuffer, state.bytesSent, state.bytesToSend - state.bytesSent, 0,
	            	new AsyncCallback(SendCallback), state);
            }
            else
            {
            	///Console.WriteLine("Sending {0} bytes completed", state.bytesToSend);
            	state.bytesSent = state.bytesToSend = 0;
            	state.sendEvent.Set();
            }
        }
    	catch (Exception e)
    	{
            Console.WriteLine(e.ToString());
            state.sendEvent.Set();
            lock (_clients)
        		_clients.Remove(state);
        }
    }
    
    void CallAccept()
    {
    	// Start an asynchronous socket to listen for connections.
        //Console.WriteLine("Waiting for a connection");
        _listener.BeginAccept( 
            new AsyncCallback(AcceptCallback),
            _listener);
    }
}