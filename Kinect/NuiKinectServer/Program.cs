﻿/*
 * Created by SharpDevelop.
 * User: catalinux
 * Date: 11/16/2014
 * Time: 12:30 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Microsoft.Kinect;

namespace NuiKinectServer
{
	class Program
	{
		public static void Main(string[] args)
		{
			try
			{
				Kinect kinect = new Kinect();
				AsynchronousSocketListener server = new AsynchronousSocketListener();
			
				server.StartListening();
				
				if (!kinect.Start(server))
					throw new Exception("Kinnect could not be started");
				
				//Console.Write("Press any key to exit . . . ");
				Console.ReadKey(true);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Caught exception: " + ex);
			}
		}
	}
}