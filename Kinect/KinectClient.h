#pragma once

#include "FileIO.h"
#include "KinectInterface.h"

#define MAX_SKELETONS 6

class KinectClient : public KinectInterface
{
public:
    KinectClient();
    virtual ~KinectClient();

    int Init();
    std::vector<VertexBufferObject> &GetVBOs();
    std::vector<Skeleton> &GetSkeletons() { return _skeletons; }
    std::mutex &GetLock();
    const std::string GetStats();

private:
    bool Connect();
    void Disconnect();
    void ThreadProc();
    void ParseHeader(char *buff);
    void ParseBuffer(char *buff, int len);
    void DecodeBuffer(PacketType packetType, char *buff, int len);
    void ReallocParseBuffer(int size);
    void SocketRead();
    void RecorderRead();

private:
    std::string _server = "localhost";
    int _port = 1234;
    bool _connected = false;
    int _connectReTries = 0;

    std::thread _thread;
    std::mutex _lock;
    bool _threadStarted = false;
    volatile bool _terminate = false;

    std::vector<VertexBufferObject> _vbos = std::vector<VertexBufferObject>(KinectMaxVBO);

    // Network
    int _sock = -1;
    static const int BufferSize = 1024;
    static const int HeaderSize =
        sizeof(char) +         /* Type */
        sizeof(unsigned int) + /* Seconds */
        sizeof(unsigned int) + /* Milliseconds */
        sizeof(int)   /* Packet length */;
    char _buff[BufferSize] = {0,};

    // Buffer parsing
    static const int MaxParsePacketSize = 1920 * 1080; // Assume an maximum
    PacketType _packetType = NoPacket;
    int _packetLen = 0;
    unsigned int _packetSeconds = 0;
    unsigned int _packetMilliSeconds = 0;

    char *_parseBuffer = new char[HeaderSize];
    int _parseBufferSize = HeaderSize;
    int _parseLen = 0;

    // Record
    std::unique_ptr<Writer> _writer;
    std::unique_ptr<Reader> _reader;

    // Joints
    std::vector<Skeleton> _skeletons = std::vector<Skeleton>(MAX_SKELETONS);
};
