#include "VarManager.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

VarManager::VarManager()
{
    set("win_posx", 150);
    set("win_posy", 150);
    set("win_width", 800);
    set("win_height", 600);

    set("mouse_sensivity", 0.005f);

    set("cam_znear", 0.1f);
    set("cam_zfar", 800.0f);
    set("cam_fovy", 60.0f);
    set("cam_driven", false);
    set("cam_joystick", false);

    set("cam_anaglyph_offset", 0.16f);

    set("enable_sky", false);
    set("enable_anaglyph", false);
    set("enable_move_control", true);
    set("enable_effects", false);
    set("enable_vignette", false);
    set("enable_bloom", false);
    set("enable_noise", false);
    set("enable_pdc", false);
    set("enable_underwater", false);

    set("terrain_chunks_drawn", 0);
    set("terrain_chunks_reflected_drawn", 0);

    set("show_camera_splines", false);

    set("time_speed", 1.0f);

    set("water_height", 2.2f);

    set("left_right_rotate", true);

    // SDL
    set("sdl_img_enabled", true);
    set("sdl_img_extensions", ".jpg,.jpeg,.png,.tga");

#ifndef HAVE_GLES
    set("scenes", "terrain,test_shadowmapping,test_refraction,toon,test_fur");
#else
    set("scenes", "simple");
#endif

    set("draw_bounding_boxes", false);
}

void VarManager::FromCfg(const std::string &cfgFile)
{
    LOG_INFO("Reading config from %s ...\n", cfgFile.c_str());

    std::string s;
    if (Util::ReadAllFile(cfgFile, s) < 0)
        throw std::runtime_error("could not open config file " + cfgFile);
    std::istringstream in(s);

    std::string line;

    while (std::getline(in, line))
    {
        if (line.length() < 2 || line[0] == '#')
            continue;
        if (line[line.length() - 1] == '\r' || line[line.length() - 1] == '\n')
            line = line.substr(0, line.length() - 1);

        if (line[1] != ' ')
            throw std::runtime_error("cfg: space is mandatory after variable type. line: " + line);
        auto pos = line.find('=');
        if (pos == std::string::npos)
            throw std::runtime_error("cfg: format should be: type name = value. line: " + line);

        std::string name = line.substr(2, pos - 2);

        switch (line[0])
        {
        case 'i':
            set(name, std::atoi(line.substr(pos + 1).c_str()));
            break;
        case 'b':
            set(name, line.substr(pos + 1) == "true");
            break;
        case 'f':
            set(name, (float)std::atof(line.substr(pos + 1).c_str()));
            break;
        case 's':
            set(name, line.substr(pos + 1));
            break;
        }
    }
}

VarManager::~VarManager()
{
    m_VarDB.clear();
}

void VarManager::Print(const std::string &msg)
{
    if (!msg.empty())
        LOG_INFO("%s\n", msg.c_str());;
    for (auto s : m_VarDB)
    {
        std::stringstream ss;
        switch (s.second.type)
        {
        case 'i':
        case 'b':
            ss << (int)s.second.ival;
            break;
        case 'f':
            ss << s.second.ival;
            break;
        case 's':
            ss << '\'' << s.second.sval << '\'';
            break;
        }
        //LOG_INFO("%c %s = %s\n", s.second.type, s.first.c_str(), ss.str().c_str());
        LOG_INFO("%c %s = %s", s.second.type, s.first.c_str(), ss.str().c_str());
    }
}
