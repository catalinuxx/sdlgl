#pragma once

#include "SceneBase.h"

class Texture2D;

class SceneSimple : public SceneBase
{
public:
    // Initialisation des donn�es propres � la sc�ne
    virtual void Init();

    // Destruction des donn�es propres � la sc�ne
    virtual ~SceneSimple();

    // Evolution des donn�es et traitement des commandes synchones
    virtual void Idle(float fElapsedTime);

    // D�marrage de la sc�ne
    virtual void Reset();

    // Affichage de la sc�ne
    virtual void Render();

    void Keyboard(bool special, unsigned char key);

    std::string GetStats();

private:
    Mesh        *pMesh = nullptr;
    Shader      *pShader = nullptr;
    Texture2D*	m_pMyTex = nullptr;
    float		m_fAngle;
    int         _group = -1;
    bool        _rotate = false;
    bool        _switchGroup = false;

    TextureCubemap *m_pSkybox = nullptr;

    // Sun
    vec2					m_vSunAngle;
    vec4					m_vSunVector;
};
