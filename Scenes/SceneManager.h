#pragma once

#include "Singleton.h"
#include <iostream>
#include <map>
#include <vector>
#include <functional>
#include <string>

#include "Script.h"

class SceneBase;

class SceneManager : public Singleton<SceneManager>
{
public:
    SceneManager();

    void Init();
    void Idle(float fElapsedTime);
    void Keyboard(bool special, unsigned char key);
    void PreRender();
    void Render();
    void Clear();

    bool setCurrent(const std::string& name);
    bool setCurrent(std::size_t idx);
    SceneBase* getScenePointer(const std::string& name);

    inline SceneBase* getCurrentScenePointer()	const
    {
        return m_pCurrentScene;
    }

    static const std::map<std::string, std::function<SceneBase *()>> Scenes;

private:
    friend class Singleton<SceneManager>;
    virtual ~SceneManager();

    using SceneDBPair = std::pair<std::string, SceneBase*>;
    std::vector<SceneDBPair> m_SceneDB; // All scenes
    SceneBase* m_pCurrentScene = NULL; // Current scene
    Script _script;
};

