#pragma once

#include "SceneBase.h"

class Texture2D;

class SceneCar : public SceneBase
{
public:
    virtual void Init();

    virtual ~SceneCar();

    virtual void Idle(float fElapsedTime);

    virtual void Reset();

    virtual void Render();

    void Keyboard(bool special, unsigned char key);

    std::string GetStats();

private:
    float		m_fAngle;
    int         _group = -1;
    bool        _rotate = true;
    bool        _switchGroup = false;
    Mesh        *_pMesh = nullptr;
    Shader      *_pShader = nullptr;
    TextureCubemap *_pSkybox = nullptr;
    std::string _stats;
    std::vector<vec3> _positions;
};
