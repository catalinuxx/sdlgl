#include "Util.h"
#include <sstream>

#include <SDL.h>

#include "Shader.h"
#include "Texture2D.h"
#include "TextureCubemap.h"
#include "Sky.h"

#include "Mathlib.h"
#include "../Camera.h"

#include "../ResourceManager.h"
#include "../VarManager.h"
#include "../Spline3D/SplineGL.h"

#include "../Inputs.h"

#include "SceneCar.h"

// Initialization data specific to the stage
void SceneCar::Init()
{
    // Resource loading
    ResourceManager& res = ResourceManager::get();
    VarManager& var = VarManager::get();

    _pSkybox = dynamic_cast<decltype(_pSkybox)>(res.LoadResource(ResourceManager::TEXTURECUBEMAP, "posx.jpg negx.jpg posy.jpg negy.jpg posz.jpg negz.jpg"));
    _pShader = dynamic_cast<decltype(_pShader)>(res.LoadResource(ResourceManager::SHADER, "simple"));

    _pMesh = dynamic_cast<decltype(_pMesh)>(res.LoadResource(ResourceManager::MESH, "car.obj"));
    if (_pMesh)
    {
        if (var.getb("draw_bounding_boxes") && _pMesh)
            _pMesh->EnableBBoxDraw();

        // Assign positions
        _positions.resize(VarManager::get().geti("car_num_cars", 1));
        for (std::size_t i = 0; i < _positions.size(); ++i)
            _positions[i] = vec3(0, 0, 40 * i);
    }

    LoadCameraTrajFromFile("toon.txt");

    m_fAngle = 0.0f;
}

SceneCar::~SceneCar()
{
}

void SceneCar::Keyboard(bool special, unsigned char key)
{
    if (key == 'z' && !special)
    {
        assert(_pMesh);
        if ((std::size_t)++_group == _pMesh->GetGroupCount())
            _group = -1;
    }
    else if (key == 's' && !special)
        _rotate = !_rotate;
    else if (key == 'g' && !special)
        _switchGroup = !_switchGroup;
}

// Evolution of data and processing with synchronous command
void SceneCar::Idle(float fElapsedTime)
{
    // Taking into account the time elapsed since the previous frame
    // our data to change with time

    if (_rotate)
        m_fAngle += 10.0f * fElapsedTime;

    if (_switchGroup)
    {
        static float total = 0;
        total += fElapsedTime;
        if (total > 0.5f)
        {
            assert(_pMesh);
            _group = (_group + 1) % _pMesh->GetGroupCount();
            total = 0;
        }
    }
}

// Starting the scene
void SceneCar::Reset()
{
    SceneBase::Reset();
}

// Viewing the scene
void SceneCar::Render()
{
    if (_pSkybox)
        Sky::get().DrawSkyAndSun(Camera::get().getEye(), vec3(1,-0.7,1), _pSkybox->getHandle(), false);

    if (_pShader && _pShader->Activate(Camera::get().GetModelView(), Camera::get().GetProjection()))
    {
        std::stringstream ss;

        Util::get().DrawAxes(*_pShader);

        int notVisible = 0;
        int total = 0;

        for (std::size_t i = 0; i < _positions.size(); i++)
            if (_pMesh)
            {
                int sign = _positions[i].z > 0.0 ? 1 : -1;
                _positions[i].z += 0.1 * sign;

                //if (_positions[i].z > 3.0 * sign) _positions[i].z *= -sign; // Reverse

                _pMesh->Move(_positions[i]);

                _pShader->SetModelView(Camera::get().GetModelView() * mat4(_pMesh->GetTranslation()) * mat4(0.0f, 1.0f, 0.0f, m_fAngle));
                _pMesh->Draw(*_pShader, _group);

                notVisible += _pMesh->GetNumberOfNonVisibleGroups();
                total += _pMesh->GetGroupCount();
            }

        ss << "Visible cars: " << total - notVisible << '/' << total;
        _stats = ss.str();

        _pShader->Deactivate();
    }
}

std::string SceneCar::GetStats()
{
    return _stats;
}

