#include "Util.h"

#include <SDL.h>

#include "../Shader.h"
#include "../Texture2D.h"
#include "TextureCubemap.h"
#include "Sky.h"

#include "Mathlib.h"
#include "../Camera.h"

#include "../ResourceManager.h"
#include "../VarManager.h"
#include "../Spline3D/SplineGL.h"

#include "../Inputs.h"

#include "SceneKinect.h"

#include "MD5Loader.h"

// Initialization data specific to the stage
void SceneKinect::Init()
{
    // Resource loading
    ResourceManager& res = ResourceManager::get();
    VarManager& var = VarManager::get();

    if (_kinect.Init() < 0)
    {
        LOG_INFO("Kinect init failed!");
        throw std::runtime_error("Kinect init failed!");
    }

    //_pSkybox = dynamic_cast<decltype(_pSkybox)>(res.LoadResource(ResourceManager::TEXTURECUBEMAP, "posx.jpg negx.jpg posy.jpg negy.jpg posz.jpg negz.jpg"));
    //_pSkybox = (TextureCubemap*)res.LoadResource(ResourceManager::TEXTURECUBEMAP, "sand_up.jpg sand_dn.jpg sand_tp.jpg sand_bm.jpg sand_lt.jpg sand_rt.jpg");
    _pShader = dynamic_cast<decltype(_pShader)>(res.LoadResource(ResourceManager::SHADER, "kinect"));

    //_pMesh = dynamic_cast<decltype(_pMesh)>(res.LoadResource(ResourceManager::MESH, "sphere.obj"));
    if (_pMesh)
    {
        if (var.getb("draw_bounding_boxes"))
            _pMesh->EnableBBoxDraw();
    }
    int tmp = 0;
    for (auto &skel : _kinect.GetSkeletons())
    {
        if (tmp++ > 0)
            break;
        skel.Load("MD5/kinect.md5mesh");
        //skel.Load("MD5/bob/bob_lamp_update.md5mesh");
    }

    //LoadCameraTrajFromFile("toon.txt");

    m_fAngle = 0.0f;
}

SceneKinect::~SceneKinect()
{
}

void SceneKinect::Keyboard(bool special, unsigned char key)
{
    if (key == 'z' && !special)
    {
        assert(_pMesh);
        if ((std::size_t)++_group == _pMesh->GetGroupCount())
            _group = -1;
    }
    else if (key == 's' && !special)
        _rotate = !_rotate;
    else if (key == 'g' && !special)
        _switchGroup = !_switchGroup;
}

// Evolution of data and processing with synchronous command
void SceneKinect::Idle(float fElapsedTime)
{
    // Taking into account the time elapsed since the previous frame
    // our data to change with time

    if (_rotate)
        m_fAngle += 10.0f * fElapsedTime;

    if (_switchGroup)
    {
        static float total = 0;
        total += fElapsedTime;
        if (total > 0.5f)
        {
            assert(_pMesh);
            _group = (_group + 1) % _pMesh->GetGroupCount();
            total = 0;
        }
    }
}

// Starting the scene
void SceneKinect::Reset()
{
    SceneBase::Reset();
}

// Viewing the scene
void SceneKinect::Render()
{
    if (_pSkybox)
        Sky::get().DrawSkyAndSun(Camera::get().getEye(), vec3(1,-0.7,1), _pSkybox->getHandle(), false);

    if (_pShader&& _pShader->Activate(Camera::get().GetModelView(), Camera::get().GetProjection()))
    {
        mat4 sca;

        Util::get().DrawAxes(*_pShader);

        //sca.scale(1.0/500.0, 1.0/500.0, 1.0/500.0); // OpenNi
        //sca.scale(1.0/2000.0, 1.0/2000.0, 1.0/2000.0); // Ms depth
        //sca.scale(2.0, 2.0, 2.0); // Ms skel
        _pShader->SetModelView(Camera::get().GetModelView() * sca);
        if (1)
        {
            //sca.scale(0.3,0.3,0.3);
            _pShader->SetModelView(Camera::get().GetModelView() *
                   mat4(1.0f, 0.0f, 0.0f, 272) *
                   mat4(0.0f, 0.0f, 1.0f, 168) *
                   sca);
            int tmp = 0;
            for (auto &skel : _kinect.GetSkeletons())
            {
                if (tmp++ > 0)
                    break;
                skel.Draw(*_pShader);
                //skel.DrawJoints(*_pShader);
            }


            //std::lock_guard<std::mutex> lock(_kinect.GetLock()); // Lock writing
            int idx = 0;
            _pShader->SetModelView(Camera::get().GetModelView());
            for (auto &vbo : _kinect.GetVBOs())
            {
                if ((idx == Skeleton1KinectVBO || idx == Skeleton2KinectVBO) && vbo.NumPoints() > 0)
                {
                    vbo.Enable(*_pShader);
                    vbo.BufferPosition();
                    vbo.BufferColor();

                    //GL_CHECK(glLineWidth(7.0));
                    //GL_CHECK(glDrawArrays(GL_TRIANGLES, 0, vbo.NumPoints()));
                    //GL_CHECK(glDrawArrays(GL_POINTS, 0, vbo.NumPoints()));
                    //GL_CHECK(glDrawArrays(GL_LINE_LOOP, 0, vbo.NumPoints()));
                    //GL_CHECK(glDrawArrays(GL_TRIANGLE_FAN, 0, vbo.NumPoints()));
                    GL_CHECK(glDrawArrays(GL_LINES, 0, vbo.NumPoints()));

                    //LOG_INFO("%d: num skel points: %d", idx, vbo.NumPoints());
                    /*for (int i = 0; i < vbo.NumPoints(); ++i)
                    {
                        LOG_INFO("%d: num skel points: %d. %.2f %.2f %.2f", idx, vbo.NumPoints(),
                            vbo.getPosition()[i].x, vbo.getPosition()[i].y, vbo.getPosition()[i].z);
                    }*/

                    vbo.Disable(*_pShader);
                }

                idx++;
            }
        }

        _pShader->Deactivate();
    }
}

std::string SceneKinect::GetStats()
{
    return _kinect.GetStats();
}
