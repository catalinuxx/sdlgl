#include "Util.h"

#include <SDL.h>

#include <sstream>
#include "../Shader.h"
#include "../Texture2D.h"
#include "TextureCubemap.h"
#include "Sky.h"

#include "SceneSimple.h"

#include "Mathlib.h"
#include "../Camera.h"

#include "../ResourceManager.h"
#include "../VarManager.h"
#include "../Spline3D/SplineGL.h"

#include "../Inputs.h"

// Initialization data specific to the stage
void SceneSimple::Init()
{
    // Resource loading
    ResourceManager& res = ResourceManager::get();
    VarManager& var = VarManager::get();

    //m_pSkybox = dynamic_cast<decltype(m_pSkybox)>(res.LoadResource(ResourceManager::TEXTURECUBEMAP, "posx.jpg negx.jpg posy.jpg negy.jpg posz.jpg negz.jpg"));
    m_pSkybox = (TextureCubemap*)res.LoadResource(ResourceManager::TEXTURECUBEMAP, "sand_up.jpg sand_dn.jpg sand_tp.jpg sand_bm.jpg sand_lt.jpg sand_rt.jpg");

    //m_pMyTex = (Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "rocks_diffuse.jpg");

    pShader = dynamic_cast<decltype(pShader)>(res.LoadResource(ResourceManager::SHADER, "simple"));
    pMesh = dynamic_cast<decltype(pMesh)>(res.LoadResource(ResourceManager::MESH, var.gets("simple_mesh")));
    if (var.getb("draw_bounding_boxes") && pMesh)
        pMesh->EnableBBoxDraw();

    //LoadCameraTrajFromFile("fur.txt");
    //LoadCameraTrajFromFile("toon.txt");
    LoadCameraTrajFromFile("out.txt");

    m_fAngle = 0.0f;
    m_vSunAngle = vec2(0.0f, RADIANS(45.0f));
}

SceneSimple::~SceneSimple()
{
}

void SceneSimple::Keyboard(bool special, unsigned char key)
{
    if (key == 'm' && !special)
    {
        assert(pMesh);
        if ((std::size_t)++_group == pMesh->GetGroupCount())
            _group = -1;
    }
    else if (key == '.' && !special)
        _rotate = !_rotate;
    else if (key == 'g' && !special)
        _switchGroup = !_switchGroup;
    else if ( key == 'z' && !special)			m_vSunAngle.y += 0.01f;
    else if ( key == 's' && !special)			m_vSunAngle.y -= 0.01f;
    else if ( key == 'q' && !special)			m_vSunAngle.x += 0.01f;
    else if ( key == 'd' && !special)			m_vSunAngle.x -= 0.01f;

    SceneBase::Keyboard(special, key);
}

// Evolution of data and processing with synchronous command
void SceneSimple::Idle(float fElapsedTime)
{
    if (_rotate)
        m_fAngle += 10.0f * fElapsedTime;

    if (_switchGroup)
    {
        static float total = 0;
        total += fElapsedTime;
        if (total > 0.5f)
        {
            assert(pMesh);
            _group = (_group + 1) % pMesh->GetGroupCount();
            total = 0;
        }
    }

    m_vSunVector = vec4(	-cosf(m_vSunAngle.x) * sinf(m_vSunAngle.y),
                            -cosf(m_vSunAngle.y),
                            -sinf(m_vSunAngle.x) * sinf(m_vSunAngle.y),
                            0.0f );
}

// Starting the scene
void SceneSimple::Reset()
{
    Camera::get().setEye(vec3(0.0f, 0.0f, 0.0f));
    SceneBase::Reset();
}

// Viewing the scene
void SceneSimple::Render()
{
    static const vec4 white(1.0f, 1.0f, 1.0f, 1.0f);
    static const vec4 black(0.0f, 0.0f, 0.0f, 1.0f);
    static const vec4 orange(1.0f, 0.5f, 0.0f, 1.0f);
    static const vec4 yellow(1.0f, 1.0f, 0.8f, 1.0f);
    static const vec3 zeros(0.0f, 0.0f, 0.0f);
    float sun_cosy = cosf(m_vSunAngle.y);
    vec4 vSunColor = white.lerp(orange, yellow, 0.25f + sun_cosy * 0.75f);
    Sky::get().DrawSkyAndSun(Camera::get().getEye(), vec3(m_vSunVector), m_pSkybox->getHandle(), false);

    if (pShader && pShader->Activate(Camera::get().GetModelView() * mat4(0.0f, 1.0f, 0.0f, m_fAngle), Camera::get().GetProjection()))
    {
        pShader->SetLight(vec3(m_vSunVector), white, vSunColor, vSunColor, zeros);

        Util::get().DrawAxes(*pShader);

        if (pMesh)
            pMesh->Draw(*pShader, _group);

        pShader->Deactivate();
    }
}

std::string SceneSimple::GetStats()
{
    if (pMesh)
    {
        std::stringstream ss;
        ss << "Visible: " << pMesh->GetGroupCount() - pMesh->GetNumberOfNonVisibleGroups() << '/' << pMesh->GetGroupCount();
        return ss.str();
    }
    return "";
}
