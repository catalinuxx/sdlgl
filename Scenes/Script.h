#pragma once

class Script
{
public:
    void Idle(float fElapsedTime);
    void ResetError();
private:
    void SceneError();
private:
    int _current = -1;
};
