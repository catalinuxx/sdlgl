#include "SceneToon.h"

#include "Mathlib.h"
#include "../Camera.h"
#include "../Mesh.h"
#include "../Inputs.h"
#include "../Shader.h"
#include "../Texture2D.h"
#include "Util.h"

#include "../ResourceManager.h"
#include "../VarManager.h"
#include "../Spline3D/SplineGL.h"

// Initialisation des donn�es propres � la sc�ne
void SceneToon::Init()
{
    // On acc�de au Resource Manager pour charger des ressources :
    ResourceManager& res = ResourceManager::get();
    VarManager& var = VarManager::get();

    // Ici on charge une texture, et on r�cup�re un pointeur sur la donn�e pour y acc�der plus facilement
    m_pMyTexGround = (Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "dirtground008.jpg");
    m_pMyTexWall = (Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "brickwall004.jpg");
    m_pMyTexTop = (Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "floor_tile_006a_reflect.jpg");

    m_pScene = (Mesh*)res.LoadResource(ResourceManager::MESH, "scene_fur.obj");
    m_objHomer = (Mesh*)res.LoadResource(ResourceManager::MESH, "bunny_1500_smooth.obj");
    m_teapot = (Mesh*)res.LoadResource(ResourceManager::MESH, "teapot.obj");
    _car = (Mesh*)res.LoadResource(ResourceManager::MESH, "car.obj");
    _sphere = (Mesh*)res.LoadResource(ResourceManager::MESH, "sphere.obj");
    if (var.getb("draw_bounding_boxes"))
    {
        if (m_pScene)
            m_pScene->EnableBBoxDraw();
        if (m_objHomer)
            m_objHomer->EnableBBoxDraw();
        if (m_teapot)
            m_teapot->EnableBBoxDraw();
        if (_car)
            _car->EnableBBoxDraw();
        if (_sphere)
            _sphere->EnableBBoxDraw();
    }

    m_shadeToon = (Shader*)res.LoadResource(ResourceManager::SHADER, "toon1");
    m_fAngle = 0.0f;

    m_vLightPos = vec3(0.0f, 4.0f, 0.0f);

    LoadCameraTrajFromFile("toon.txt");
}

// Evolution des donn�es et traitement des commandes synchones
void SceneToon::Idle(float fElapsedTime)
{
    Inputs& inputs = Inputs::get();

    if ( inputs.Key('z') )          m_vLightPos.z += 0.1f;
    if ( inputs.Key('s') )			m_vLightPos.z -= 0.1f;
    if ( inputs.Key('q') )			m_vLightPos.x += 0.1f;
    if ( inputs.Key('d') )			m_vLightPos.x -= 0.1f;
    if ( inputs.Key('r') )			m_vLightPos.y += 0.1f;
    if ( inputs.Key('f') )			m_vLightPos.y -= 0.1f;

    // On prend en compte le temps �coul� depuis la frame pr�c�dente
    // pour faire �voluer nos donn�es en fonction du temps
    m_fAngle += 10.0f * fElapsedTime;
}

// D�marrage de la sc�ne
void SceneToon::Reset()
{
    // On peut par exemple remettre la cam�ra � une position donn�e :
    Camera::get().setEye(vec3(0.0f, 0.0f, 0.0f));

    m_fAngle = 0.0f;

    SceneBase::Reset();
}

// Affichage de la sc�ne
void SceneToon::Render()
{
    vec4 white(1.0f, 1.0f, 1.0f, 1.0f);
    vec4 red(1.0f, 0.0f, 0.0f, 1.0f);
    vec4 blue(0.0f, 0.0f, 1.0f, 1.0f);
    vec4 zeros(0.0f, 0.0f, 0.0f, 0.0f);
    vec4 marron(0.95, 0.6, 0.25, 1.0);
    vec4 jaune(1.0, 0.8, 0.0, 1.0);

    if (m_shadeToon && m_shadeToon->Activate(Camera::get().GetModelView(), Camera::get().GetProjection()))
    {
        m_shadeToon->SetTexActive(false);

        m_shadeToon->SetLight(m_vLightPos, white, white, white, vec3());

        m_shadeToon->SetMatSpecular(white);
        m_shadeToon->SetMatShininess(10.0f);

        //Util::get().DrawAxes(*m_shadeToon);

        if (_sphere)
        {
            // Scale and translate to light pos
            mat4 sca;
            sca.scale(0.1, 0.1, 0.1);
            mat4 trans = Camera::get().GetModelView() * mat4(m_vLightPos.x, m_vLightPos.y, m_vLightPos.z) * sca;
            m_shadeToon->SetModelView(trans);
            _sphere->Draw(*m_shadeToon, -1, true);
        }

        m_shadeToon->SetTexActive(true);

        m_shadeToon->SetModelView(Camera::get().GetModelView());

        // Ground
        m_pMyTexGround->Bind(0);
        m_pScene->Draw(*m_shadeToon, 0, true);
        m_pMyTexGround->Unbind(0);

        // Wall
        m_pMyTexWall->Bind(0);
        m_pScene->Draw(*m_shadeToon, 3, true);
        m_pMyTexWall->Unbind(0);

        // Plafon
        m_pMyTexTop->Bind(0);
        m_pScene->Draw(*m_shadeToon, 1, true);
        m_pMyTexTop->Unbind(0);

        m_shadeToon->SetTexActive(false);

        mat4 move(0.0, 4.0, 0.0);

        // Car
        {
            // Scale, translate and rotate
            mat4 sca;
            sca.scale(0.5, 0.5, 0.5);
            mat4 trans = Camera::get().GetModelView() * mat4(0.0f, 1.0f, 0.0f, m_fAngle) * move * sca;
            m_shadeToon->SetModelView(trans);
            //m_shadeToon->SetMatAmbient(red / 4);
            //m_shadeToon->SetMatDiffuse(red / 2);
            m_shadeToon->SetMatAmbient(white/8);
            m_shadeToon->SetMatDiffuse(white/8);

            m_shadeToon->SetTexActive(true);
            _car->Draw(*m_shadeToon, -1, true);
            m_shadeToon->SetTexActive(false);
        }

        // Teapot
        {
            // Translate, rotate and translate again
            m_teapot->Move(vec3(0.0, 0.0, -12.0));
            mat4 trans = Camera::get().GetModelView() * mat4(m_teapot->GetTranslation()) * mat4(0.0f, 1.0f, 0.0f, m_fAngle) * move;
            m_shadeToon->SetModelView(trans);

            m_shadeToon->SetMatAmbient(marron / 4);
            m_shadeToon->SetMatDiffuse(marron / 2);
            m_teapot->Draw(*m_shadeToon, -1, true);
        }

        // Rabbit
        {
            // Scale, translate, rotate and translate again
            mat4 sca;
            sca.scale(25.0, 25.0, 25.0);
            m_objHomer->Move(vec3(0.0, 2.0, 9.0));
            mat4 trans = Camera::get().GetModelView() * mat4(m_objHomer->GetTranslation()) * mat4(0.0f, 1.0f, 0.0f, m_fAngle) * move * sca;
            m_shadeToon->SetModelView(trans);

            m_shadeToon->SetMatAmbient(jaune / 4);
            m_shadeToon->SetMatDiffuse(jaune / 2);
            m_objHomer->Draw(*m_shadeToon, -1, true);
        }

        m_shadeToon->Deactivate();
    }
}
