#pragma once

#include "Util.h"
#include <string>
#include <SDL.h>

#include "Mathlib.h"

class SplineGL;
class FontString;

// Classe de base abstraite des sc�nes
class SceneBase
{
public:
    // Initialisation des donn�es propres � la sc�ne
    virtual void Init() {}

    // Evolution des donn�es et traitement des commandes synchones
    virtual void Idle(float fElapsedTime) {}

    // Traitement des commandes asynchones
    virtual void Keyboard(bool special, unsigned char key);

    // D�marrage de la sc�ne
    virtual void Reset();

    // Pr�-affichage de la sc�ne (peut utiliser des FBO)
    virtual void PreRender() {}

    // Affichage de la sc�ne (ne peut pas utiliser des FBO)
    virtual void Render() = 0;

    // On met � jour la position dans les splines de cam�ra
    void InterpolCameraTraj(float fElapsedTime);

    // Affiche la trajectoire de la cam�ra
    void DrawTraj(Shader &shader);

    virtual std::string GetStats() {return "";}

    SceneBase();
    virtual ~SceneBase();

private:
    SceneBase(const SceneBase& sc) {}
    SceneBase &operator=(const SceneBase& sc)
    {
        return (*this);
    }

public:
    // Display a text on the display
    static void DrawString(Shader &shader, FontString& fontStr);

    // Load camera trajectory from file
    bool LoadCameraTrajFromFile(const std::string& name);

    // Save the camera trajectory to file
    bool SaveCameraTrajInFile(const std::string& name);

    inline SplineGL *getCamEyeSpline()	const
    {
        return m_pCamEyeTraj;
    }

protected:
    SplineGL*		m_pCamEyeTraj;
    SplineGL*		m_pCamLookAtTraj;
};
