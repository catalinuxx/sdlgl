#include <sstream>

#include "SceneTerrain.h"

#include "Mathlib.h"
#include "../Camera.h"
#include "../Mesh.h"
#include "../Timer.h"
#include "../Sky.h"
#include "../Frustum.h"
#include "Util.h"

#include "../Shader.h"
#include "../TextureCubemap.h"
#include "../Texture2D.h"
#include "../FrameBufferObject.h"
#include "../Inputs.h"

#include "../ResourceManager.h"
#include "../VarManager.h"
#include "../ImageTools.h"
#include "../Terrain/Terrain.h"
#include "../Terrain/TerrainChunk.h"

bool useWater = false;

void SceneTerrain::Init()
{
    ResourceManager& res = ResourceManager::get();

    m_vSunAngle = vec2(0.0f, RADIANS(45.0f));

    m_pSkybox = (TextureCubemap*)res.LoadResource(ResourceManager::TEXTURECUBEMAP, "sand_up.jpg sand_dn.jpg sand_tp.jpg sand_bm.jpg sand_lt.jpg sand_rt.jpg");

    //_shaders.push_back(m_pShaderLighting = (Shader*)res.LoadResource(ResourceManager::SHADER, "lighting1"));
    _shaders.push_back(m_pShaderTerrain	= (Shader*)res.LoadResource(ResourceManager::SHADER, "terrain_ground1"));
    //_shaders.push_back(m_pShaderWater = (Shader*)res.LoadResource(ResourceManager::SHADER, "terrain_water1"));
    //_shaders.push_back(m_pShaderGrass = (Shader*)res.LoadResource(ResourceManager::SHADER, "terrain_grass1"));
    _shaders.push_back(m_pShaderTree = (Shader*)res.LoadResource(ResourceManager::SHADER, "terrain_tree1"));

    res.LoadResource(ResourceManager::MESH, "terrain_house.obj");

    res.LoadResource(ResourceManager::TEXTURE2D, "wall_diffuse.jpg");
    res.LoadResource(ResourceManager::TEXTURE2D, "wall_NM_height.tga");

    m_tTextures.push_back((Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "terrain_detail_NM.tga"));
    m_tTextures.push_back((Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "sandfloor009a.jpg"));
    m_tTextures.push_back((Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "terrain_rocky_map_1024.png"));
    m_tTextures.push_back((Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "terrain_grass_map_1024.png"));
    m_tTextures.push_back((Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "terrain_water_caustics.jpg"));


    res.LoadResource(ResourceManager::TEXTURE2D, "grass_billboards.tga");
    res.LoadResource(ResourceManager::TEXTURE2D, "palm_texture.tga");


    m_pTexWaterNoiseNM = (Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "terrain_water_NM.jpg");
    m_pTerrainDiffuseMap = (Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "test_heightmap512_2_diffusemap.jpg");

    //var.set("water_height", 2.2f);

    m_pTerrain = new Terrain();
    assert(m_pTerrain != NULL);
    //BoundingBox bbox( vec3(-300.0f, 0.0f, -300.0f), vec3(300.0f, 40.0f, 300.0f) );
    BoundingBox bbox( vec3(-30.0f, 0.0f, -30.0f), vec3(30.0f, 10.0f, 30.0f) );
    m_pTerrain->Load("textures/test_heightmap512_2.jpg", bbox, 16);
    {
        ImageTools::ImageData img;
        ImageTools::OpenImage("textures/test_heightmap512_2_diffusemap.jpg", img);
        m_pTerrain->GenerateGrass(img, 20/*000*/);
        m_pTerrain->GenerateVegetation(img, 10);
        m_pTerrain->ComputeBoundingBox();
    }

    vec3 fogColor(0.7f, 0.7f, 0.9f);
    if (m_pShaderTerrain && m_pShaderTerrain->Activate(Camera::get().GetModelView(), Camera::get().GetProjection()))
    {
        m_pShaderTerrain->Uniform("bbox_min", m_pTerrain->getBoundingBox().min);
        m_pShaderTerrain->Uniform("bbox_max", m_pTerrain->getBoundingBox().max);
        m_pShaderTerrain->Uniform("fog_color", fogColor);
        m_pShaderTerrain->Deactivate();
    }

    if (m_pShaderWater && m_pShaderWater->Activate(Camera::get().GetModelView(), Camera::get().GetProjection()))
    {
        m_pShaderWater->Uniform("bbox_min", m_pTerrain->getBoundingBox().min);
        m_pShaderWater->Uniform("bbox_max", m_pTerrain->getBoundingBox().max);
        m_pShaderWater->Uniform("fog_color", fogColor);
        m_pShaderWater->Deactivate();
    }

    if (useWater)
    m_fboWaterReflection.Create(FrameBufferObject::FBO_2D_COLOR, 512, 512, "WaterReflection");

    _waterVbo.getPosition() =
    {
        {0,0,0},
        {0,0,0},
        {0,0,0},
        {0,0,0},
    };
    _waterVbo.Create(GL_DYNAMIC_DRAW);

    LoadCameraTrajFromFile("terrain_1.txt");
}

SceneTerrain::~SceneTerrain()
{
    delete m_pTerrain;
    m_tTextures.clear();
}

void SceneTerrain::Idle(float fElapsedTime)
{
    Inputs& inputs = Inputs::get();

    if ( inputs.Key('z') )			m_vSunAngle.y += 0.01f;
    if ( inputs.Key('s') )			m_vSunAngle.y -= 0.01f;
    if ( inputs.Key('q') )			m_vSunAngle.x += 0.01f;
    if ( inputs.Key('d') )			m_vSunAngle.x -= 0.01f;

    m_vSunVector = vec4(	-cosf(m_vSunAngle.x) * sinf(m_vSunAngle.y),
                            -cosf(m_vSunAngle.y),
                            -sinf(m_vSunAngle.x) * sinf(m_vSunAngle.y),
                            0.0f );
}

void SceneTerrain::Keyboard(bool special, unsigned char key)
{
    SceneBase::Keyboard(special, key);
}


void SceneTerrain::Reset()
{
    Camera::get().setEye(vec3(0.0f, 0.0f, 0.0f));
    SceneBase::Reset();
}

void SceneTerrain::PreRender()
{
    Frustum& frust = Frustum::get();
    //VarManager& var = VarManager::get();

    vec3 eye_pos = Camera::get().getEye();
    vec3 sun_pos = eye_pos - vec3(m_vSunVector);
    //GLfloat tabOrtho[TERRAIN_SHADOWMAPS_COUNT] = {20.0, 100.0};

////	float sizey = fabs(sinf(m_vSunAngle.y)) + 1.0f;
    //float sizey = 1.0f;

    // On place la cam�ra � la lumi�re et on r�cup�re les matrices
    vec3 eye = Camera::get().getEye();
    //	eye.y = m_pTerrain->getPosition(eye.x, eye.z).y;
    mat4 modelView = Camera::get().GetModelView();
    modelView.look_at(vec3(eye.x - 500 * m_vSunVector.x,		eye.y - 500 * m_vSunVector.y,		eye.z - 500 * m_vSunVector.z),
       eye, vec3(0.0, 1.0, 0.0));
    //Frustum::get().Extract(vec3(-m_vSunVector));

    //mat4 projection = Camera::get().projection;

    // Recover the ModelView and Projection of the light
	//Frustum::get().Extract(sun_pos);
    //Camera::get().projection = mat4::orthoS(-1.0, 1.0, -1.0, 1.0, (GLfloat)var.getf("cam_znear"), (GLfloat)var.getf("cam_zfar"));
    Frustum::get().Extract(modelView, Camera::get().GetProjection(), sun_pos);
    mat4& matLightMV = frust.getModelviewMatrix();
    mat4& matLightProj = frust.getProjectionMatrix();
    m_matSunModelviewProj = matLightProj * matLightMV;

    // Restore
    Frustum::get().Extract(Camera::get().GetModelView(), Camera::get().GetProjection(), eye_pos);

    if (!useWater) return;
    // Display the scene reflected on a FBO
    m_fboWaterReflection.Begin();
    GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
    RenderEnvironment(true);
    m_fboWaterReflection.End();
}

void SceneTerrain::Render()
{
    // Display normal scene
    RenderEnvironment(false);

    // Display water surface
    if (useWater) RenderWaterSurface();


#ifndef HAVE_GLES
    /**/
    static FontString _drawnChunksStr, _reflectedChunksStr;
    VarManager& var = VarManager::get();
    // Text display
    std::stringstream strStream;
    strStream << var.geti("terrain_chunks_drawn") << " chunks";
    DrawString({1.0f, 60.0f}, _drawnChunksStr, strStream.str());
    strStream.str("");
    strStream << var.geti("terrain_chunks_reflected_drawn") << " reflected chunks";
    DrawString({1.0f, 30.0f}, _reflectedChunksStr, strStream.str());
    /**/
#endif // HAVE_GLES
}

// Rendu de la surface de l'eau
void SceneTerrain::RenderWaterSurface()
{
    VarManager& var = VarManager::get();
    float h = var.getf("water_height");

    GL_CHECK(glDisable(GL_CULL_FACE));
    GL_CHECK(glEnable(GL_BLEND));

    mat4 modelView = Camera::get().GetModelView();
    mat4 projection = Camera::get().GetProjection();

    if (m_pShaderWater && m_pShaderWater->Activate(modelView, projection))
    {
        m_pShaderWater->SetTextureMatrix(m_matSunModelviewProj);
        m_fboWaterReflection.Bind(0);
        m_pShaderWater->UniformTexture("texWaterReflection", 0);

        m_pTexWaterNoiseNM->Bind(1);
        m_pShaderWater->UniformTexture("texWaterNoiseNM", 1);

        m_pShaderWater->Uniform("win_width", var.geti("win_width"));
        m_pShaderWater->Uniform("win_height", var.geti("win_height"));
        m_pShaderWater->Uniform("noise_tile", 10.0f);
        m_pShaderWater->Uniform("noise_factor", 0.1f);
        m_pShaderWater->Uniform("time", Timer::get().getCurrentTime());
        m_pShaderWater->Uniform("water_shininess", 50.0f);

        vec3 e = Camera::get().getEye();
        float d = 2.0f * var.getf("cam_zfar");
        //float d = 0.02f * var.getf("cam_zfar");

        _waterVbo.getPosition() =
        {
            {e.x - d, h, e.z - d},
            {e.x - d, h, e.z + d},
            {e.x + d, h, e.z + d},
            {e.x + d, h, e.z - d},
        };
        _waterVbo.Enable(*m_pShaderWater);
        _waterVbo.BufferPosition();
        GL_CHECK(glDrawArrays(GL_TRIANGLE_FAN, 0, 4));
        _waterVbo.Disable(*m_pShaderWater);

        m_pTexWaterNoiseNM->Unbind(1);
        m_fboWaterReflection.Unbind(0);

        m_pShaderWater->Deactivate();
    }

    for (auto &item : _shaders)
        if (item && item->Activate(modelView, projection))
        {
            item->SetTextureMatrix(mat4());
            item->Deactivate();
        }
}


// Rendu de l'environnement (pour la r�flexion de l'eau ou non)
void SceneTerrain::RenderEnvironment(bool bWaterReflection)
{
    ResourceManager& res = ResourceManager::get();
    VarManager& var = VarManager::get();
    Camera& cam = Camera::get();

    mat4 modelView = Camera::get().GetModelView();
    mat4 projection = Camera::get().GetProjection();

    vec4 white(1.0f, 1.0f, 1.0f, 1.0f);
    vec4 black(0.0f, 0.0f, 0.0f, 1.0f);
    vec4 orange(1.0f, 0.5f, 0.0f, 1.0f);
    vec4 yellow(1.0f, 1.0f, 0.8f, 1.0f);
    float sun_cosy = cosf(m_vSunAngle.y);

    vec4 vSunColor = white.lerp(orange, yellow, 0.25f + sun_cosy * 0.75f);

    if (useWater)
    {
    if (cam.getEye().y < var.getf("water_height"))
    {
        bWaterReflection = false;
        var.set("enable_underwater", true);
    }
    else
    {
        var.set("enable_underwater", false);
    }
    }


    // Affichage du ciel et du soleil
    Sky::get().DrawSkyAndSun( Camera::get().getEye(), vec3(m_vSunVector), m_pSkybox->getHandle(), bWaterReflection );

    // Si c'est pour la r�flexion, on dessine la sc�ne � l'envers
    if (bWaterReflection)
    {
        GL_CHECK(glDisable(GL_CULL_FACE));
        //TODO
        mat4 sca;
        sca.scale(-1.0f, 1.0f, 1.0f);
        modelView = sca;
        cam.RenderLookAt(modelView, projection, true, var.getf("water_height"));
    }

    vec3 zeros(0.0f, 0.0f, 0.0f);
    for (auto &item : _shaders)
        if (item && item->Activate(modelView, projection))
        {
            item->SetLight(vec3(m_vSunVector), white, vSunColor, vSunColor, zeros);
            item->SetTextureMatrix(m_matSunModelviewProj);
            item->Deactivate();
        }

    // Display house
    if (m_pShaderLighting && m_pShaderLighting->Activate(modelView, projection))
    {
        m_pShaderLighting->SetTextureMatrix(m_matSunModelviewProj);
        m_pShaderLighting->SetMatAmbient(white / 6);
        m_pShaderLighting->SetMatDiffuse(white);

        res.getTexture2D("wall_diffuse.jpg")->Bind(0);
        res.getTexture2D("wall_NM_height.tga")->Bind(1);

        m_pShaderLighting->UniformTexture("texDiffuse", 0);
        m_pShaderLighting->UniformTexture("texNormalHeightMap", 1);
        m_pShaderLighting->Uniform("mode", 1);
        m_pShaderLighting->Uniform("enable_shadow_mapping", 0);
        m_pShaderLighting->Uniform("tile_factor", 2.0f);

        m_pShaderLighting->SetModelView(modelView * mat4(172.0f, 5.18f, 175.2f));
        res.getMesh("terrain_house.obj")->Draw(*m_pShaderLighting);

        m_pShaderLighting->SetModelView(modelView * mat4(19.0f, 35.8f, -123.1f));
        res.getMesh("terrain_house.obj")->Draw(*m_pShaderLighting);

        m_pShaderLighting->SetModelView(modelView * mat4(1.338f, 18.02f, 259.0f));
        res.getMesh("terrain_house.obj")->Draw(*m_pShaderLighting);

        m_pShaderLighting->SetModelView(modelView * mat4(178.6f, 32.42f, 26.31f));
        res.getMesh("terrain_house.obj")->Draw(*m_pShaderLighting);

        res.getTexture2D("wall_NM_height.tga")->Unbind(1);
        res.getTexture2D("wall_diffuse.jpg")->Unbind(0);

        m_pShaderLighting->Deactivate();
    }

    vec4 vGroundAmbient = white.lerp(white * 0.2f, black, sun_cosy);
    for (auto &item : _shaders)
        if (item && item->Activate(modelView, projection))
        {
            item->SetMatAmbient(vGroundAmbient);
            item->SetMatDiffuse(white);
            item->SetMatSpecular(white);
            item->SetMatShininess(50.0f);
            item->Deactivate();
        }

    // Activation des textures
    GLuint idx = 0;
    for (GLuint i = 0; i < m_tTextures.size(); i++)
        m_tTextures[i]->Bind(idx++);
    m_pTerrainDiffuseMap->Bind(idx++);

    // Affichage du terrain
    if (m_pShaderTerrain && m_pShaderTerrain->Activate(modelView, projection))
    {
        m_pShaderTerrain->SetTextureMatrix(m_matSunModelviewProj);

        //m_pShaderTerrain->Uniform("detail_scale", 120.0f);
        //m_pShaderTerrain->Uniform("diffuse_scale", 70.0f);
        m_pShaderTerrain->Uniform("detail_scale", 1.0f);
        m_pShaderTerrain->Uniform("diffuse_scale", 1.0f);

        m_pShaderTerrain->Uniform("water_height", var.getf("water_height"));
        m_pShaderTerrain->Uniform("water_reflection_rendering", bWaterReflection);

        m_pShaderTerrain->Uniform("time", Timer::get().getCurrentTime());

        m_pShaderTerrain->UniformTexture("texNormalHeightMap", 0);
        m_pShaderTerrain->UniformTexture("texDiffuse0", 1);
        m_pShaderTerrain->UniformTexture("texDiffuse1", 2);
        m_pShaderTerrain->UniformTexture("texDiffuse2", 3);
        m_pShaderTerrain->UniformTexture("texWaterCaustics", 4);
        m_pShaderTerrain->UniformTexture("texDiffuseMap", 5);

        m_pShaderTerrain->Uniform("depth_map_size", 512);

        int ret = m_pTerrain->DrawGround(*m_pShaderTerrain, bWaterReflection);
        var.set(bWaterReflection ? "terrain_chunks_reflected_drawn" : "terrain_chunks_drawn", ret);

        if (useWater) m_pTerrain->DrawInfinitePlane(*m_pShaderTerrain, Camera::get().getEye(), 2.0f * var.getf("cam_zfar"));
        m_pShaderTerrain->Deactivate();
    }

    m_pTerrainDiffuseMap->Unbind(--idx);
    for (GLint i = (GLint)m_tTextures.size() - 1; i >= 0; i--)
        m_tTextures[i]->Unbind(--idx);

    // Affichage de l'herbe
    if (!bWaterReflection)
    {
        GL_CHECK(glEnable(GL_BLEND));
        if (m_pShaderGrass && m_pShaderGrass->Activate(modelView, projection))
        {
            m_pShaderGrass->SetTextureMatrix(m_matSunModelviewProj);
            res.getTexture2D("grass_billboards.tga")->Bind(0);

            m_pShaderGrass->UniformTexture("texDiffuse", 0);
            m_pShaderGrass->Uniform("depth_map_size", 512);

            m_pShaderGrass->Uniform("time", Timer::get().getCurrentTime());
            m_pShaderGrass->Uniform("lod_metric", TERRAIN_GRASS_MAX_DISTANCE);

            m_pTerrain->DrawGrass(*m_pShaderGrass, bWaterReflection);

            res.getTexture2D("grass_billboards.tga")->Unbind(0);

            m_pShaderGrass->Deactivate();
        }
        GL_CHECK(glDisable(GL_BLEND));
    }

    for (auto &item : _shaders)
        if (item && item->Activate(modelView, projection))
        {
            item->SetTextureMatrix(mat4());
            item->Deactivate();
        }

    if (m_pShaderTree && m_pShaderTree->Activate(modelView, projection))
    {
        m_pShaderTree->SetTextureMatrix(Frustum::get().getModelviewInvMatrix());

        GLuint idx = 0;
        res.getTexture2D("palm_texture.tga")->Bind(idx++);

        m_pShaderTree->UniformTexture("texDiffuse", 0);

        m_pShaderTree->Uniform("time", Timer::get().getCurrentTime());

        m_pTerrain->DrawObjects(*m_pShaderTree, modelView, bWaterReflection);

        res.getTexture2D("palm_texture.tga")->Unbind(--idx);

        m_pShaderTree->SetTextureMatrix(mat4());
        m_pShaderTree->Deactivate();
    }
}
