#include <algorithm>
#include "SceneManager.h"
#include "ResourceManager.h"
#include "SceneBase.h"
#include "SceneTestParallax.h"
#include "SceneTestRefraction.h"
#include "SceneTestFur.h"
#include "SceneTerrain.h"
#include "SceneSimple.h"
#include "SceneTestShadowMapping.h"
#include "SceneToon.h"
#include "SceneCar.h"
#include "SceneKinect.h"
#include "../Camera.h"
#include "../VarManager.h"
#include <assert.h>
#include "Util.h"

const std::map<std::string, std::function<SceneBase *()>> SceneManager::Scenes =
{
    {"terrain", []() { return new SceneTerrain(); } },
    {"test_parallax", []() { return new SceneTestParallax(); } },
    {"test_refraction", []() { return new SceneTestRefraction(); } },
    {"test_fur", []() { return new SceneTestFur(); } },
    {"simple", []() { return new SceneSimple(); } },
    {"test_shadowmapping", []() { return new SceneTestShadowMapping(); } },
    {"toon", []() { return new SceneToon();} },
    {"car", []() { return new SceneCar(); } },
    {"kinect", []() { return new SceneKinect(); } },
};

SceneManager::SceneManager()
{
    VarManager& var = VarManager::get();

    LOG_INFO("Scenes: %s\n", var.gets("scenes").c_str());

    auto scenes = Util::Split(var.gets("scenes"), ',');
    for (auto str : scenes)
    {
        auto search = Scenes.find(str);
        if (search == Scenes.end())
            throw std::runtime_error("Scene " + str + " could not be found");
        m_SceneDB.push_back(std::make_pair(str, search->second()));
    }
}

bool SceneManager::setCurrent(const std::string& name)
{
    auto ptr = getScenePointer(name);
    if (!ptr)
    {
        std::cerr << "Could not set current theme to " << name << std::endl;
        return false;
    }
    if (m_pCurrentScene != ptr)
    {
        m_pCurrentScene = ptr;
        m_pCurrentScene->Reset();
    }
    // Scene has changed, time to reset script
    _script.ResetError();
    return true;
}

bool SceneManager::setCurrent(std::size_t i)
{
    if (i >= m_SceneDB.size())
    {
        std::cerr << "Could not set current to index " << i << std::endl;
        return false;
    }
    return setCurrent(m_SceneDB[i].first);
}

SceneBase* SceneManager::getScenePointer(const std::string& name)
{
    auto it = std::find_if(m_SceneDB.begin(), m_SceneDB.end(), [&name](SceneDBPair & item)
    {
        return item.first == name;
    } );
    if (it == m_SceneDB.end())
        return nullptr;
    return it->second;
}

void SceneManager::Init()
{
    for (auto pair : m_SceneDB)
        pair.second->Init();
    if (m_SceneDB.size() > 0)
        setCurrent(0);
}

void SceneManager::Clear()
{
    if (m_SceneDB.size())
    {
        for (auto pair : m_SceneDB)
        {
            delete pair.second;
            pair.second = nullptr;
        }
        m_SceneDB.clear();
        LOG_INFO("Done deleting scenes");
    }
}

SceneManager::~SceneManager()
{
    Clear();
}

void SceneManager::Idle(float fElapsedTime)
{
    if (Camera::get().getType() == Camera::DRIVEN)
    {
        _script.Idle(fElapsedTime);
        m_pCurrentScene->InterpolCameraTraj(fElapsedTime);
    }
    m_pCurrentScene->Idle(fElapsedTime);
}

void SceneManager::Keyboard(bool special, unsigned char key)
{
    m_pCurrentScene->Keyboard(special, key);
}

void SceneManager::PreRender()
{
    m_pCurrentScene->PreRender();
}

void SceneManager::Render()
{
    VarManager& var = VarManager::get();

    m_pCurrentScene->Render();

    if (var.getb("show_camera_splines"))
    {
        Shader *shader = ResourceManager::get().GetDefaultShader();
        if (shader && shader->Activate(Camera::get().GetModelView(), Camera::get().GetProjection()))
        {
            m_pCurrentScene->DrawTraj(*shader);
            shader->Deactivate();
        }
    }
}

