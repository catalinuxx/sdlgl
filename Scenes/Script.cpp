#include "SceneManager.h"
#include "SceneBase.h"
#include "SceneTestParallax.h"
#include "SceneTestRefraction.h"
#include "SceneTestFur.h"
#include "SceneTerrain.h"
#include "SceneSimple.h"
#include "SceneTestShadowMapping.h"
#include "SceneToon.h"
#include "../VarManager.h"
#include <assert.h>
#include "../Spline3D/SplineGL.h"
#include "Script.h"

void Script::Idle(float fElapsedTime)
{
    SceneManager& manag = SceneManager::get();
    SceneBase* pScene = manag.getCurrentScenePointer();
    assert(pScene);

    static const std::string tSceneName[8] = {"terrain", "test_shadowmapping", "terrain", "test_refraction", "terrain", "toon", "terrain", "test_fur"};

    switch (_current)
    {
    case -2:
        // Error state, nothing to do ...
        break;
    case -1:
    {
        if (manag.setCurrent(tSceneName[0]))
        {
            SceneTerrain* pTerrain = dynamic_cast<SceneTerrain *>(manag.getCurrentScenePointer());
            pTerrain->LoadCameraTrajFromFile("terrain_1.txt");
            pTerrain->setSunAngle(vec2(0.0f, RADIANS(45.0f)));
            _current = 0;
        }
        else
            SceneError();
        break;
    }
    case 0:
    {
        if (manag.setCurrent(tSceneName[0]))
        {
            if (pScene->getCamEyeSpline()->isFinished())
            {
                if (manag.setCurrent(tSceneName[1]))
                    _current = 1;
                else
                    SceneError();
            }
        }
        else
            SceneError();
        break;
    }
    case 1:
    {
        if (manag.setCurrent(tSceneName[1]))
        {
            if (pScene->getCamEyeSpline()->isFinished())
            {
                if (manag.setCurrent(tSceneName[2]))
                {
                    SceneTerrain* pTerrain = dynamic_cast<SceneTerrain *>(manag.getCurrentScenePointer());
                    pTerrain->LoadCameraTrajFromFile("terrain_2.txt");
                    pTerrain->setSunAngle(vec2(0.0f, RADIANS(-30.0f)));
                    _current = 2;
                }
                else
                    SceneError();
            }
        }
        else
            SceneError();
        break;
    }

    case 2:
    {
        if (manag.setCurrent(tSceneName[2]))
        {
            if (pScene->getCamEyeSpline()->isFinished())
            {
                if (manag.setCurrent(tSceneName[3]))
                    _current = 3;
                else
                    SceneError();
            }
        }
        else
            SceneError();
        break;
    }

    case 3:
    {
        if (manag.setCurrent(tSceneName[3]))
        {
            if (pScene->getCamEyeSpline()->isFinished())
            {
                if (manag.setCurrent(tSceneName[4]))
                {
                    SceneTerrain* pTerrain = dynamic_cast<SceneTerrain *>(manag.getCurrentScenePointer());
                    pTerrain->LoadCameraTrajFromFile("terrain_3.txt");
                    pTerrain->setSunAngle(vec2(RADIANS(-5.0f), RADIANS(71.0f)));
                    _current = 4;
                }
                else
                    SceneError();
            }
        }
        else
            SceneError();
        break;
    }

    case 4:
    {
        if (manag.setCurrent(tSceneName[4]))
        {
            if (pScene->getCamEyeSpline()->isFinished())
            {
                if (manag.setCurrent(tSceneName[5]))
                    _current = 5;
                else
                    SceneError();
            }
        }
        else
            SceneError();
        break;
    }

    case 5:
    {
        if (manag.setCurrent(tSceneName[5]))
        {
            if (pScene->getCamEyeSpline()->isFinished())
            {
                if (manag.setCurrent(tSceneName[6]))
                {
                    SceneTerrain* pTerrain = dynamic_cast<SceneTerrain *>(manag.getCurrentScenePointer());
                    pTerrain->LoadCameraTrajFromFile("terrain_4.txt");
                    pTerrain->setSunAngle(vec2(RADIANS(50.0f), RADIANS(-65.0f)));
                    _current = 6;
                }
                else
                    SceneError();
            }
            else
                SceneError();
        }
        break;
    }

    case 6:
    {
        if (manag.setCurrent(tSceneName[6]))
        {
            if (pScene->getCamEyeSpline()->isFinished())
            {
                if (manag.setCurrent(tSceneName[7]))
                    _current = 7;
                else
                    SceneError();
            }
        }
        else
            SceneError();
        break;
    }

    case 7:
    {
        if (manag.setCurrent(tSceneName[7]))
        {
            if (pScene->getCamEyeSpline()->isFinished())
            {
                if (manag.setCurrent(tSceneName[0]))
                {
                    SceneTerrain* pTerrain = dynamic_cast<SceneTerrain *>(manag.getCurrentScenePointer());
                    pTerrain->LoadCameraTrajFromFile("terrain_1.txt");
                    pTerrain->setSunAngle(vec2(0.0f, RADIANS(45.0f)));
                    _current = 0;
                }
                else
                    SceneError();
            }
        }
        else
            SceneError();
        break;
    }

    default:
        assert(0);
    }
}

void Script::SceneError()
{
    _current = -2;
}

void Script::ResetError()
{
    if (_current == -2)
        _current = -1;
}
