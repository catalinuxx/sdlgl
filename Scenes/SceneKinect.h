#pragma once

#include "SceneBase.h"
#include "Kinect.h"
#include "Skeleton.h"

class Texture2D;

class SceneKinect : public SceneBase
{
public:
    virtual void Init();

    virtual ~SceneKinect();

    virtual void Idle(float fElapsedTime);

    virtual void Reset();

    virtual void Render();

    void Keyboard(bool special, unsigned char key);

    std::string GetStats();

private:
    float		m_fAngle;
    int         _group = -1;
    bool        _rotate = true;
    bool        _switchGroup = false;
    Mesh        *_pMesh = nullptr;
    Shader      *_pShader = nullptr;
    TextureCubemap *_pSkybox = nullptr;
    Kinect      _kinect;
};
