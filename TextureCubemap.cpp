#include "Util.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "TextureCubemap.h"

bool TextureCubemap::Load(const std::string& name, const std::string &defaultPath)
{
    if (!Texture::Load(name))
        return false;

    Bind();

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);

    if (!Texture::s_bGenerateMipmaps)
    {
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
    else
    {
        glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
#ifndef HAVE_GLES
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_GENERATE_MIPMAP, GL_TRUE);
#endif // HAVE_GLES
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
#ifndef HAVE_GLES
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
#endif // HAVE_GLES

    if (!name.empty())
    {
        int i = 0;
        std::stringstream ss(name);
        std::string it;
        while (std::getline(ss, it, ' '))
        {
            if (i >= 6)
            {
                std::cerr << "ERROR : Too many textures for cubemap " << name << ". Should be only 6." << std::endl;
                return false;
            }
            if (!LoadFile(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, Util::IsFilename(it) ? defaultPath + it : it))
                return false;
            i++;
        }
    }

    Unbind();

    return true;
}



