#pragma once

#include <iostream>
#include <map>
#include "Util.h"
#include "Mathlib.h"
#include "IResource.h"

// -------------------------------
// Shader (vertex & pixel)
// -------------------------------

class Shader : public IResource
{
public:
    static bool Init();
    bool Load(const std::string& name, const std::string& extensions = "");
    bool Activate(const mat4& modelView, const mat4 &projection);
    static void Deactivate();

    GLuint GetProgramId() { return m_nProgram; }

    static Shader* getActivatedShader()
    {
        return s_pCurrent;
    }
    /*
    	static bool isExtensionSupported(const std::string& ext);
    	static void PrintSupportedExtensions();*/

    // Set shader variables
    void Uniform(const std::string& ext, GLint value);
    void Uniform(const std::string& ext, GLfloat value);
    void Uniform(const std::string& ext, const vec2& value);
    void Uniform(const std::string& ext, const vec3& value);
    void Uniform(const std::string & ext, const vec4& value);
    void Uniform(const std::string & ext, const mat3& value);
    void Uniform(const std::string & ext, const mat4& value);
    void Uniform(const std::string &ext, const glm::vec2 &value);
    void Uniform(const std::string &ext, const glm::vec3 &value);
    void Uniform(const std::string &ext, const glm::vec4 &value);
    void Uniform(const std::string &ext, const glm::mat4 &value);
    void Uniform(const std::string &ext, const bool &value);
    void UniformTexture(const std::string& ext, GLint slot);

    GLint GetUniformLocation(const std::string &ext);
    GLint GetAttribLocation(const std::string &ext);

    void SetTexActive(bool val, int slot = 0);
    bool &GetTexActive() { return _texactive; }
    void SetTexInv(bool val, int slot = 0);
    bool &GetTexInv() { return _texinv; }
    int  GetTexSlot() { return _tex; }
    void SetModelView(const mat4 &modelView);
    mat4 &GetModelView();
    void SetNormalMatrix(const mat4 &modelView);
    void SetProjection(const mat4 &projection);
    void SetMatAmbient(const vec4 &color) { Uniform(MAT_AMBIENT_UNIFORM, color); }
    void SetMatDiffuse(const vec4 &color) { Uniform(MAT_DIFFUSE_UNIFORM, color); }
    void SetMatSpecular(const vec4 &color) { Uniform(MAT_SPECULAR_UNIFORM, color); }
    void SetMatEmission(const vec4 &color) { Uniform(MAT_EMISSION_UNIFORM, color); }
    void SetMatShininess(const GLfloat &val) { Uniform(MAT_SHININESS_UNIFORM, val); }
    void SetLightPosition(const vec3 &pos) { Uniform(LIGHT_POSITION_UNIFORM, pos); }
    void SetLight(const vec3 &pos, const vec4 &ambient, const vec4 &diffuse, const vec4 &specular,
        const vec3 &spotDirection)
    {
        Uniform(LIGHT_POSITION_UNIFORM, pos);
        Uniform("LightAmbient", ambient);
        Uniform("LightDiffuse", diffuse);
        Uniform("LightSpecular", specular);
        Uniform("LightSpotDirection", spotDirection);
    }
    void SetTextureMatrix(const mat4 &mat)
    {
        Uniform("TextureMatrix", mat);
    }

    bool HasNormal() { return _normalPos >= 0; }
    bool HasTangent() { return _tangentPos >= 0; }
    bool HasColor() { return _colorPos >= 0; }
    bool HasTexCoord() { return _texcoordPos >= 0; }
    GLint GetVertexPos() { assert(_vertexPos >= 0); return _vertexPos; }
    GLint GetNormalPos() { assert(_normalPos >= 0); return _normalPos; }
    GLint GetTangentPos() { assert(_tangentPos >= 0); return _tangentPos; }
    GLint GetColorPos() { assert(_colorPos >= 0) ;return _colorPos; }
    GLint GetTexCoordPos() { assert(_texcoordPos >= 0); return _texcoordPos; }

    Shader() : IResource()
    {
        m_nProgram = 0;
        m_strName = "undef";
    }

    mat4 modelView, projection;

private:
    static GLuint loadShader(const std::string& filestr);
    static bool compileShader(GLuint object, const std::string &name);
    static GLuint linkShaders(GLuint* object, const unsigned int& nb, const std::string &name);

private:
    static bool		s_bInitialized;
    static Shader*	s_pCurrent;
    std::string		m_strName;
    GLuint		    m_nProgram;

    GLint _vertexPos = -1, _texcoordPos = -1, _colorPos = -1, _normalPos = -1, _tangentPos = -1;
    GLint _normalMatrixPos = -1;

    bool _texactive = false, _texactiveSet = false;
    bool _texinv = false, _texinvSet = false;
    int  _tex = 0;

    std::map<std::string, GLint> _locations;
};
