#pragma once

#include <iostream>
#include <map>
#include <string>
#include <assert.h>

#include "Singleton.h"
#include "IResource.h"
#include "Texture2D.h"
#include "TextureCubemap.h"
#include "Mesh.h"
#include "Shader.h"
#include "Font.h"

class ResourceManager : public Singleton<ResourceManager>
{
public:
    enum RES_TYPE {TEXTURE2D, TEXTURECUBEMAP, MESH, SHADER, FONT};

    IResource* LoadResource(RES_TYPE type, const std::string& name, GLenum usage = GL_STATIC_DRAW);
    IResource* NewResource(IResource* data, const std::string& name);

    inline IResource* getResource(const std::string& name)
    {
        if (m_ResDB.find(name) == m_ResDB.end())
        {
            LOG_WARN("Could not find resource %s", name.c_str());
            assert(m_ResDB.find(name) != m_ResDB.end());
            return nullptr;
        }
        return m_ResDB.find(name)->second;
    }
    inline Texture2D* getTexture2D(const std::string& name)
    {
        return (Texture2D*)getResource(name);
    }
    inline TextureCubemap* getTextureCubemap(const std::string& name)
    {
        return dynamic_cast<TextureCubemap*>(getResource(name));
    }
    inline Mesh* getMesh(const std::string& name)
    {
        return dynamic_cast<Mesh*>(getResource(name));
    }
    inline Shader* getShader(const std::string& name)
    {
        return dynamic_cast<Shader*>(getResource(name));
    }
    inline Font *getFont(const std::string& name)
    {
        return dynamic_cast<Font*>(getResource(name));
    }

    void Init();
    void Clear();

    Font *GetDefaultFont() { return _defaultFont; }
    Shader *GetDefaultShader() { return _defaultShader; }

private:
    friend class Singleton<ResourceManager>;
    virtual ~ResourceManager();

    std::map<std::string, IResource *>	m_ResDB;

    Font *_defaultFont = nullptr;
    Shader *_defaultShader = nullptr;
};
