#include "Util.h"

#ifndef ANDROID
#include <windows.h> // for Sleep
#else
#include <unistd.h> // for usleep
#endif // ANDROID

#include <sstream>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <SDL_rwops.h>

#include "ResourceManager.h"
#include "Camera.h"

Util::Util()
{
}

Util::~Util()
{
}

void Util::Init()
{
    {
        auto &tPosition	= _axisVbo.getPosition();
        auto &tColor	= _axisVbo.getColor();
        tPosition = {
            {0.0f, 0.0f, 0.0f},
            {1.0f, 0.0f, 0.0f},

            {0.0f, 0.0f, 0.0f},
            {0.0f, 1.0f, 0.0f},

            {0.0f, 0.0f, 0.0f},
            {0.0f, 0.0f, 1.0f},
        };
        tColor = {
            {1.0f, 0.0f, 0.0f, 1.0},
            {0.0f, 1.0f, 0.0f, 1.0},
            {0.0f, 0.0f, 1.0f, 1.0},
        };
        _axisVbo.Create(GL_STATIC_DRAW);
    }

    {
        auto &tPosition	= _quadVbo.getPosition();
        auto &tTex	= _quadVbo.getTexcoord();
        tPosition =
        {
            {-1.0f, -1.0f, 0.0f},
            { 1.0f, -1.0f, 0.0f},
            { 1.0f,  1.0f, 0.0f},
            {-1.0f,  1.0f, 0.0f},
        };
        tTex =
        {
            {0.0f, 0.0f},
            {1.0f, 0.0f},
            {1.0f, 1.0f},
            {0.0f, 1.0f},
        };
        _quadVbo.Create(GL_DYNAMIC_DRAW);
    }
}

std::vector<std::string> Util::Split(const std::string &str, char delim)
{
    std::vector<std::string> ret;
    std::istringstream stream(str);
    std::string s;
    while (getline(stream, s, delim))
        ret.push_back(s);
    return ret;
}

std::string Util::Basename(const std::string& path)
{
    std::size_t pos = 0;
    if ((pos = path.rfind('\\')) != std::string::npos)
        return path.substr(0, pos + 1);
    if ((pos = path.rfind('/')) != std::string::npos)
        return path.substr(0, pos + 1);
    return "";
}

std::string Util::Filename(const std::string& path)
{
    std::size_t pos = 0;
    if ((pos = path.rfind('\\')) != std::string::npos)
        return path.substr(pos + 1);
    if ((pos = path.rfind('/')) != std::string::npos)
        return path.substr(pos + 1);
    return path;
}

int Util::StrNICmp(const char *s1, const char *s2, std::size_t n)
{
    for (std::size_t i = 0; i < n; ++i)
    {
        auto c1 = std::tolower(s1[i]);
        auto c2 = std::tolower(s2[i]);
        if (c1 < c2)
            return -1;
        if (c1 > c2)
            return 1;
        if (c1 == 0)
            break;
    }
    return 0;
}

bool Util::IsFilename(const std::string& path)
{
    std::size_t pos = 0;
    if ((pos = path.find('\\')) != std::string::npos)
        return false;
    if ((pos = path.find('/')) != std::string::npos)
        return false;
    return true;
}

int Util::Round(double x)
{
    return (int)(x + 0.5);
}

int Util::NextPowerOfTwo(int x)
{
    double logbase2 = log(x) / log(2);
    return round(pow(2, ceil(logbase2)));
}

std::string Util::CurrentDateTime()
{
    char buf[80] = {0,};

    time_t now = time(0);
    struct tm tstruct;
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d-%m %X", &tstruct);

    return buf;
}

std::string Util::MaxStr(const std::string& str, std::size_t maxSize)
{
    if (str.length() > maxSize)
        return str.substr(str.length() - maxSize);
    return str;
}

int Util::ReadAllFile(const std::string &fileName, std::string &buffer)
{
    SDL_RWops *io = SDL_RWFromFile(fileName.c_str(), "rb");
    if (!io)
    {
        LOG_WARN("Could not open file %s. Error: %s", fileName.c_str(), SDL_GetError());
        return -1;
    }

    char buf[256] = {0,};
    size_t nRead = 0;
    while ((nRead = io->read(io, buf, 1, sizeof(buf))) > 0)
    {
        buffer.append(buf, nRead);
    }
    io->close(io);
    return 0;
}


void Util::DrawAxes(Shader &shader)
{
    shader.SetTexActive(false);
    _axisVbo.Enable(shader);
    GL_CHECK(glDrawArrays(GL_LINES, 0, 6));
    _axisVbo.Disable(shader);
}

void Util::DrawQuadAtScreen(Shader &shader, const vec3 &coords)
{
    // TODO: Optimize this by creating different vbos for different sizes
    bool changed = false;
    auto &tPosition	= _quadVbo.getPosition();
    for (auto &pos : tPosition)
    {
        int sign = 0;

        sign = pos.x < 0 ? -1 : 1;
        if (pos.x != coords.x * sign)
        {
            pos.x = coords.x * sign;
            changed = true;
        }

        sign = pos.y < 0 ? -1 : 1;
        if (pos.y != coords.y * sign)
        {
            pos.y = coords.y * sign;
            changed = true;
        }
    }
    _quadVbo.Enable(shader);
    if (changed)
        _quadVbo.BufferPosition();
    GL_CHECK(glDrawArrays(GL_TRIANGLE_FAN, 0, 4));
    _quadVbo.Disable(shader);
}

SDL_Surface *Util::ConvertSDLSurface(SDL_Surface *surface)
{
    if (!surface)
        return nullptr;
    SDL_PixelFormat RGBAFormat;
    RGBAFormat.palette = 0;
    RGBAFormat.BitsPerPixel = 32; RGBAFormat.BytesPerPixel = 4;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    RGBAFormat.Rmask = 0xFF000000; RGBAFormat.Rshift =  0; RGBAFormat.Rloss = 0;
    RGBAFormat.Gmask = 0x00FF0000; RGBAFormat.Gshift =  8; RGBAFormat.Gloss = 0;
    RGBAFormat.Bmask = 0x0000FF00; RGBAFormat.Bshift = 16; RGBAFormat.Bloss = 0;
    RGBAFormat.Amask = 0x000000FF; RGBAFormat.Ashift = 24; RGBAFormat.Aloss = 0;
#else
    RGBAFormat.Rmask = 0x000000FF; RGBAFormat.Rshift = 24; RGBAFormat.Rloss = 0;
    RGBAFormat.Gmask = 0x0000FF00; RGBAFormat.Gshift = 16; RGBAFormat.Gloss = 0;
    RGBAFormat.Bmask = 0x00FF0000; RGBAFormat.Bshift =  8; RGBAFormat.Bloss = 0;
    RGBAFormat.Amask = 0xFF000000; RGBAFormat.Ashift =  0; RGBAFormat.Aloss = 0;
#endif
    SDL_LockSurface(surface);
    SDL_Surface *conv = nullptr;
    if ((conv = SDL_ConvertSurface(surface, &RGBAFormat, SDL_SWSURFACE)) == nullptr)
    {
        LOG_WARN("WARN: Could not convert SDL surface. Error: %s" , SDL_GetError());
        SDL_FreeSurface(surface);
        return nullptr;
    }
    SDL_UnlockSurface(surface);
    SDL_FreeSurface(surface);

    return conv;
}

void Util::Clear()
{
    _axisVbo.Clear();
    _quadVbo.Clear();
}

void Util::SleepMS(int ms)
{
    if (ms > 10000 || ms < 0)
    {
        LOG_WARN("Sleeping for %d ms!", ms);
        assert(ms > 0);
    }
#ifndef ANDROID
    Sleep(ms);
#else
    usleep(ms * 1000);
#endif
}
