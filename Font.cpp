#include "Util.h"
#include "Font.h"
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <assert.h>
#include <stdio.h>

#include "ResourceManager.h"
#include "Effects.h"

bool Font::Load(const std::string& name, std::size_t size)
{
    SDL_RWops *io = SDL_RWFromFile(name.c_str(), "rb");
    if (!io)
    {
        LOG_WARN("Could not open font file %s using SDL_RWFromFile", name.c_str());
        return false;
    }
    if (!(_font = TTF_OpenFontRW(io, 1, size)))
    {
        printf("Error loading font %s: %s\n", name.c_str(), TTF_GetError());
        return false;
    }
    return true;
}

Font::~Font()
{
    if (_font)
    {
        TTF_CloseFont(_font);
        _font = nullptr;
    }
}

void Font::Draw(Shader &shader, const FontString &text)
{
    assert(_font);

    if (text.Get().empty())
        return;

    //GL_CHECK(glDisable(GL_TEXTURE_2D));
    //GL_CHECK(glDisable(GL_DEPTH_TEST));
    GL_CHECK(glEnable(GL_BLEND));
    GL_CHECK(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

    text.GetTexture().Bind(0);
    shader.SetTexActive(true);
    Util::get().DrawQuadAtScreen(shader, vec3(text.GetTexture().W()/2, text.GetTexture().H()/2, 0));
    text.GetTexture().Unbind(0);

    GL_CHECK(glDisable(GL_BLEND));
}

bool FontString::Set(Font &font, const std::string &text, const SDL_Color &color)
{
    if (_text != text)
    {
        if (!font.GetFont())
        {
            assert(font.GetFont());
            return false;
        }

        SDL_Surface *surface = TTF_RenderText_Blended(font.GetFont(), text.c_str(), color);
        surface = Util::ConvertSDLSurface(surface);
        if (!surface)
        {
            LOG_WARN("WARN: Could not create SDL font surface for text '%s'. Error: %s" , text.c_str(), TTF_GetError());
            return false;
        }
        if (!_tex.Load((GLubyte *)surface->pixels, surface->w, surface->h, surface->format->BytesPerPixel))
        {
            LOG_WARN("Cannot create font Texture2D for text %s", text.c_str());
            SDL_FreeSurface(surface);
            return false;
        }
        SDL_FreeSurface(surface);
        _text = text;
    }
    return true;
}

FontString::~FontString()
{
}
