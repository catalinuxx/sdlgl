#include "FileIO.h"

bool Writer::Create(const std::string &filename)
{
    if (!(_file = SDL_RWFromFile(filename.c_str(), "wb")))
        return false;
    return true;
}

int Writer::Write(const char *buff, int len)
{
    assert(_file);
    return _file->write(_file, buff, 1, len);
}

void Writer::Close()
{
    if (_file)
    {
        _file->close(_file);
        _file = nullptr;
    }
}

bool Reader::Open(const std::string &filename)
{
    if (!(_file = SDL_RWFromFile(filename.c_str(), "rb")))
        return false;
    return true;
}
int Reader::Read(char *buff, int len)
{
    assert(_file);
    return _file->read(_file, buff, 1, len);
}

void Reader::Rewind()
{
    assert(_file);
    _file->seek(_file, 0, RW_SEEK_SET);
}

void Reader::Close()
{
    if (_file)
    {
        _file->close(_file);
        _file = nullptr;
    }
}
