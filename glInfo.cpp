#include "Util.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <cstring>
#include <assert.h>
#include "glInfo.h"
using namespace std;

bool glInfo::Init()
{
    _init = true;
#ifndef HAVE_GLES
    glewInit();
#endif
    return ExtractInfo();
}


///////////////////////////////////////////////////////////////////////////////
// extract openGL info
// This function must be called after GL rendering context opened.
///////////////////////////////////////////////////////////////////////////////
bool glInfo::ExtractInfo()
{
    char* str = 0;
    char* tok = 0;

    // get vendor string
    str = (char*)glGetString(GL_VENDOR);
    if (str) this->vendor = str;                 // check NULL return value
    else return false;

    // get renderer string
    str = (char*)glGetString(GL_RENDERER);
    if (str) this->renderer = str;               // check NULL return value
    else return false;

    // get version string
    str = (char*)glGetString(GL_VERSION);
    if (str) this->version = str;                // check NULL return value
    else return false;

    // get all extensions as a string
    str = (char*)glGetString(GL_EXTENSIONS);

    // split extensions
    if (str)
    {
        tok = strtok((char*)str, " ");
        while (tok)
        {
            this->extensions.push_back(tok);    // put a extension into struct
            tok = strtok(0, " ");               // next token
        }
    }
    else
    {
        return false;
    }

    // sort extension by alphabetical order
    std::sort(this->extensions.begin(), this->extensions.end());

    // get number of color bits
    glGetIntegerv(GL_RED_BITS, &this->redBits);
    glGetIntegerv(GL_GREEN_BITS, &this->greenBits);
    glGetIntegerv(GL_BLUE_BITS, &this->blueBits);
    glGetIntegerv(GL_ALPHA_BITS, &this->alphaBits);

    // get depth bits
    glGetIntegerv(GL_DEPTH_BITS, &this->depthBits);

    // get stecil bits
    glGetIntegerv(GL_STENCIL_BITS, &this->stencilBits);

#ifndef HAVE_GLES
    // get max number of lights allowed
    glGetIntegerv(GL_MAX_LIGHTS, &this->maxLights);
#else
    this->maxLights = 0;
#endif

    // get max texture resolution
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &this->maxTextureSize);

#ifndef HAVE_GLES
    // get max number of clipping planes
    glGetIntegerv(GL_MAX_CLIP_PLANES, &this->maxClipPlanes);
#else
    this->maxClipPlanes = 0;
#endif

#ifndef HAVE_GLES
    // get max modelview and projection matrix stacks
    glGetIntegerv(GL_MAX_MODELVIEW_STACK_DEPTH, &this->maxModelViewStacks);
#else
    this->maxModelViewStacks = 0;
#endif
#ifndef HAVE_GLES
    glGetIntegerv(GL_MAX_PROJECTION_STACK_DEPTH, &this->maxProjectionStacks);
#else
    this->maxProjectionStacks = 0;
#endif
#ifndef HAVE_GLES
    glGetIntegerv(GL_MAX_ATTRIB_STACK_DEPTH, &this->maxAttribStacks);
#else
    this->maxAttribStacks = 0;
#endif

#ifndef HAVE_GLES
    // get max texture stacks
    GL_CHECK(glGetIntegerv(GL_MAX_TEXTURE_STACK_DEPTH, &this->maxTextureStacks));
#else
    this->maxTextureStacks = 0;
#endif // HAVE_GLES

    return true;
}



///////////////////////////////////////////////////////////////////////////////
// check if the video card support a certain extension
///////////////////////////////////////////////////////////////////////////////
bool glInfo::isExtensionSupported(const std::string& ext)
{
    assert(_init);
    for (std::vector<string>::const_iterator it = extensions.begin(); it != extensions.end(); it++)
    {
        if (*it == ext)
            return true;
    }
    LOG_WARN("WARNING : Extension \"%s\" is not supported", ext.c_str());
    return false;
}



///////////////////////////////////////////////////////////////////////////////
// print OpenGL info to screen and save to a file
///////////////////////////////////////////////////////////////////////////////
void glInfo::PrintInfo()
{
    stringstream ss;

    ss << endl; // blank line
    ss << "OpenGL Driver Info" << endl;
    ss << "==================" << endl;
    ss << "Vendor: " << this->vendor << endl;
    ss << "Version: " << this->version << endl;
    ss << "Renderer: " << this->renderer << endl;

    ss << endl;
    ss << "Color Bits(R,G,B,A): (" << this->redBits << ", " << this->greenBits
       << ", " << this->blueBits << ", " << this->alphaBits << ")\n";
    ss << "Depth Bits: " << this->depthBits << endl;
    ss << "Stencil Bits: " << this->stencilBits << endl;

    ss << endl;
    ss << "Max Texture Size: " << this->maxTextureSize << "x" << this->maxTextureSize << endl;
    ss << "Max Lights: " << this->maxLights << endl;
    ss << "Max Clip Planes: " << this->maxClipPlanes << endl;
    ss << "Max Modelview Matrix Stacks: " << this->maxModelViewStacks << endl;
    ss << "Max Projection Matrix Stacks: " << this->maxProjectionStacks << endl;
    ss << "Max Attribute Stacks: " << this->maxAttribStacks << endl;
    ss << "Max Texture Stacks: " << this->maxTextureStacks << endl;

    ss << endl;
    ss << "Total Number of Extensions: " << this->extensions.size() << endl;
    ss << "==============================" << endl;

#ifdef HAVE_GLES
    for (unsigned int i = 0; i < this->extensions.size(); ++i)
        ss << this->extensions.at(i) << endl;
    ss << "======================================================================" << endl;
#endif

    LOG_INFO("%s", ss.str().c_str());;
}
