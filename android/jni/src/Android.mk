LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := SDLGL

TOP := $(LOCAL_PATH)/../../..

SDL_PATH := $(TOP)/3rdparty/SDL/SDL2
SDL_IMAGE_PATH := $(TOP)/3rdparty/SDL/SDL2_image
SDL_TTF_PATH := $(TOP)/3rdparty/SDL/SDL2_ttf

LOCAL_CFLAGS := -DANDROID -DHAVE_GLES 
LOCAL_CPPFLAGS := $(LOCAL_CFLAGS) -std=c++11 -frtti -fexceptions 
LOCAL_LDLIBS := -landroid -llog -lEGL -lGLESv2

LOCAL_C_INCLUDES := \
	$(TOP)/MD5 \
	$(TOP)/Kinect \
	$(TOP)/Skeleton \
	$(TOP)/Math \
	$(SDL_PATH)/include $(SDL_IMAGE_PATH) $(SDL_TTF_PATH) $(TOP)/3rdparty/glm $(TOP)

FILE_LIST := \
	$(SDL_PATH)/src/main/android/SDL_android_main.c \
	$(wildcard $(TOP)/*.cpp) \
	$(wildcard $(TOP)/Scenes/*.cpp) \
	$(wildcard $(TOP)/Spline3D/*.cpp) \
	$(wildcard $(TOP)/Terrain/*.cpp) \
	$(wildcard $(TOP)/TinyObj/*.cpp) \
	$(wildcard $(TOP)/MD5/*.cpp) \
	$(wildcard $(TOP)/Skeleton/*.cpp) \
	$(TOP)/Kinect/Kinect.cpp \
	$(TOP)/Kinect/KinectClient.cpp
	
LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)

LOCAL_SHARED_LIBRARIES := SDL2
LOCAL_SHARED_LIBRARIES += SDL2_image
LOCAL_SHARED_LIBRARIES += SDL2_ttf
LOCAL_SHARED_LIBRARIES += SDL2_mixer

include $(BUILD_SHARED_LIBRARY)
$(call import-module,SDL2)LOCAL_PATH := $(call my-dir)
$(call import-module,SDL2_image)LOCAL_PATH := $(call my-dir)
$(call import-module,SDL2_ttf)LOCAL_PATH := $(call my-dir)
#$(call import-module,SDL2_mixer)LOCAL_PATH := $(call my-dir)
