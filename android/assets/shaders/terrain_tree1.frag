#version 100

precision lowp float;

uniform sampler2D Tex;

uniform vec4 LightAmbient;
uniform vec4 LightDiffuse;
uniform vec4 MatAmbient;
uniform vec4 MatDiffuse;
uniform vec4 MatSpecular;
uniform float MatShininess;

varying mat4 FragModelView;
varying mat4 FragTextureMatrix;
varying vec4 FragDestinationColor;
varying vec2 FragTexCoordOut;
varying vec2 FragTexCoordOut1;

void main (void)
{
	vec4 cBase = texture2D(Tex, FragTexCoordOut);
	if(cBase.a < 0.4) discard;
	
	vec4 cAmbient = LightAmbient * MatAmbient;
	vec4 cDiffuse = LightDiffuse * MatDiffuse * FragDestinationColor;
	
	gl_FragColor = cAmbient * cBase + cDiffuse * cBase;
	gl_FragColor.a = FragDestinationColor.a;
	
}














