#version 100

precision lowp float;

attribute vec3 Position;
attribute vec2 TexCoord;
attribute vec3 Tangent;
attribute vec3 Normal;

uniform mat4 ModelView;
uniform mat4 Projection;
uniform mat3 NormalMatrix;
uniform mat4 TextureMatrix;
uniform mat4 TextureMatrix1;

uniform vec3 LightPosition;
uniform bool TexInv;
uniform vec3 LightSpotDirection;

varying vec2 FragTexCoordOut;
varying vec4 FragTexCoordOut1;

varying vec4 vPixToLightTBN[1];	// Vecteur du pixel courant � la lumi�re
varying vec3 vPixToEyeTBN;					// Vecteur du pixel courant � l'oeil
varying vec3 vVertexMV;
varying vec3 vNormalMV;
varying vec3 vPixToLightMV;
varying vec3 vLightDirMV;

// SHADOW MAPPING //
uniform int enable_shadow_mapping;
////////////////////


#define MODE_PHONG		0
#define MODE_BUMP		1
#define MODE_PARALLAX	2
#define MODE_RELIEF		3
uniform int mode;

#define LIGHT_DIRECTIONAL		0.0
#define LIGHT_OMNIDIRECTIONAL	1.0
#define LIGHT_SPOT				2.0
				
void main(void)
{
	gl_Position = Projection * ModelView * vec4(Position, 1.0);
	FragTexCoordOut = TexCoord;
	if (TexInv)
        FragTexCoordOut.y = -FragTexCoordOut.y;
	
	vec3 n = normalize(NormalMatrix * Normal);
	vec3 t = normalize(NormalMatrix * Tangent);
	vec3 b = cross(n, t);
	
	vNormalMV = n;
	
	vec4 vLightPosMV = vec4(LightPosition, 1.0);		// Position (ou direction) de la lumi�re dans la MV
	vVertexMV = vec3(ModelView * vec4(Position,1.0));	// Position du vertex dans la MV
	
	vec3 tmpVec;



	if(vLightPosMV.w == LIGHT_DIRECTIONAL)
		tmpVec = -vLightPosMV.xyz;					// Lumi�re directionelle
	else
		tmpVec = vLightPosMV.xyz - vVertexMV.xyz;	// Lumi�re ponctuelle

	vPixToLightMV = tmpVec;
/*
	if(mode == MODE_PHONG)
	{
		vPixToLightTBN[0].xyz = tmpVec.xyz;
		vPixToLightTBN[0].w = vLightPosMV.w;	// ponctuelle ou directionnelle
		
		vPixToEyeTBN = -vVertexMV;
	}
	else*/
	{
		// Position ou direction de la lumi�re
		vPixToLightTBN[0].x = dot(tmpVec, t);
		vPixToLightTBN[0].y = dot(tmpVec, b);
		vPixToLightTBN[0].z = dot(tmpVec, n);
		vPixToLightTBN[0].w = vLightPosMV.w;	// ponctuelle ou directionnelle
			
		// Vecteur vue
		tmpVec = -vVertexMV;
		vPixToEyeTBN.x = dot(tmpVec, t);
		vPixToEyeTBN.y = dot(tmpVec, b);
		vPixToEyeTBN.z = dot(tmpVec, n);
	}
	
		
	if(length(LightSpotDirection) > 0.001)
	{
		// Lumi�re spot
		vLightDirMV = normalize(LightSpotDirection);
		vPixToLightTBN[0].w = LIGHT_SPOT;
	}
	else
	{
		// Lumi�re non spot
		vLightDirMV = LightSpotDirection;
	}
	
	if(enable_shadow_mapping != 0)
	{
		// pos a subit les transformations + la cam�ra
		vec4 pos = ModelView * vec4(Position, 1.0);
		// on multiplie par la matrice inverse de la cam�ra : pos a seulement subit les transformations
		pos = TextureMatrix * pos;
		// on multiplie par la matrice de la lumi�re : position du Vertex dans le rep�re de la lumi�re
		FragTexCoordOut1 = TextureMatrix1 * pos;
	}
}
