#version 100

precision lowp float;

uniform sampler2D Tex;

varying vec2 FragTexCoordOut;

uniform sampler2D texLeftEye;
uniform sampler2D texRightEye;

void main(void)
{
	vec4 colorLeftEye = texture2D(texLeftEye, FragTexCoordOut);
	vec4 colorRightEye = texture2D(texRightEye, FragTexCoordOut);
	gl_FragColor = vec4(colorLeftEye.r, colorRightEye.g, colorRightEye.b, 1.0);
}
