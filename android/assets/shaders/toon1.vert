#version 100

precision lowp float;

attribute vec3 Position;
attribute vec4 Color;
attribute vec3 Normal;
attribute vec2 TexCoord;

uniform mat4 ModelView;
uniform mat4 Projection;
uniform mat3 NormalMatrix;

uniform vec3 LightPosition;
uniform bool TexInv;

varying vec3 FragNormal, FragViewVec, FragLightVec;
varying vec4 FragDestinationColor;
varying vec2 FragTexCoordOut;

void main(void)
{
    vec4 pos = vec4(Position, 1.0);
	vec3 vertex = (ModelView * pos).xyz;
	FragTexCoordOut = TexCoord;
	if (TexInv)
        FragTexCoordOut.y = -FragTexCoordOut.y;

    FragViewVec = -vertex;
	FragLightVec = LightPosition - vertex;

    FragDestinationColor = Color;
	FragNormal = NormalMatrix * Normal;
	gl_Position = Projection * ModelView * pos;
}
