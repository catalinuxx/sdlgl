#version 100

precision lowp float;

attribute vec3 Position;
attribute vec3 Normal;
attribute vec3 Tangent;
attribute vec2 TexCoord;

uniform mat4 ModelView;
uniform mat4 Projection;
uniform mat3 NormalMatrix;
uniform vec3 LightPosition;
uniform mat4 TextureMatrix;

varying vec4 FragDestinationColor;
varying vec2 FragTexCoordOut;
varying vec2 FragTexCoordOut1;
varying mat4 FragModelView;
varying mat4 FragTextureMatrix;

uniform float time;

				
void main(void)
{
	
	FragTexCoordOut = TexCoord;
	
	vec4 vertex = vec4(Position, 1.0);
	vec4 vertexMV = ModelView * vertex;
	vec3 normalMV = NormalMatrix * Normal;
	
	vec4 vertexM = TextureMatrix * ModelView * vec4(0.0, 0.0, 0.0, 1.0);
	
	// Animation des arbres
	float move_speed = (mod(vertexM.y * vertexM.z, 50.0)/50.0 + 0.5);
	float move_offset = vertexM.x;
	vertex.x += 0.01 * pow(vertex.y, 2.0) * cos(time * move_speed + move_offset);
	
	
	vec4 vLightPosMV = vec4(-LightPosition, 1.0);		// Lumi�re directionelle
	float intensity = dot(vLightPosMV.xyz, normalMV);
	FragDestinationColor = vec4(intensity, intensity, intensity, 1.0);
	FragDestinationColor.a = 1.0 - clamp(length(vertexMV)/120.0, 0.0, 1.0);
		
	gl_Position = Projection * ModelView * vertex;
}





