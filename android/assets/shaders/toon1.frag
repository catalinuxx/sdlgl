#version 100

precision lowp float;

uniform vec4 MatAmbient;     // Acm
uniform vec4 MatDiffuse;     // Dcm
uniform vec4 MatSpecular;    // Scm

uniform sampler2D Tex;
uniform bool TexActive;

varying vec4 FragDestinationColor;
varying vec2 FragTexCoordOut;

varying vec3 FragNormal, FragViewVec, FragLightVec;

void main()
{
	vec4 colorTex = vec4(1.0, 1.0, 1.0, 1.0);

	if(TexActive)
		colorTex = texture2D(Tex, FragTexCoordOut);

	vec4 color;
	float facteur = 1.0;

	vec3 N = normalize(FragNormal);
	vec3 L = normalize(FragLightVec);
	vec3 V = normalize(FragViewVec);
	vec3 R = normalize(2.0*dot(N,V)*N-V);
	// Intense diffuse
	float LdotN = dot(L,N);
	// Intense spec
	float LdotR = dot(L,R);

	// 3 level diffuse
	if(LdotN<0.2)
    {
		facteur=0.5;
    }
	else
    {
        if(LdotN<0.5)
            facteur=0.75;
        else
            facteur=0.9;
    }

	color = MatAmbient + (MatDiffuse * facteur);

	// Specular
	if(LdotR > 0.95)
		color += MatSpecular * pow(clamp(LdotR, 0.0, 1.0), 20.0);

	if(color.r>1.0) color.r = 1.0;
	if(color.g>1.0) color.g = 1.0;
	if(color.b>1.0) color.b = 1.0;

	gl_FragColor = color * colorTex;
	gl_FragColor.a = 1.0;
}

