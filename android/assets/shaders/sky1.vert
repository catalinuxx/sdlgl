#version 100

precision lowp float;

attribute vec3 Position;

uniform mat4 ModelView;
uniform mat4 Projection;

varying vec3 vertex;

void main(void)
{
	vertex = normalize(Position);	
	gl_Position = Projection * ModelView * vec4(Position, 1.0);
}
