#version 100

precision lowp float;

uniform sampler2D Tex;

varying vec2 FragTexCoordOut;

// Texture size
uniform vec2 size;

// blur horizontal ou vertical
uniform bool horizontal;

// Taille du blurage
uniform int kernel_size;

void main()
{
	vec2 pas = 1.0/size;
	vec2 uv = FragTexCoordOut;
	vec4 color = vec4(0.0, 0.0, 0.0, 1.0);


	// HORIZONTAL BLUR
	if(horizontal)
    {
		int j = 0;
		int sum = 0;
		for(int i = -kernel_size; i <= kernel_size; i++)
		{
			vec4 value = texture2D(Tex, uv + vec2(pas.x * float(i), 0.0));
            int iAbs = i > 0 ? i : -1;
			int factor = kernel_size + 1 - iAbs;
			color += value * float(factor);
			sum += factor;
		}
		color /= float(sum);
	}

	// VERTICAL BLUR
	else {
		int j = 0;
		int sum = 0;
		for(int i=-kernel_size; i<=kernel_size; i++)
        {
			vec4 value = texture2D(Tex, uv + vec2(0.0, pas.y * float(i)));
            int iAbs = i > 0 ? i : -1;
			int factor = kernel_size + 1 - iAbs;
			color += value * float(factor);
			sum += factor;
		}
		color /= float(sum);
	}

	gl_FragColor = color;
	gl_FragColor.a = 1.0;
}
