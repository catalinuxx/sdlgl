#version 100

precision lowp float;

attribute vec3 Position;
attribute vec3 Normal;
attribute vec3 Tangent;

uniform mat4 ModelView;
uniform mat4 Projection;
uniform vec3 LightPosition;
uniform mat4 TextureMatrix;

varying vec2 FragTexCoordOut;
varying vec4 FragTexCoordOut1;
varying mat4 FragModelView;
varying mat4 FragTextureMatrix;

// Bounding Box du terrain
uniform vec3 bbox_min;
uniform vec3 bbox_max;

varying vec3 vPixToLight;		// Vecteur du pixel courant � la lumi�re
varying vec3 vPixToEye;			// Vecteur du pixel courant � l'oeil
varying vec4 vPosition;
		
void main(void)
{
	FragModelView = ModelView;
	vec4 pos = vec4(Position, 1.0);
	gl_Position = Projection * ModelView * pos;
	
	vPosition = vec4(Position, 1.0);
	vec3 vPositionNormalized = (Position - bbox_min.xyz) / (bbox_max.xyz - bbox_min.xyz);
	FragTexCoordOut = vPositionNormalized.xz;
	
	vPixToLight = -LightPosition;		// Position (ou direction) de la lumi�re dans la MV
	vPixToEye = -vec3(ModelView * pos);		// Position du vertex dans la MV
	
	// on multiplie par la matrice de la lumi�re : position du Vertex dans le rep�re de la lumi�re
	FragTexCoordOut1 = TextureMatrix * pos;		
}
