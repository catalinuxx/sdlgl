#pragma once

#include <iostream>
#include "Util.h"
#include "Mathlib.h"
#include <vector>
#include "BoundingBox.h"
#include "IResource.h"

#include <memory>

#include "TinyObj/TinyObj.h"

class VertexBufferObject;

class Mesh : public IResource
{
public:
    bool Load(const std::string& name, GLenum usage = GL_STATIC_DRAW);

    void Draw(Shader &shader, int group = -1, bool forceDraw = false, bool ignoreMat = false);
    int GetGroupByName(const std::string& name);

    inline GLuint GetGroupCount() const
    {
        return (GLuint)_shapes.size();
    }
    inline BoundingBox&	getBoundingBox()
    {
        return m_BBox;
    }

    static void EnableComputeNormals(bool b)
    {
        s_bComputeNormals = b;
    }

    int GetNumberOfNonVisibleGroups()
    {
        return _numberOfNonVisibleGroups;
    }

    void Move(const vec3 &translate)
    {
        _translation = translate;
    }
    const vec3 &GetTranslation() { return _translation; }

    void EnableBBoxDraw();

    const std::vector<tinyobj::shape_t> &GetShapes() { return _shapes; }

    Mesh();
    virtual ~Mesh();

private:
    bool LoadOBJ(const std::string& filename);
    void Clear();
    void DrawGroup(Shader &shader, GLuint group, bool forceDraw, bool ignoreMat);

    void ComputeNormals();
    void ComputeTangents();
    void ComputeBoundingBox();

private:
    std::vector<tinyobj::shape_t> _shapes;
    std::vector<tinyobj::material_t> _materials;
    BoundingBox m_BBox;
    static bool s_bComputeNormals;	// Etat pour le chargement : g�n�ration ou non des normales � la main
    std::string _name;
    int _numberOfNonVisibleGroups = 0;
    bool _drawBBox = false;
    GLenum _usage = GL_STATIC_DRAW;
    vec3 _translation;
};
