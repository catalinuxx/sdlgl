#include "Util.h"
#include "Camera.h"
#include "Frustum.h"
#include <assert.h>

void Camera::SaveCamera()
{
    tSaveVectors[0] = vEye;
    tSaveVectors[1] = vCenter;
    tSaveVectors[2] = vViewDir;
    tSaveVectors[3] = vLeftDir;
    tSaveVectors[4] = vUp;

    tSaveFloats[0] = fAngleX;
    tSaveFloats[1] = fAngleY;

    bSaved = true;
}

void Camera::RestoreCamera()
{
    if (bSaved)
    {
        vEye = tSaveVectors[0];
        vCenter = tSaveVectors[1];
        vViewDir = tSaveVectors[2];
        vLeftDir = tSaveVectors[3];
        vUp = tSaveVectors[4];

        fAngleX = tSaveFloats[0];
        fAngleY = tSaveFloats[1];
    }

    bSaved = false;
}


Camera::Camera()
{
    // Param�tres de la cam�ra par d�fault :
    fAngleX	=	3.0f;
    fAngleY	=	M_PI / 2;

    vUp			= vec3(0.0f, 1.0f, 0.0f);
    vEye		= vec3(0.0f, 0.0f, 0.0f);

    eType = FREE;
    bSaved = false;

    Refresh();
}


void Camera::Refresh()
{
    switch (eType)
    {
    case FREE:
        vViewDir.x = cosf(fAngleX + fJoyAngleX) * sinf(fAngleY);
        vViewDir.y = cosf(fAngleY + fJoyAngleY);
        vViewDir.z = sinf(fAngleX + fJoyAngleX) * sinf(fAngleY + fJoyAngleY);
        vCenter = vEye + vViewDir;
        vLeftDir.cross(vUp, vViewDir);
        vLeftDir.normalize();
        break;

    case DRIVEN:
        vViewDir = vCenter - vEye;
        vViewDir.normalize();
        vLeftDir.cross(vUp, vViewDir);
        vLeftDir.normalize();
        break;
    }

    _frustum.Extract(modelView, projection, vEye);
//	vLeftDir.cross(vUp, vViewDir);
}

void Camera::SelfRenderLookAt(bool inverty, float planey)
{
    if (inverty)
        modelView.look_at(vec3(vEye.x,		2.0f * planey - vEye.y,		vEye.z),
                     vec3(vCenter.x,	2.0f * planey - vCenter.y,	vCenter.z),
                     vec3(-vUp.x,		-vUp.y,					-vUp.z));
    else
        modelView.look_at(vEye, vCenter, vUp);
    _frustum.Extract(modelView, projection, vEye);
}

void Camera::RenderLookAt(mat4 &modelView, mat4 &projection, bool inverty, float planey)
{
    if (inverty)
        modelView.look_at(vec3(vEye.x,		2.0f * planey - vEye.y,		vEye.z),
                     vec3(vCenter.x,	2.0f * planey - vCenter.y,	vCenter.z),
                     vec3(-vUp.x,		-vUp.y,					-vUp.z));
    else
        modelView.look_at(vEye, vCenter, vUp);
    _frustum.Extract(modelView, projection, vEye);
}




// Autres Fonctions :
void Camera::PlayerMoveForward(float factor)
{
    // Avancer/Reculer
    //vec3 vPrevEye = vEye;	// on sauvegarde la position pr�c�dente

    // on fait varier la position de la cam�ra :
    vEye += vViewDir * factor;

    Refresh();
}

void Camera::TranslateForward(float factor)	 	// Avancer/Reculer
{
    // vec3 vPrevEye = vEye;	// on sauvegarde la position pr�c�dente

    // on fait varier la position de la cam�ra :
    vEye += vViewDir * factor;

    Refresh();
}

void Camera::PlayerMoveStrafe(float factor)	 	// Strafe Gauche/Droit
{
    // vec3 vPrevEye = vEye;	// on sauvegarde la position pr�c�dente

    // on fait varier la position de la cam�ra :
    vEye += vLeftDir * factor;

    Refresh();
}

void Camera::TranslateStrafe(float factor)	 	// Strafe Gauche/Droit
{
    // vec3 vPrevEye = vEye;	// on sauvegarde la position pr�c�dente

    // on fait varier la position de la cam�ra :
    vEye += vLeftDir * factor;

    Refresh();
}


void Camera::MoveAnaglyph(float factor)
{
    vEye += vLeftDir * factor;
    vCenter += vLeftDir * factor;
}

void Camera::RenderLookAtToCubeMap(mat4 &modelView, mat4 &projection, const vec3& eye, unsigned int nFace)
{
    assert(nFace < 6);

    // Tableau de vecteurs "Center" pour la cam�ra :
    vec3 TabCenter[6] = {	vec3(eye.x + 1.0f,	eye.y,		eye.z),
                            vec3(eye.x - 1.0f,	eye.y,		eye.z),

                            vec3(eye.x,			eye.y + 1.0f,	eye.z),
                            vec3(eye.x,			eye.y - 1.0f,	eye.z),

                            vec3(eye.x,			eye.y,		eye.z + 1.0f),
                            vec3(eye.x,			eye.y,		eye.z - 1.0f)
                        };


    // Tableau de vecteurs "Up" pour la cam�ra :
    static vec3 TabUp[6] = {	vec3(0.0f,	-1.0f,	0.0f),
                                vec3(0.0f,	-1.0f,	0.0f),

                                vec3(0.0f,	0.0f,	1.0f),
                                vec3(0.0f,	0.0f,	-1.0f),

                                vec3(0.0f,	-1.0f,	0.0f),
                                vec3(0.0f,	-1.0f,	0.0f)
                           };

    setEye(eye);

#ifndef HAVE_GLES
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
#endif // HAVE_GLES

    modelView.look_at(eye, TabCenter[nFace], TabUp[nFace]);

    _frustum.Extract(modelView, projection, eye);
}
