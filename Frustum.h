#pragma once

#include "Mathlib.h"
#include "Singleton.h"
#include "BoundingBox.h"

#define FRUSTUM_OUT			0
#define FRUSTUM_IN			1
#define FRUSTUM_INTERSECT	2

class Frustum
{
public:
    void Extract(const mat4 &modelView, const mat4 &projection, const vec3& eye);

    bool ContainsPoint(const vec3& point) const;
    int ContainsBoundingBox(const BoundingBox& bbox) const;
    int ContainsSphere(const vec3& center, float radius) const;

//	vec3 Intersect2D(int id1, int id2) const;

    vec3& getEyePos()
    {
        return m_EyePos;
    }
    mat4& getModelviewMatrix()
    {
        return m_mtxMV;
    }
    mat4& getModelviewInvMatrix()
    {
        return m_mtxMVinv;
    }
    mat4& getProjectionMatrix()
    {
        return m_mtxProj;
    }

    static Frustum &get()
    {
        static Frustum dummy;
        return dummy;
    }

private:
    vec3	m_EyePos;
    vec4	m_tFrustumPlanes[6];	// 6 plans
    mat4	m_mtxMV, m_mtxMVinv;	// Matrice de Modelview
    mat4	m_mtxProj; 				// Matrice de Projection
    mat4	m_mtxMVProj;			// Matrice Projection * Modelview

};
