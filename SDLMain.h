#pragma once

class SDLMain
{
public:
    //Starts up SDL, creates window, and initializes OpenGL
    SDLMain(int argc, char **argv);
    virtual ~SDLMain();

    // Main loop
    int run();
    // Frees media and shuts down SDL
    void close();

private:
    void idle();

    // Input handler
    int handleKey(unsigned char key);

    // Called on resize
    void reshapeGL(int newwidth, int newheight);

    void initGL();

    void displayGL(void);

private:
    // The window we'll be rendering to
    SDL_Window* _window = nullptr;

    // OpenGL context
    SDL_GLContext _glContext;

    // Font strings
    FontString _fpsFontString;
    FontString _speedFontString;
    FontString _cameraString;
    FontString _sceneString;

    // Accelerometer
    SDL_Joystick *_joystick = nullptr;

    ivec2 _ppos;
};
