#pragma once

#include "Util.h"
#include "Singleton.h"
#include "Mathlib.h"
#include <iostream>

class Shader;
class Mesh;

class Sky : public Singleton<Sky>
{
public:
    void Init();
    void DrawSky(const vec3& vEyePos, const GLuint& cubemap, bool invert = false) const;
    void DrawSkyAndSun(const vec3& vEyePos, const vec3& vSunVect, const GLuint& cubemap, bool invert) const;

private:
    Shader*	m_pShd = nullptr;
    Mesh *_sphere = nullptr;
};
