#include "Util.h"
#include "Sky.h"
#include "TextureCubemap.h"
#include "ResourceManager.h"
#include "VarManager.h"
#include "Camera.h"

void Sky::Init()
{
    if (!VarManager::get().getb("enable_sky"))
        return;
    ResourceManager& res = ResourceManager::get();
    m_pShd = (Shader*)res.LoadResource(ResourceManager::SHADER, "sky1");
    _sphere = (Mesh*)res.LoadResource(ResourceManager::MESH, "sphere.obj");
}

void Sky::DrawSkyAndSun(const vec3& vEyePos, const vec3& vSunVect, const GLuint& cubemap, bool invert) const
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap);

    assert(m_pShd || !"Please enable sky!");
    if (m_pShd && _sphere)
    {
        mat4 sca;
        sca.scale(1.0, 1.0, 1.0);
        if (invert)
            sca *= mat4(1.0, -1.0, 1.0);

        if (m_pShd->Activate(Camera::get().GetModelView() * mat4(Camera::get().getEye()) * sca, Camera::get().GetProjection()))
        {
            m_pShd->SetMatDiffuse(vec4(1,1,0,1));

            m_pShd->UniformTexture("texSky", 0);

            m_pShd->Uniform("enable_sun", true);
            m_pShd->Uniform("sun_vector", vSunVect);
    //		m_pShd->Uniform("view_vector", Camera::get().getViewDir());

            GL_CHECK(glDisable(GL_CULL_FACE));
    #ifndef HAVE_GLES
            GL_CHECK(glDisable(GL_LIGHTING));
    #endif // HAVE_GLES

            _sphere->Draw(*m_pShd, -1, true);

            m_pShd->Deactivate();
        }
    }

    GL_CHECK(glClear(GL_DEPTH_BUFFER_BIT));
}

void Sky::DrawSky(const vec3& vEyePos, const GLuint& cubemap, bool invert) const
{
    GL_CHECK(glActiveTexture(GL_TEXTURE0));
    GL_CHECK(glEnable(GL_TEXTURE_CUBE_MAP));
    GL_CHECK(glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap));
    if (m_pShd && _sphere)
    {
        mat4 sca;
        sca.scale(1.0, 1.0, 1.0);
        if (invert)
            sca *= mat4(1.0, -1.0, 1.0);
        m_pShd->Activate(Camera::get().GetModelView() * mat4(Camera::get().getEye()) * sca, Camera::get().GetProjection());
        m_pShd->UniformTexture("texSky", 0);

        m_pShd->Uniform("enable_sun", false);

        GL_CHECK(glDisable(GL_CULL_FACE));
#ifndef HAVE_GLES
        GL_CHECK(glDisable(GL_LIGHTING));
#endif // HAVE_GLES
        _sphere->Draw(*m_pShd, -1, true);

        m_pShd->Deactivate();
    }
    GL_CHECK(glDisable(GL_TEXTURE_CUBE_MAP));

    GL_CHECK(glClear(GL_DEPTH_BUFFER_BIT));

    /*
    	static GLfloat xPlane[] = { 1.0f, 0.0f, 0.0f, 0.0f };
    	static GLfloat yPlane[] = { 0.0f, 1.0f, 0.0f, 0.0f };
    	static GLfloat zPlane[] = { 0.0f, 0.0f, 1.0f, 0.0f };

    	glMatrixMode(GL_MODELVIEW);
    	glPushMatrix();
    	glTranslatef(vEyePos.x, vEyePos.y, vEyePos.z);
    	if(invert)
    		glScalef(1.0f, -1.0f, 1.0f);

    	glPushAttrib(GL_ENABLE_BIT);
    	glPushAttrib(GL_POLYGON_BIT);

    	glEnable (GL_TEXTURE_GEN_S);
    	glEnable (GL_TEXTURE_GEN_T);
    	glEnable (GL_TEXTURE_GEN_R);

    	glEnable (GL_TEXTURE_CUBE_MAP);

    	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap);

    	glTexGeni (GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    	glTexGeni (GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    	glTexGeni (GL_R, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);

    	glTexGenfv (GL_S, GL_OBJECT_PLANE, xPlane);
    	glTexGenfv (GL_T, GL_OBJECT_PLANE, yPlane);
    	glTexGenfv (GL_R, GL_OBJECT_PLANE, zPlane);

    	glDisable(GL_CULL_FACE);
    	glDisable(GL_LIGHTING);
    	glColor3f(1.0f, 1.0f, 1.0f);
    	Util::get().Sphere(1.0, 4, 4);

    	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    	glDisable (GL_TEXTURE_CUBE_MAP);

    	glDisable (GL_TEXTURE_GEN_S);
    	glDisable (GL_TEXTURE_GEN_T);
    	glDisable (GL_TEXTURE_GEN_R);

    	glPopAttrib();
    	glPopAttrib();

    	glClear(GL_DEPTH_BUFFER_BIT);
    	glPopMatrix();*/
}
