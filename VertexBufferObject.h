#pragma once

#include <iostream>
#include "Util.h"
#include "Mathlib.h"
#include "BoundingBox.h"
#include <vector>
#include <memory>

class VertexBufferObject
{
public:
    bool Create(GLenum usage);

    void Enable(Shader &shader);
    void Disable(Shader &shader);

    // Buffers de données
    inline std::vector<vec3>& getPosition()
    {
        return m_tDataPosition;
    }
    inline std::vector<vec3>& getNormal()
    {
        return m_tDataNormal;
    }
    inline std::vector<vec2>& getTexcoord()
    {
        return m_tDataTexcoord;
    }
    inline std::vector<vec3>& getTangent()
    {
        return m_tDataTangent;
    }
    inline std::vector<vec4>& getColor()
    {
        return m_tDataColor;
    }

    int &NumPoints() { return _numPoints; }

    void BufferPosition();
    void BufferTexCoord();
    void BufferColor();

    void ComputeBoundingBox();
    BoundingBox GetBoundingBox() {return _BBox; };
    void CreateBBoxVBOs(bool update = false);
    void DrawBBox(Shader &shader);

    VertexBufferObject();
    virtual ~VertexBufferObject();
    void Clear();

private:
    void Enable_VA();	// Activation en Vertex Array
    void Enable_VBO(Shader &shader);	// Activation en Vertex Buffer Object
    void Disable_VA();	// Désactivation en Vertex Array
    void Disable_VBO(Shader &shader);	// Désactivation en Vertex Buffer Object

private:
    // VBO identification
    GLuint		m_nVBOid;
    GLintptr	m_nVBO_OffsetPosition;
    GLintptr	m_nVBO_OffsetNormal;
    GLintptr	m_nVBO_OffsetTexcoord;
    GLintptr	m_nVBO_OffsetTangent;
    GLintptr	m_nVBO_OffsetColor;

    // Data :
    std::vector<vec3>	m_tDataPosition;
    std::vector<vec3>	m_tDataNormal;
    std::vector<vec2>	m_tDataTexcoord;
    std::vector<vec3>	m_tDataTangent;
    std::vector<vec4>	m_tDataColor;

    BoundingBox _BBox;

    bool _haveBBoxVbos = false;
    std::unique_ptr<VertexBufferObject> _bboxVbo1, _bboxVbo2, _bboxVbo3;

    int _numPoints = 0;
};
