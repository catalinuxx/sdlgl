#pragma once

#include "Singleton.h"
#include <iostream>
#include <map>
#include <string>
#include "Util.h"

#include <assert.h>

class VarManager : public Singleton<VarManager>
{
public:
    VarManager();

    inline void set(const std::string& name, float val)
    {
        m_VarDB[name].type = 'f';
        m_VarDB[name].fval = val;
    }
    inline void set(const std::string& name, int val)
    {
        m_VarDB[name].type = 'i';
        m_VarDB[name].ival = val;
    }
    inline void set(const std::string& name, bool val)
    {
        m_VarDB[name].type = 'b';
        m_VarDB[name].bval = val;
    }
    inline void set(const std::string& name, const char *val)
    {
        m_VarDB[name].type = 's';
        m_VarDB[name].sval = val;
    }
    inline void set(const std::string& name, const std::string val)
    {
        m_VarDB[name].type = 's';
        m_VarDB[name].sval = val;
    }

    inline float getf(const std::string& name, const float &def = 0.0f)
    {
        bool found = false;
        auto &item = getVar(name, found);
        if (!found) item.fval = def;
        return item.fval;
    }
    inline int   geti(const std::string& name, const int &def = 0)
    {
        bool found = false;
        auto &item = getVar(name, found);
        if (!found) item.ival = def;
        return item.ival;
    }
    inline bool  getb(const std::string& name, const float &def = false)
    {
        bool found = false;
        auto &item = getVar(name, found);
        if (!found) item.bval = def;
        return item.bval;
    }
    inline std::string gets(const std::string& name, const std::string& def = "")
    {
        bool found = false;
        auto &item = getVar(name, found);
        if (!found) item.sval = def;
        return item.sval;
    }

    void FromCfg(const std::string &cfgFile);
    void Print(const std::string &msg = "");

private:
    friend class Singleton<VarManager>;
    virtual ~VarManager();

    struct sVar
    {
        unsigned char type;
        std::string sval;
        union
        {
            float fval;
            int ival;
            bool bval;
        };
    };
    std::map<std::string, sVar>	m_VarDB;
    inline sVar &getVar(const std::string& name, bool &found)
    {
        if (m_VarDB.find(name) == m_VarDB.end())
        {
            LOG_WARN("WARN: Could not find variable %s\n", name.c_str());
            //assert(m_VarDB.find(name) != m_VarDB.end());
            found = false;
            return (m_VarDB[name] = sVar());
        }
        found = true;
        return m_VarDB[name];
    }
};

