#pragma once

#include "Util.h"
#include <iostream>
#include "Mathlib.h"

// -------------------------------
// Chargement d'images
// retourne les donn�es brutes
// -------------------------------

namespace ImageTools
{
class ImageData
{
public:
    GLubyte*	data;
    unsigned int w, h, d;

    ivec3	getColor(unsigned int x, unsigned int y) const;
    //	ivec3	getColor(float x, float y);

    ImageData()
    {
        w = h = d = 0;
        data = NULL;
    }
    virtual ~ImageData();
};

void     OpenImage(const std::string& filename, ImageData& img);
GLubyte* OpenImage(const std::string& filename, unsigned int& w, unsigned int& h, unsigned int& d);

GLubyte* OpenImagePPM(const std::string& filename, unsigned int& w, unsigned int& h, unsigned int& d);
GLubyte* OpenImageSDL(const std::string& filename, unsigned int& w, unsigned int& h, unsigned int& d);
}

