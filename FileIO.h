#pragma once

#include <memory>
#include <cstdio>

#include "KinectInterface.h"

class Writer
{
public:
    bool Create(const std::string &filename);
    int Write(const char *buff, int len);
    void Close();
    virtual ~Writer() { Close(); }
private:
    SDL_RWops *_file = nullptr;
};

class Reader
{
public:
    bool Open(const std::string &filename);
    int Read(char *buff, int len);
    void Rewind();
    void Close();
    virtual ~Reader() { Close(); }
private:
    SDL_RWops *_file = nullptr;
};
