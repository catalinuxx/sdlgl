#pragma once

#define VERTEX_ATTRIB_NAME "Position"
#define COLOR_ATTRIB_NAME "Color"
#define NORMAL_ATTRIB_NAME "Normal"
#define TANGENT_ATTRIB_NAME "Tangent"
#define TEXCOORD_ATTRIB_NAME "TexCoord"
#define TEXCOORD1_ATTRIB_NAME "TexCoord1"
#define TEX_UNIFORM "Tex"
#define TEXACTIVE_UNIFORM "TexActive"
#define TEXINV_UNIFORM "TexInv"
#define MODEL_VIEW_UNIFORM "ModelView"
#define PROJECTION_UNIFORM "Projection"
#define NORMAL_MATRIX_UNIFORM "NormalMatrix"
#define LIGHT_POSITION_UNIFORM "LightPosition"
#define LIGHT_SPOT_DIRECTION_UNIFORM "LightSpotDirection"
#define MAT_AMBIENT_UNIFORM "MatAmbient"
#define MAT_DIFFUSE_UNIFORM "MatDiffuse"
#define MAT_SPECULAR_UNIFORM "MatSpecular"
#define MAT_EMISSION_UNIFORM "MatEmission"
#define MAT_SHININESS_UNIFORM "MatShininess"

#ifdef __ANDROID__
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <jni.h>
#include <android/asset_manager.h>
#include <SDL_system.h>

#define GL_ENABLE_BIT 0x00002000
#define GL_VIEWPORT_BIT 0x00000800
#define GL_FRAMEBUFFER_EXT 0x8D40
#define GL_COLOR_BUFFER_BIT 0x00004000
#define GL_PIXEL_MODE_BIT 0x00000020
#define GL_POLYGON_BIT 0x00000008
#define GL_LINE_BIT 0x00000004
#define GL_QUADS 0x0007

// This is defined in GLES3!
#define GL_DEPTH_COMPONENT24 0x81A6
#define GL_TEXTURE_WRAP_R 0x8072

#else

#include <GL/glew.h>
#ifndef GL_DEPTH_COMPONENT24_OES
#define GL_DEPTH_COMPONENT24_OES GL_DEPTH_COMPONENT24
#endif // GL_DEPTH_COMPONENT24

#endif

#include <SDL.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <cstring>
#include <string>
#include <vector>
#include "Mathlib.h"
#include "Singleton.h"
#include "Shader.h"
#include "VertexBufferObject.h"

#define LOG_INFO(format, ...) SDL_Log("%s|%-13s:%03d|%-7s| " format, \
    Util::CurrentDateTime().c_str(), \
    Util::MaxStr(Util::Filename(__FILE__), 13).c_str(), __LINE__, \
    Util::MaxStr(__FUNCTION__, 7).c_str(), \
    ##__VA_ARGS__ )
#define LOG_WARN(format, ...) SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "%s|%-13s:%03d|%-7s| " format, \
    Util::CurrentDateTime().c_str(), \
    Util::MaxStr(Util::Filename(__FILE__), 13).c_str(), __LINE__, \
    Util::MaxStr(__FUNCTION__, 7).c_str(), \
    ##__VA_ARGS__ )

#define GL_CHECK(__code) \
do { \
    __code; \
    { \
      Util::get().LastGLError() = glGetError(); \
      if (Util::get().LastGLError() != GL_NO_ERROR) \
      { \
        LOG_WARN("GL Error %i (0x%.8x)", Util::get().LastGLError(), Util::get().LastGLError()); \
      } \
    } \
} while (false);

class Util : public Singleton<Util>
{
public:
    void Init();

    static std::vector<std::string> Split(const std::string &str, char delim = ',');
    static std::string Basename(const std::string& path);
    static std::string Filename(const std::string& path);
    static int StrNICmp(const char *s1, const char *s2, std::size_t n);
    static bool IsFilename(const std::string& path);
    static int Round(double x);
    static int NextPowerOfTwo(int x);
    void Sphere(float radius, int slices, int stacks);
    void Cylinder(float base, float top, float height, int slices, int stacks);
    static std::string GLErrorString(unsigned int error);
    static std::string CurrentDateTime();
    static std::string MaxStr(const std::string& str, std::size_t maxSize);

    static int ReadAllFile(const std::string &fileName, std::string &buffer);

    void DrawAxes(Shader &shader);
    void DrawQuadAtScreen(Shader &shader, const vec3 &coords = {1.0, 1.0, 1.0});

    static SDL_Surface *ConvertSDLSurface(SDL_Surface *surface);

    GLenum &LastGLError() { return _lastGLError; }

    static void SleepMS(int ms);

    void Clear();
private:
    friend class Singleton<Util>;
    Util();
    virtual ~Util();

    GLenum _lastGLError = 0;

    VertexBufferObject _axisVbo;
    VertexBufferObject _quadVbo;
};
