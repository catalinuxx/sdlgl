#pragma once

#include "Util.h"
#include "Texture.h"
#include "IResource.h"

// -------------------------------
// Textures Cubemap (6 images 2D)
// -------------------------------

class TextureCubemap : public IResource, public Texture
{
public:
    virtual GLenum getTextureType() const
    {
        return GL_TEXTURE_CUBE_MAP;
    }
    bool Load(const std::string& name, const std::string &defaultPath = "textures/");

    TextureCubemap() : IResource(), Texture() {}
};


