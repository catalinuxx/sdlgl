#pragma once

#include "Util.h"
#include <iostream>

// -------------------------------
// Textures abstraites
// -------------------------------

class Texture
{
public:
    virtual GLenum getTextureType() const = 0;

    void Gen();
    virtual bool Load(const std::string& name);

    void Bind(GLuint slot) const;
    void Unbind(GLuint slot) const;
    void Clear();

    GLuint getHandle() const
    {
        return m_nHandle;
    }

    static void EnableGenerateMipmaps(bool b)
    {
        s_bGenerateMipmaps = b;
    }

    unsigned int W() const { return _w; }
    unsigned int H() const { return _h; }
    unsigned int D() const { return _d; }

    Texture()
    {
        m_nHandle = 0;
    }
    virtual ~Texture();

protected:
    void Bind() const;
    void Unbind() const;
    bool LoadFile(GLenum target, const std::string& name);
    void LoadData(GLenum target, GLubyte* ptr, unsigned int w, unsigned int h, unsigned int d);

protected:
    GLuint	m_nHandle = 0;				// ID de la texture
    static bool s_bGenerateMipmaps;	// Etat pour le chargement : génération ou non des mipmaps
    unsigned int _w = 0;
    unsigned int _h = 0;
    unsigned int _d = 0;
};

