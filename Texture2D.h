#pragma once

#include "Texture.h"
#include "IResource.h"

// -------------------------------
// Textures 2D
// -------------------------------

class Texture2D : public IResource, public Texture
{
public:
    virtual GLenum getTextureType() const
    {
        return GL_TEXTURE_2D;
    }
    bool Load(const std::string& name);
    bool Load(GLubyte* ptr, unsigned int w, unsigned int h, unsigned int d);

    Texture2D() : IResource(), Texture() {}
};


