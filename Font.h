#pragma once

#include "IResource.h"
#include "Mathlib.h"
#include "Shader.h"
#include "Texture2D.h"

#include <string>
#include <SDL.h>

struct _TTF_Font;
class SDL_Color;
class SDL_Rect;
class SDL_Surface;
class Font;

class FontString
{
public:
    virtual ~FontString();
    bool Set(Font &font, const std::string &text, const SDL_Color &color = {255, 255, 255, 0});
    void Clear() { _tex.Clear(); }
    std::string Get() const { return _text; }
    const Texture2D &GetTexture() const { return _tex; }
private:
    //SDL_Surface *_surface = nullptr;
    Texture2D _tex;
    std::string _text;
};

class Font : public IResource
{
public:
    virtual ~Font();
    bool Load(const std::string& name, std::size_t size = 18);
    void Draw(Shader &shader, const FontString &text);
    struct _TTF_Font *GetFont() { return _font; }

private:
    struct _TTF_Font *_font = nullptr;
};
