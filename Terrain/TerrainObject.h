#pragma once

#include <iostream>
#include "Util.h"
#include "Mathlib.h"
#include <vector>

class Mesh;

class TerrainObject
{
public:
    enum TYPE {PALM};

    void Draw(Shader &shader, const mat4 &modelView, unsigned int id);

    inline Mesh*		getMesh(unsigned int id = 0)	const
    {
        return m_tMesh[id];
    }
    inline const vec3&	getPosition()					const
    {
        return m_vPosition;
    }

    TerrainObject(TYPE mesh, vec4 tr);
    virtual ~TerrainObject();

private:
    vec3				m_vPosition;
    float				m_fAngle;
    std::vector<Mesh*>	m_tMesh;

};

