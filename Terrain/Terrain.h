#pragma once

#include <iostream>
#include "Util.h"
#include "Mathlib.h"
#include "../BoundingBox.h"
#include "../ImageTools.h"
#include <vector>

class Quadtree;
class VertexBufferObject;
class TerrainObject;

class Terrain
{
public:
    void Load(const std::string& heightmap, const BoundingBox& bbox, GLuint chunkSize = 64);
    bool GenerateGrass(const ImageTools::ImageData& map, unsigned int density);
    bool GenerateVegetation(const ImageTools::ImageData& map, unsigned int density);
    bool ComputeBoundingBox();

    int  DrawGround(Shader &shader, bool bReflection);
    int  DrawGrass(Shader &shader, bool bReflection);
    int  DrawObjects(Shader &shader, const mat4 &modelView, bool bReflection);
    void DrawInfinitePlane(Shader &shader, const vec3& eye, float max_distance);

    vec3  getPosition(float x_clampf, float z_clampf) const;
    vec3  getNormal(float x_clampf, float z_clampf) const;
    vec3  getTangent(float x_clampf, float z_clampf) const;

    const BoundingBox&	getBoundingBox()
    {
        return m_BBox;
    }

    Terrain();
    virtual ~Terrain();

private:


private:
    BoundingBox				m_BBox;
    GLuint					m_nHMWidth, m_nHMHeight;

    // Quadtree pour g�rer les donn�es
    Quadtree*				m_pQuadtree;

    // VBO
    VertexBufferObject*		m_pGroundVBO;
    VertexBufferObject*		m_pGrassVBO;

    VertexBufferObject _infPlaneVbo;

};


