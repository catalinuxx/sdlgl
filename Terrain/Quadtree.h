#pragma once

#include <iostream>
#include <string>
#include "Mathlib.h"
#include "Util.h"

class QuadtreeNode;
class BoundingBox;

class Quadtree
{
public:
    void Build(BoundingBox* pBBox, ivec2 HMSize, unsigned int minHMSize);
    void ComputeBoundingBox(const vec3* vertices);

    int  DrawGround(bool bReflection);
    int  DrawGrass(Shader &shader, bool bReflection);
    int  DrawObjects(Shader &shader, const mat4 &modelView, bool bReflection);
    void DrawBBox(Shader &shader);

    QuadtreeNode*	FindLeaf(vec2& pos);

    Quadtree()
    {
        m_pRoot = NULL;
    }
    virtual ~Quadtree();

private:
    QuadtreeNode*	m_pRoot;	// Noeud principal du Quadtree
};


