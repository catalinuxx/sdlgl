#pragma once

#include <iostream>
#include <vector>
#include <list>
#include "../BoundingBox.h"
#include "Util.h"

#define CHILD_NW	0
#define CHILD_NE	1
#define CHILD_SW	2
#define CHILD_SE	3

#define CHUNK_BIT_TESTCHILDREN		0x1
#define CHUNK_BIT_WATERREFLECTION	0x2



class Frustum;
class TerrainChunk;

class QuadtreeNode
{
public:
    //fonction recursive de traitement des noeuds
    void Build(unsigned int depth, ivec2 pos, ivec2 HMsize, unsigned int minHMSize);
    void ComputeBoundingBox(const vec3* vertices);

    int  DrawGround(Frustum* pFrust, int options);
    int  DrawGrass(Shader &shader, bool bReflection);
    int  DrawObjects(Shader &shader, const mat4 &modelView, bool bReflection);
    void DrawBBox(Shader &shader, bool bTest);

    inline bool isALeaf() const
    {
        return m_pChildren == 0;
    }
    inline const BoundingBox&	getBoundingBox()
    {
        return m_BBox;
    }
    inline void setBoundingBox(const BoundingBox& bbox)
    {
        m_BBox = bbox;
    }
    inline QuadtreeNode*	getChildren()
    {
        return m_pChildren;
    }
    inline TerrainChunk*	getChunk()
    {
        return m_pTerrainChunk;
    }

    QuadtreeNode()
    {
        m_pChildren = NULL;
        m_pTerrainChunk = NULL;
        m_nLOD = 0;
    }
    virtual ~QuadtreeNode();

private:
    int				m_nLOD;				// Niveau de LOD
    float			m_fDistance;		// Distance du centre par rapport � la cam�ra
    BoundingBox		m_BBox;				// Taille de la BBOX 3D englobant le noeud
    QuadtreeNode*	m_pChildren;		// Enfants du noeud
    TerrainChunk*	m_pTerrainChunk;	// Chunk (partie du terrain) si c'est une feuille
    VertexBufferObject _bboxVbo1, _bboxVbo2, _bboxVbo3;
};

