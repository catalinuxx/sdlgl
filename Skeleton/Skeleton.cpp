
#include "Skeleton.h"

Skeleton::Skeleton()
{
    // Set joints
    for (int i = 0; i < MaxJoints; ++i)
        _joints.push_back(Joint((JointType)i, vec3(0.0, 0.0, 0.0)));
}

Skeleton::~Skeleton()
{
}

void Skeleton::SetJoint(JointType type, const vec3 &pos)
{
    assert(type < MaxJoints);

    switch (type)
    {
    case MaxJoints:
        return;
    case HipCenter:
    case Spine:
    case ShoulderCenter:
    case Head:
    case ShoulderLeft:
    case ElbowLeft:
    case WristLeft:
    case HandLeft:
    case ShoulderRight:
    case ElbowRight:
    case WristRight:
    case HandRight:
    case HipLeft:
    case KneeLeft:
    case AnkleLeft:
    case FootLeft:
    case HipRight:
    case KneeRight:
    case AnkleRight:
    case FootRight:
        break;
    }

    _joints[(int)type] = Joint(type, pos);
    if (_loader)
        _loader->SetJoint(JointNames[type], pos);

    //LOG_INFO("Set joint %s to %.2f,%.2f,%2f", JointNames[type].c_str(), pos.x, pos.y, pos.z);
}

void Skeleton::SetPosition(const vec3 &pos)
{
    LOG_INFO("Set position to %.2f,%.2f%.2f", pos.x, pos.y, pos.z);
    _pos = pos;
}

void Skeleton::Load(const std::string &filename)
{
    _loader = new MD5Loader(filename);
    _loader->parse();
}

void Skeleton::Draw(Shader &shader)
{
    assert(_loader);
    _loader->draw(shader);
}

void Skeleton::DrawJoints(Shader &shader)
{
    assert(_loader);
    _loader->drawJoints(shader);
}
