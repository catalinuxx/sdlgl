#pragma once

#include "Mesh.h"
#include "MD5Loader.h"

enum JointType
{
    HipCenter,
    Spine,
    ShoulderCenter,
    Head,
    ShoulderLeft,
    ElbowLeft,
    WristLeft,
    HandLeft,
    ShoulderRight,
    ElbowRight,
    WristRight,
    HandRight,
    HipLeft,
    KneeLeft,
    AnkleLeft,
    FootLeft,
    HipRight,
    KneeRight,
    AnkleRight,
    FootRight,

    MaxJoints,
};

static const std::vector<std::string> JointNames =
{
    "HipCenter",
    "Spine",
    "ShoulderCenter",
    "Head",
    "ShoulderLeft",
    "ElbowLeft",
    "WristLeft",
    "HandLeft",
    "ShoulderRight",
    "ElbowRight",
    "WristRight",
    "HandRight",
    "HipLeft",
    "KneeLeft",
    "AnkleLeft",
    "FootLeft",
    "HipRight",
    "KneeRight",
    "AnkleRight",
    "FootRight",
};

struct Joint
{
    Joint(JointType type, const vec3 &pos):
        type(type), pos(pos) {}
    JointType type;
    vec3 pos;
};

class Skeleton
{
public:
    Skeleton();
    void SetJoint(JointType type, const vec3 &pos);
    void SetPosition(const vec3 &pos);
    void Load(const std::string &filename);
    void Draw(Shader &shader);
    void DrawJoints(Shader &shader);
    virtual ~Skeleton();
private:
    void ComputeWeights();
private:
    MD5Loader *_loader = nullptr;
    std::vector<Joint> _joints;
    vec3 _pos = vec3(0.0, 0.0, 0.0);
};
