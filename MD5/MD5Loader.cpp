#include "MD5Loader.h"
#include "Util.h"
#include <fstream>
#include <iostream>
#include <cmath>

#include "ResourceManager.h"

MD5Loader::~MD5Loader()
{
    std::cout << "MD5Loader destructor" << std::endl;
    for (std::vector < Md5Joint * >::iterator jointIt = this->joints.begin();
            jointIt != this->joints.end(); jointIt++)
    {
        delete (*jointIt);
    }
    this->joints.clear();
    for (std::vector < Md5Mesh * >::iterator meshIt = this->meshes.begin();
            meshIt != this->meshes.end(); meshIt++)
    {
        Md5Mesh *mesh = (*meshIt);
        for (std::vector <  Md5Vert * >::iterator vertIt = mesh->vertices.begin();
                vertIt != mesh->vertices.end(); vertIt++)
        {
            delete (*vertIt);
        }
        mesh->vertices.clear();


        for (std::vector <  Md5Tri * >::iterator triIt = mesh->triangles.begin();
                triIt != mesh->triangles.end(); triIt++)
        {
            delete (*triIt);
        }
        mesh->triangles.clear();

        for (std::vector <  Md5Weight * >::iterator weightIt = mesh->weights.begin();
                weightIt != mesh->weights.end(); weightIt++)
        {
            delete (*weightIt);
        }
        mesh->weights.clear();

        delete mesh;
    }
    this->meshes.clear();


}

MD5Loader::MD5Loader(std::string fileName)
{
    std::cout << "MD5Loader constructor" << std::endl;
    this->fileName = "meshs/" + fileName;

}

void MD5Loader::parse()
{
    std::lock_guard<std::mutex> lock(_parseMutex); // Lock SetJoint

    char tempBuffer[512];
    std::ifstream md5File(this->fileName.c_str());

    if (!md5File.good())
    {
        std::cerr << "Cannot open MD5 model file: " << this->fileName << std::endl;
        return;
    }
    md5File.getline(tempBuffer, 512);
    std::string tempBufferString(tempBuffer);
    if (tempBufferString != "MD5Version 10")
    {
        std::cerr << "Unknown file format: First line \"\n" << tempBufferString << "\n\" expected \"MD5Version 10\"" << std::endl;
        return;
    }

    while (md5File.good())
    {
        md5File.getline(tempBuffer, 512);
        tempBufferString = tempBuffer;

        //std::cout << tempBufferString << std::endl;
        if (tempBufferString.find("joints {") != std::string::npos)
        {
            //std::cout << "Joints found" << std::endl;
            while (md5File.good())
            {
                md5File.getline(tempBuffer, 512);
                tempBufferString = tempBuffer;
                if (tempBufferString.find("}") != std::string::npos)
                {
                    //std::cout << "End of joints" << std::endl;
                    break;
                }
                this->addJoint(tempBufferString);
            }
        }

        if (tempBufferString.find("mesh {") != std::string::npos)
        {
            //std::cout << "Mesh found" << std::endl;
            Md5Mesh *newMesh = new Md5Mesh();

            while (md5File.good())
            {
                md5File.getline(tempBuffer, 512);
                tempBufferString = tempBuffer;
                if (tempBufferString.find("}") != std::string::npos)
                {
                    //std::cout << "End of mesh" << std::endl;
                    break;
                }
                this->addToMesh(tempBufferString, newMesh);
            }
            this->meshes.push_back(newMesh);
        }
    }
    std::cout << "Loaded " << this->joints.size() << " joints." << std::endl;
    std::cout << "Loaded " << this->meshes.size() << " meshes." << std::endl;
    int meshCount = 0;
    for (std::vector < Md5Mesh * >::iterator it = this->meshes.begin(); it != this->meshes.end (); it++)
    {
        std::cout << "Mesh:\t" << meshCount++ << std::endl;
        std::cout << "Verts:\t" << (*it)->vertices.size() << std::endl;
        std::cout << "Tris:\t" << (*it)->triangles.size() << std::endl;
        std::cout << "Weights:\t" << (*it)->weights.size() << std::endl;

        (*it)->vbo.getPosition().resize((*it)->triangles.size() * 3);
        (*it)->vbo.getTexcoord().resize((*it)->triangles.size() * 3);
        if (!(*it)->vbo.Create(GL_DYNAMIC_DRAW))
        {
            std::cerr << "VBO.Create() failed" << std::endl;
            assert(false);
        }
    }
    _vboJoints.getPosition().resize(this->joints.size() * 2);
    if (!_vboJoints.Create(GL_DYNAMIC_DRAW))
    {
        std::cerr << "VBO.Create() failed" << std::endl;
        assert(false);
    }

    ComputeVBO();
    LOG_INFO("BBOX: x:%.2f,%.2f y:%.2f,%.2f z:%.2f,%.2f", _bbox.min.x, _bbox.max.x, _bbox.min.y, _bbox.max.y, _bbox.min.z, _bbox.max.x);
}

void
MD5Loader::addToMesh(std::string meshLine, Md5Mesh *mesh)
{
    if (meshLine.find("numtris")!= std::string::npos)
    {
        //TODO: Check triangle count
        std::cout << "NUMTRIES: " << meshLine << std::endl;
    }
    else if (meshLine.find("numverts")!= std::string::npos)
    {
        //TODO: Check vert count
        std::cout << "NUMVERTS: " << meshLine << std::endl;
    }
    else if (meshLine.find("numweights")!= std::string::npos)
    {
        //TODO: Check weight count
        std::cout << "NUMWEIGHTS: " << meshLine << std::endl;
    }
    else if (meshLine.find("tri") != std::string::npos)
    {
        this->addTri(meshLine, mesh);
    }
    else if (meshLine.find("vert") != std::string::npos)
    {
        addVert(meshLine, mesh);
    }
    else if (meshLine.find("weight") != std::string::npos)
    {
        addWeight(meshLine, mesh);
    }
    else if (meshLine.find("shader") != std::string::npos)
    {
        addShader(meshLine, mesh);
    }
}

void MD5Loader::addTri(std::string meshLine, Md5Mesh *mesh)
{
    //std::cout <<"Adding tri"<<std::endl;
    Md5Tri *newTri = new Md5Tri();
    sscanf(meshLine.c_str(), "\ttri %d %d %d %d",
           &(newTri->index), &(newTri->vertices[0]), &(newTri->vertices[1]), &(newTri->vertices[2]));
    /*std::cout << meshLine << std::endl
    	<< "Tri:\t" << newTri->index << " " << newTri->vertices[0] << " " << newTri->vertices[1]
    	<< " " << newTri->vertices[2] << std::endl;*/
    mesh->triangles.push_back(newTri);
}

void MD5Loader::addShader(std::string meshLine, Md5Mesh *mesh)
{
    char shaderBuffer[512];
    sscanf (meshLine.c_str(), "\tshader \"%s\"", shaderBuffer);
    mesh->shader = shaderBuffer;
    auto pos = mesh->shader.find('\"');
    if (pos != std::string::npos)
        mesh->shader = mesh->shader.substr(0, pos);
    LOG_INFO("Shader: %s", mesh->shader.c_str());
    mesh->tex.reset((Texture2D *)ResourceManager::get().LoadResource(ResourceManager::TEXTURE2D, mesh->shader + ".tga"));
}

void MD5Loader::addWeight(std::string meshLine, Md5Mesh *mesh)
{
    //std::cout <<"Adding weight"<<std::endl;
    Md5Weight *newWeight = new Md5Weight();
    sscanf(meshLine.c_str(), "\tweight %d %d %f ( %f %f %f )",
           &(newWeight->index), &(newWeight->joint), &(newWeight->weightValue),
           &(newWeight->position.x), &(newWeight->position.y), &(newWeight->position.z));
    /*std::cout << meshLine << std::endl
    	<< "Weight:\t" << newWeight->index << " " << newWeight->joint << " " << newWeight->weightValue << " "
    	<< newWeight->position.x << " " << newWeight->position.y << " " << newWeight->position.z << std::endl;*/

    mesh->weights.push_back(newWeight);
}

void MD5Loader::addVert(std::string meshLine, Md5Mesh *mesh)
{
    //std::cout <<"Adding vert"<<std::endl;
    Md5Vert *newVert = new Md5Vert();
    sscanf(meshLine.c_str(), "\tvert %d ( %f %f ) %d %d",
           &(newVert->index), &(newVert->textureCoords.x), &(newVert->textureCoords.y),
           &(newVert->weightIndex), &(newVert->weightElementCount));
    /*std::cout << meshLine << std::endl
    	<< "Vert:\t" << newVert->index << " ( " << newVert->textureCoords.u << " " << newVert->textureCoords.v
    	<< " ) " << newVert->weightIndex << " " << newVert->weightElementCount << std::endl;*/
    mesh->vertices.push_back(newVert);
}

void MD5Loader::addJoint(std::string jointString)
{
    char nameBuffer[512];
    //std::cout << "Adding joint:\t" <<  jointString<<std::endl;
    Md5Joint *newJoint = new Md5Joint();
    sscanf (jointString.c_str(), "\t\"%s\t%d ( %f %f %f ) ( %f %f %f )", nameBuffer, &(newJoint->parent), &(newJoint->position.x), &(newJoint->position.y), &(newJoint->position.z),
            &(newJoint->orientation.x), &(newJoint->orientation.y), &(newJoint->orientation.z));

    newJoint->name = nameBuffer;
    newJoint->name = newJoint->name.substr(0, newJoint->name.length()-1);
    //std::cout << "Joint name:" << newJoint->name << "\tParent:\t" << newJoint->parent << "\tPosition x:y:z\t" << newJoint->position.x <<":"<< newJoint->position.y <<":"<< newJoint->position.z<< "\tOrientation x:y:z\t" << newJoint->orientation.x <<":"<< newJoint->orientation.y <<":"<< newJoint->orientation.z << std::endl;

    // Calculate w value for orientation (is normalized, so 1 = x^2 + y^2 + z^2 + w^2)
    float x = newJoint->orientation.x;
    float y = newJoint->orientation.y;
    float z = newJoint->orientation.z;
    float w = 1.0f - (x*x) - (y*y) - (z*z);
    if (w < 0.0f)
    {
        w = 0.0f;
    }
    else
    {
        w = -sqrt(w);
    }
    newJoint->orientation.w = w;
    if (newJoint->parent == -1.0f)
    {
        //std::cout << "Is the origin joint"<<std::endl;
        this->originJoint = newJoint;
    }
    this->joints.push_back(newJoint);
}

void MD5Loader::draw(Shader &shader)
{
    std::lock_guard<std::mutex> lock(_parseMutex); // Lock SetJoint

    for (auto &mesh : meshes)
    {
        mesh->vbo.Enable(shader);

        if (mesh->changed)
        {
            mesh->vbo.BufferPosition();
            mesh->vbo.BufferTexCoord();
            mesh->changed = false;
        }

        int slot = 0;
        if (mesh->tex)
        {
            mesh->tex->Bind(slot);
            shader.SetTexActive(true, slot);
            shader.SetTexInv(false, slot);
            slot++;
        }

        GL_CHECK(glDrawArrays(GL_TRIANGLES, 0, mesh->triangles.size() * 3));

        if (mesh->tex)
        {
            shader.SetTexActive(false, slot);
            mesh->tex->Unbind(--slot);
        }

        mesh->vbo.Disable(shader);
    }
}

void MD5Loader::drawJoints(Shader &shader)
{
    int jointNo = 0;
    std::size_t vboIdx = 0;
    for (std::vector < Md5Joint * >::iterator it = this->joints.begin(); it != this->joints.end (); it++)
    {
        Md5Joint *parent, *joint = (*it);
        if (joint->parent != -1.0f)
        {
            //Not drawing origin

            parent = this->joints[joint->parent];

            //std::cout << "Drawing joint "<< jointNo << std::endl;
            jointNo++;

            //std::cout << parent->position.x << '\t'<< parent->position.y<< '\t'<< parent->position.z<<std::endl;
            //std::cout << joint->position.x << '\t'<< joint->position.y<< '\t'<< joint->position.z<<std::endl;
            _vboJoints.getPosition()[vboIdx++] = parent->position;
            _vboJoints.getPosition()[vboIdx++] = joint->position;
        }
    }

    _vboJoints.Enable(shader);
    _vboJoints.BufferPosition();
    GL_CHECK(glDrawArrays(GL_LINES, 0, this->joints.size() * 2));
    _vboJoints.Disable(shader);
}

vec3 MD5Loader::genTriNormal(vec3 &p1, vec3 &p2, vec3 &p3)
{
    vec3 vertex1 = p2-p1;
    vec3 vertex2 = p3-p1;
    vertex2.cross(vertex1);
    vertex2.normalize();
    return vertex2;
}

quat getRotationTo(const vec3& src, const vec3& dest, const vec3& fallbackAxis = vec3(0,0,0))
{
    // Based on Stan Melax's article in Game Programming Gems
    quat q;
    // Copy, since cannot modify local
    vec3 v0 = src;
    vec3 v1 = dest;
    v0.normalize();
    v1.normalize();

    float d = v0.dot(v1);
    // If dot == 1, vectors are the same
    if (d >= 1.0f)
    {
        return quat();
    }
    if (d < (1e-6f - 1.0f))
    {
        if (fallbackAxis != vec3())
        {
            // rotate 180 degrees about the fallback axis
            q.set(fallbackAxis, 180);
        }
        else
        {
            // Generate an axis
            /*vec4 axis = vec3::UNIT_X.crossProduct(*this);
            if (axis.isZeroLength()) // pick another if colinear
                axis = vec3::UNIT_Y.crossProduct(*this);
            axis.normalize();
            q.set(axis, 180);*/
        }
    }
    else
    {
        float s = sqrt( (1+d)*2 );
        float invs = 1.0 / s;

        vec3 c = v0;
        c.cross(v1);

        q.x = c.x * invs;
        q.y = c.y * invs;
        q.z = c.z * invs;
        q.w = s * 0.5f;
        q.normalize();
    }
    return q;
}

void MD5Loader::SetJoint(const std::string &name, const vec3 &pos)
{
    std::lock_guard<std::mutex> lock(_parseMutex); // Lock parse

    bool found = false;
    for (auto &joint : joints)
    {
        if (name == joint->name)
        {
            found = true;

            vec3 v1 = joint->position;
            vec3 v2 = pos * 4;
            /*quat q;
            vec3 a;
            a.cross(v1, v2);
            q.x = a.x; q.y = a.y; q.z = a.z;
            q.w = sqrt((v1.length() * v1.length()) * (v2.length() * v2.length())) + v1.dot(v2);*/

            joint->position = v2;
            joint->orientation = getRotationTo(v1, v2);

            // LOG_WARN("Joint %s set to %.2f,%.2f,%.2f!", name.c_str(), pos.x, pos.y, pos.z);
        }
    }
    if (found)
    {
        ComputeVBO();
    }
    else
    {
        //LOG_WARN("Joint %s is not mapped!", name.c_str());
    }
}

void MD5Loader::ComputeVBO()
{
    _bbox.min = vec3(100000.0f, 100000.0f, 100000.0f);
    _bbox.max = vec3(-100000.0f, -100000.0f, -100000.0f);
    for (std::vector < Md5Mesh * >::iterator it = this->meshes.begin (); it != this->meshes.end (); it++)
    {
        //std::cout << "MESH" << std::endl;
        Md5Mesh* mesh = (*it);
        //glBegin(GL_TRIANGLES);
        std::size_t vboIdx = 0;
        for (std::vector < Md5Tri * >::iterator triIt = mesh->triangles.begin(); triIt != mesh->triangles.end(); triIt++)
        {
            //std::cout << "TRIANGLE" << std::endl;
            Md5Tri *triangle = (*triIt);
            for (int i = 0; i < 3; i++)
            {
                vec3 finalPosition(0.0f, 0.0f, 0.0f);
                Md5Vert *vertex = mesh->vertices[triangle->vertices[i]];
                int weightCount = vertex->weightElementCount;
                int firstWeight = vertex->weightIndex;
                for (int weightIndex = firstWeight; weightIndex < firstWeight+weightCount; weightIndex++)
                {
                    Md5Weight *weight = mesh->weights[weightIndex];
                    Md5Joint *joint = this->joints[weight->joint];
                    vec3 weightPosition = joint->orientation.rotate(weight->position);
                    finalPosition += (joint->position + weightPosition) * weight->weightValue;
                }
                _bbox.Add(finalPosition);
                //std::cout << "VERTEX pos " << finalPosition.x <<"," << finalPosition.y << "," << finalPosition.z << std::endl;
                if (vboIdx >= mesh->vbo.getPosition().size())
                {
                    LOG_WARN("Too many positions! vboIdx:%d numPos:%d", vboIdx, mesh->vbo.getPosition().size());
                    assert(vboIdx < mesh->vbo.getPosition().size());
                }

                mesh->vbo.getPosition()[vboIdx] = finalPosition;
                mesh->vbo.getTexcoord()[vboIdx] = vertex->textureCoords;
                //std::cout << "VERTEX texPos " << vertex->textureCoords.x <<"," << vertex->textureCoords.y << std::endl;
                vboIdx++;
            }
        }

        mesh->changed = true;
    }
}

void MD5Loader::ComputeBoundingBox()
{
    _bbox.min = vec3(100000.0f, 100000.0f, 100000.0f);
    _bbox.max = vec3(-100000.0f, -100000.0f, -100000.0f);

    for (auto &mesh : meshes)
    {
        auto tPosition = mesh->vbo.getPosition();
        for (int i = 0; i < (int)tPosition.size(); i++)
            _bbox.Add(tPosition[i]);
    }
}
