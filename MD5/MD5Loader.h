#pragma once

#include <mutex>
#include <string>
#include "mesh.h"
#include "MathLib.h"
#include "VertexBufferObject.h"
#include <vector>

class Md5Joint
{
public:
    std::string name;
    int parent;
    vec3 position;
    quat orientation;
};
class Md5Vert
{
public:
    int index;
    vec2 textureCoords;
    int weightIndex;
    int weightElementCount;
    vec3 normal;
};

class Md5Tri
{
public:
    int index;
    int vertices[3];
};

class Md5Weight
{
public:
    int index;
    int joint;
    float weightValue;
    vec3 position;
};

class Md5Mesh
{
public:
    std::string shader;
    std::vector<Md5Vert*> vertices;
    std::vector<Md5Tri*> triangles;
    std::vector<Md5Weight*> weights;
    VertexBufferObject vbo;
    std::unique_ptr<Texture2D> tex;
    volatile bool changed = true;
};

class MD5Loader
{
public:
    ~MD5Loader();
    MD5Loader(std::string fileName);
    void drawJoints(Shader &shader);
    void draw(Shader &shader);
    void parse();
    void ComputeBoundingBox();

    void SetJoint(const std::string &name, const vec3 &pos);

private:
    std::vector<Md5Joint*> joints;
    std::vector<Md5Mesh*> meshes;
    Md5Joint* originJoint;
    std::string fileName;
    VertexBufferObject _vboJoints;

    void addJoint(std::string jointString);
    void addToMesh(std::string meshLine, Md5Mesh *mesh);
    void addTri(std::string meshLine, Md5Mesh *mesh);
    void addVert(std::string meshLine, Md5Mesh *mesh);
    void addWeight(std::string meshLine, Md5Mesh *mesh);
    void addShader(std::string meshLine, Md5Mesh *mesh);

    vec3 genTriNormal(vec3 &p1, vec3 &p2, vec3 &p3);
    void ComputeVBO();

    std::mutex _parseMutex;
    BoundingBox _bbox;
};
