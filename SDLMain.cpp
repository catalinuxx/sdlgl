// Using SDL, SDL OpenGL, standard IO, and, strings

#include "Util.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdio.h>

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
#include <assert.h>
#include <stdexcept>

#include "Mathlib.h"
#include "Camera.h"
#include "Mesh.h"
#include "Timer.h"
#include "Sky.h"

#include "glInfo.h"
#include "Shader.h"
#include "TextureCubemap.h"
#include "Texture2D.h"
#include "FrameBufferObject.h"
#include "Inputs.h"
#include "Scenes/SceneManager.h"
#include "ResourceManager.h"
#include "VarManager.h"
#include "Scenes/SceneBase.h"
#include "Effects.h"

#define CHECK_ERRORS

#include "SDLMain.h"

bool _doubleClick = false;
GLfloat _clickTime = -100.0f;

SDLMain::SDLMain(int argc, char **argv)
{
    LOG_INFO("XXXXX %s started on %s", argv[0], Util::CurrentDateTime().c_str());

    // Initialize variables from config
    VarManager& var = VarManager::get();
    std::string cfgFile;
    if (argc > 1)
        cfgFile = argv[1];
    else
    {
        cfgFile = argv[0];
        auto pos = cfgFile.find(".exe");
        if (pos != std::string::npos)
            cfgFile.replace(pos, 4, ".cfg");
        else
            cfgFile += ".cfg";
    }
    var.FromCfg(cfgFile);
    var.Print("main");

    // Initialize SDL
    auto sdlInitRet = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);
    if (sdlInitRet < 0)
        throw std::runtime_error(std::string("SDL could not initialize! SDL Error: ") + SDL_GetError());
    LOG_INFO("SDL Init return value: %d\n", sdlInitRet);

    if (var.getb("cam_joystick"))
    {
        _joystick = SDL_JoystickOpen(0);
        if (_joystick)
            LOG_INFO("We have a joystick!");
    }

    // Initialize SDL image support
    auto imgInitRet = IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF | IMG_INIT_WEBP);
    if (!imgInitRet)
        throw std::runtime_error(std::string("IMG_Init failed: ") + IMG_GetError());
    LOG_INFO("IMG_Init return value: %d\n", imgInitRet);

    // Initialize SDL font support
    auto ttfInitRet = TTF_Init();
    if (ttfInitRet < 0)
        throw std::runtime_error(std::string("TTF_Init failed: ") + TTF_GetError());
    LOG_INFO("TTF_Init return value: %d\n", ttfInitRet);

    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);

    SDL_DisplayMode mode;
    //SDL_GetDisplayMode(0, 0, &mode);
    SDL_GetCurrentDisplayMode(0, &mode);
    LOG_INFO("Width: %d, Height: %d\n ", mode.w, mode.h);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
#ifdef WIN32
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
#else
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    var.set("win_width", mode.w);
    var.set("win_height", mode.h);
#endif

    // Create window
    _window = SDL_CreateWindow("SDLGL",
                               SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                               var.geti("win_width"), var.geti("win_height"),

#ifdef ANDROID
                               SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_BORDERLESS
#else
                               SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
#endif // HAVE_GLES
                              );
    if (_window == NULL )
        throw std::runtime_error(std::string("Window could not be created! SDL Error: ") + SDL_GetError() );

    // Create context
    _glContext = SDL_GL_CreateContext(_window);
    if (_glContext == NULL )
        throw std::runtime_error(std::string("OpenGL context could not be created! SDL Error: ") + SDL_GetError());

#ifdef _WIN32
    // Initialize GLEW
    glewExperimental = GL_TRUE;
    GLenum glewError = glewInit();
    if (glewError != GLEW_OK )
        throw std::runtime_error(std::string("Error initializing GLEW! ") + (const char *)glewGetErrorString(glewError ));
#endif

#ifdef HAVE_GLES
    // Use Vsync
    /*if ( SDL_GL_SetSwapInterval( 1 ) < 0 )
    {
        LOG_INFO("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
    }*/
#endif

    initGL();
    reshapeGL(var.geti("win_width"), var.geti("win_height"));
}

SDLMain::~SDLMain()
{
    close();
}

int SDLMain::run()
{
    //Main loop flag
    bool quit = false;

    //Event handler
    SDL_Event e;

    //Enable text input
    SDL_StartTextInput();

    //While application is running
    while (!quit)
    {
        //Handle events on queue
        while ( SDL_PollEvent( &e ) != 0 )
        {
            //LOG_INFO("e.type: %x", e.type);

            //User requests quit
            if (e.type == SDL_QUIT )
            {
                LOG_INFO("Received SDL_QUIT");
                quit = true;
            }
            else if (e.type == SDL_TEXTINPUT && handleKey(e.text.text[ 0 ]) < 0)
                quit = true;
            else if (e.type == SDL_KEYDOWN)
            {
                unsigned char key = (char)e.key.keysym.sym;
                Inputs::get().SpecialFunc(key);
                SceneManager::get().Keyboard(true, key);
            }
            else if (e.type == SDL_KEYUP)
            {
                unsigned char key = (char)e.key.keysym.sym;
                Inputs::get().KeyboardUpFunc(key);
                Inputs::get().SpecialUpFunc(key);
            }
            else if (e.type == SDL_MOUSEMOTION || e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP)
            {
                int x = 0, y = 0;
                SDL_GetMouseState(&x, &y);
                if (e.type == SDL_MOUSEMOTION)
                {
                    if (Inputs::get().MouseButton(SDL_BUTTON_LEFT))
                        Inputs::get().MotionFunc(x, y);
#ifdef __WIN32
                    _clickTime = -100.0f; // Disable double click check
#endif // __WIN32
                }
                else if (e.type == SDL_MOUSEBUTTONDOWN)
                {
                    if (e.button.button == SDL_BUTTON_LEFT)
                    {
                        Inputs::get().MouseFunc(e.button.button, e.button.state, x, y);
                        _ppos = Inputs::get().MousePos();
                        auto diff = Timer::get().getCurrentTime() - _clickTime;
                        if (diff < 0.6f)
                        {
                            _doubleClick = true;
                            LOG_INFO("SDL_MOUSEBUTTONDOWN Double click. diff(%.2f)", _doubleClick, diff);
                        }
                        else
                        {
                            LOG_INFO("SDL_MOUSEBUTTONDOWN Normal click %d. diff(%.2f)", _doubleClick, diff);
                            _doubleClick = false;
                        }
                        _clickTime = Timer::get().getCurrentTime();
                    }
                }
                else if (e.type == SDL_MOUSEBUTTONUP)
                {
                    if (Inputs::get().MouseButton(SDL_BUTTON_LEFT))
                    {
                        Inputs::get().MouseFunc(e.button.button, e.button.state, x, y);
                        //LOG_INFO("SDL_MOUSEBUTTONUP %d", _doubleClick);
                    }
                }
            }
            else if (e.type == SDL_WINDOWEVENT && e.window.event == SDL_WINDOWEVENT_RESIZED)
                reshapeGL(e.window.data1, e.window.data2);
            else if (e.type == SDL_MOUSEWHEEL)
            {
                if (e.wheel.y < 0)
                    Camera::get().PlayerMoveForward(-2.0f);
                else
                    Camera::get().PlayerMoveForward(+2.0f);
            }
        }

        idle();

        displayGL();

        //Update screen
        SDL_GL_SwapWindow(_window);
    }

    //Disable text input
    SDL_StopTextInput();

    return 0;
}

void SDLMain::idle(void)
{
    Timer& timer = Timer::get();
    Inputs& inputs = Inputs::get();
    VarManager& var = VarManager::get();

    timer.Idle();

    ivec2 pos = Inputs::get().MousePos();
    //static ivec2 ppos = pos;

    float fElapsedTime = var.getf("time_speed") * timer.getElapsedTime();

    if (inputs.MouseButton(SDL_BUTTON_LEFT) && var.getb("enable_move_control"))
    {
        float sensivity = var.getf("mouse_sensivity");
        if (_doubleClick)
            Camera::get().PlayerMoveForward(10.0f * fElapsedTime);
        Camera::get().Rotate((float)(pos.x - _ppos.x) * sensivity,
                             (float)(pos.y - _ppos.y) * sensivity);
    }
    _ppos = pos;

    SceneManager::get().Idle(fElapsedTime);

    if (inputs.SKey(SDL_SCANCODE_UP) )		Camera::get().PlayerMoveForward(10.0f * fElapsedTime);
    if (inputs.SKey(SDL_SCANCODE_DOWN) )	Camera::get().PlayerMoveForward(-10.0f * fElapsedTime);
    if (inputs.SKey(SDL_SCANCODE_LEFT) || inputs.SKey(SDL_SCANCODE_RIGHT))
    {
        if (!var.getb("left_right_rotate"))
        {
            if (inputs.SKey(SDL_SCANCODE_LEFT) )	Camera::get().PlayerMoveStrafe(20.0f * fElapsedTime);
            if (inputs.SKey(SDL_SCANCODE_RIGHT) )	Camera::get().PlayerMoveStrafe(-20.0f * fElapsedTime);
        }
        else
        {
            int direction = 0;
            if (inputs.SKey(SDL_SCANCODE_LEFT)) direction = -1;
            if (inputs.SKey(SDL_SCANCODE_RIGHT)) direction = 1;
            Camera::get().RotateX((float)direction * fElapsedTime);
        }
    }

    if (_joystick)
    {
        SDL_JoystickUpdate();
        float x = (float) SDL_JoystickGetAxis(_joystick, 0) / 32768.0;
        float y = (float) SDL_JoystickGetAxis(_joystick, 1) / 32768.0;
        //float z = (float) SDL_JoystickGetAxis(_joystick, 2) / 32768.0;
        static float gx = 0.0;
        static float gy = 0.0;
        //static float gz = 0.0;
        //LOG_INFO("%.2f:%.2f:%.2f", x, y, z);
        //Camera::get().Rotate(xAxisForceInGs, yAxisForceInGs);
        if (std::abs(gx - x) > 0.02)
        {
            //LOG_INFO("%.2f", gx - x);
            gx = x;
            if (!Inputs::get().MouseButton(SDL_BUTTON_LEFT))
                Camera::get().setJoyAngleX(-x * 2 * M_PI);
        }
        if (std::abs(gy - y) > 0.02)
        {
            //LOG_INFO("%.2f", gy - y);
            gy = y;
            if (!Inputs::get().MouseButton(SDL_BUTTON_LEFT))
                Camera::get().setJoyAngleY(y * 2 * M_PI);
        }
    }

    Effects::get().idle();
}

void SDLMain::reshapeGL(int newwidth, int newheight)
{
    VarManager& var = VarManager::get();
    var.set("win_width", newwidth);
    var.set("win_height", newheight);

    LOG_INFO("Window resized to %dx%d", newwidth, newheight);

    float ratio = (float)newwidth / (float)newheight;
    GL_CHECK(glViewport(0, 0, (GLint)newwidth, (GLint)newheight));

    Camera::get().SetProjection(mat4());
    Camera::get().Perspective(var.getf("cam_fovy"), ratio, var.getf("cam_znear"), var.getf("cam_zfar"));

    Camera::get().SetModelView(mat4());

    // Recréation des FBO
    Effects::get().reshapeFBO(newwidth, newheight);
}

void SDLMain::displayGL(void)
{
    VarManager& var = VarManager::get();
    SceneManager& scenes = SceneManager::get();

    GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

    Camera::get().SelfRenderLookAt();

    // First render step
    scenes.PreRender();

    // Second render step
    Effects::get().Render();

    GL_CHECK(glClear(GL_DEPTH_BUFFER_BIT));
    GL_CHECK(glDisable(GL_CULL_FACE));

    // Text display
    Shader *shader = ResourceManager::get().GetDefaultShader();
    Font *fnt = ResourceManager::get().GetDefaultFont();

    if (shader && fnt && shader->Activate(mat4(), mat4::orthoS(0.0f, (float)var.geti("win_width"), 0.0f, (float)var.geti("win_height"))))
    {
        std::stringstream strStream;

        strStream << Timer::get().getFPSCounter() << " FPS";
        _fpsFontString.Set(*fnt, strStream.str(), {255, 255, 0, 0});

        strStream.str("");
        strStream << (int)(var.getf("time_speed") * 100.0f) << " %";
        _speedFontString.Set(*fnt, strStream.str());

        strStream.str("");
        strStream <<
             "(" << std::setprecision(2) << Camera::get().getEye().x <<
             "," << std::setprecision(2) << Camera::get().getEye().y <<
             "," << std::setprecision(2) << Camera::get().getEye().z <<
             ") " ;
        _cameraString.Set(*fnt, strStream.str(), {0, 255, 255, 0});

        int numRows = 0;

        shader->SetMatDiffuse(vec4(1.0f, 1.0f, 0.0f, 1.0f));
        shader->SetModelView(mat4(1.0f + _fpsFontString.GetTexture().W() / 2.0f, (float)var.geti("win_height") - (++numRows) * _fpsFontString.GetTexture().H(), 0.0));
        fnt->Draw(*shader, _fpsFontString);

        shader->SetMatDiffuse(vec4(1.0f, 1.0f, 1.0f, 1.0f));
        shader->SetModelView(mat4(1 + _speedFontString.GetTexture().W() / 2, var.geti("win_height") - (++numRows) * _fpsFontString.GetTexture().H(), 0.0));
        fnt->Draw(*shader, _speedFontString);

        shader->SetMatDiffuse(vec4(0.0f, 1.0f, 1.0f, 1.0f));
        shader->SetModelView(mat4(1 + _cameraString.GetTexture().W() / 2, var.geti("win_height") - (++numRows) * _fpsFontString.GetTexture().H(), 0.0));
        fnt->Draw(*shader, _cameraString);

        if (auto ptr = SceneManager::get().getCurrentScenePointer())
        {
            std::string stats = ptr->GetStats();
            if (!stats.empty())
            {
                _sceneString.Set(*fnt, stats, {0, 255, 0, 0});

                shader->SetMatDiffuse(vec4(0.0f, 1.0f, 0.0f, 1.0f));
                shader->SetModelView(mat4(1 + _sceneString.GetTexture().W() / 2, var.geti("win_height") - (++numRows) * _fpsFontString.GetTexture().H(), 0.0));
                fnt->Draw(*shader, _sceneString);
            }
        }

        shader->Deactivate();
    }
}

void SDLMain::initGL()
{
    VarManager& var = VarManager::get();
    GL_CHECK(glClearColor(0.75f, 0.75f, 0.75f, 1.0f));

    GL_CHECK(glEnable(GL_CULL_FACE));
    GL_CHECK(glCullFace(GL_BACK));
    GL_CHECK(glFrontFace(GL_CCW));
    GL_CHECK(glEnable(GL_DEPTH_TEST));
#ifndef HAVE_GLES
    GL_CHECK(glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST));	// Really Nice Perspective Calculations
    GL_CHECK(glDisable(GL_LIGHTING));
    GL_CHECK(glShadeModel(GL_SMOOTH));
#endif // HAVE_GLES
    GL_CHECK(glDisable(GL_BLEND));
    GL_CHECK(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

    glInfo::get().Init();
    glInfo::get().PrintInfo();

//	Shader::PrintSupportedExtensions();

    Util::get().Init();

    ResourceManager::get().Init();

    Shader::Init();


    SceneManager::get().Init();

    Sky::get().Init();

    //Camera::get().setEye(vec3(12.0f, 0.0f, 0.0f));
    //Camera::get().setEye(vec3(16.0f, 4.0f, 1.0f)); // toon
    Camera::get().setEye(vec3(0.49f, -0.27f, -3.8f)); // kinect

    Effects::get().init();

    Camera::get().setType(var.getb("cam_driven") ? Camera::DRIVEN : Camera::FREE);
}

int SDLMain::handleKey(unsigned char key)
{
    if (key == 'q' )
        return -1; // quit

    Inputs::get().KeyboardFunc(key);
    SceneManager::get().Keyboard(false, key);
    VarManager& var = VarManager::get();

    switch (key)
    {
    case 'a':
        var.set("enable_anaglyph", !var.getb("enable_anaglyph"));
        break;
	//case 'e': var.set("enable_effects", !var.getb("enable_effects")); break;
    case 'b':
        var.set("enable_bloom", !var.getb("enable_bloom"));
        break;
    case 'v':
        var.set("enable_vignette", !var.getb("enable_vignette"));
        break;
    case 'n':
        var.set("enable_noise", !var.getb("enable_noise"));
        break;
    case 't':
        var.set("show_camera_splines", !var.getb("show_camera_splines"));
        break;
    case 'c':
        var.set("enable_pdc", !var.getb("enable_pdc"));
        break;

    case '+':
    {
        float speed = var.getf("time_speed");
        speed += 0.1f;
        if (speed > 10.0f) speed = 10.0f;
        var.set("time_speed", speed);
        break;
    }

    case '-':
    {
        float speed = var.getf("time_speed");
        speed -= 0.1f;
        if (speed < 0.1f) speed = 0.1f;
        var.set("time_speed", speed);
        break;
    }

    /*case 'r':
        SceneManager::get().getCurrentScenePointer()->Reset();
        break;*/
    case ' ':
    {
        switch (Camera::get().getType())
        {
        case Camera::FREE:
            Camera::get().setType(Camera::DRIVEN);
            break;
        case Camera::DRIVEN:
            Camera::get().setType(Camera::FREE);
            break;
        };
        break;
    }

    break;
    }

    if (Camera::get().getType() == Camera::FREE)
    {
        switch (key)
        {
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            SceneManager::get().setCurrent(key - '1');
            break;
        }
    }

    return 0;
}

void SDLMain::close()
{
    LOG_INFO("Closing ...\n");

    SceneManager::get().Clear();
    ResourceManager::get().Clear();
    Effects::get().Clear();
    Util::get().Clear();

    _fpsFontString.Clear();
    _speedFontString.Clear();
    _cameraString.Clear();
    _sceneString.Clear();

    if (_window)
    {
        // Destroy window
        SDL_DestroyWindow( _window );
        _window = NULL;
    }

    if (_joystick)
    {
        SDL_JoystickClose(_joystick);
        _joystick = nullptr;
    }

    // Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char* argv[])
{
    try
    {
        SDLMain main(argc, argv);
        return main.run();
    }
    catch (std::exception &e)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Caught main exception: %s\n", e.what());
    }
    return -1;
}
