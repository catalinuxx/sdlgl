#if 0
#pragma once

#include <iostream>
#include "Util.h"
#include "Mathlib.h"
#include <vector>
#include "BoundingBox.h"
#include "IResource.h"

class VertexBufferObject;

// -------------------------------
// Objet 3D
// chargement � partir de mod�les .obj
// -------------------------------

class Mesh : public IResource
{
public:
    bool Load(const std::string& name);

    void Draw();
    void Draw(GLuint group);

    inline GLuint		getGroupCount()		const
    {
        return (GLuint)m_tGroup.size();
    }
    inline BoundingBox&	getBoundingBox()
    {
        return m_BBox;
    }

    static void EnableComputeNormals(bool b)
    {
        s_bComputeNormals = b;
    }

    Mesh();
    virtual ~Mesh();
private:
    bool LoadOBJ(const std::string& filename);

    bool ComputeVBO();
    void ComputeNormals();
    void ComputeTangents();
    void ComputeBoundingBox();

private:
    struct sFace
    {
        GLuint ind[3];
    };

    struct sGroup
    {
        std::string			strName;
        long				nMaterial;
        std::vector<sFace>	tFace;
    };

private:
    std::vector<sGroup>		m_tGroup;

    BoundingBox				m_BBox;

    // VBO
    VertexBufferObject*		m_pVBO;

    static bool s_bComputeNormals;	// Etat pour le chargement : g�n�ration ou non des normales � la main

};
#endif
