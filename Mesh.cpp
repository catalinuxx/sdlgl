#include "Util.h"
#include "Mesh.h"

#include <iostream>
#include <algorithm>
using namespace std;
#include <string>
#include <sstream>
#include <fstream>
#include <assert.h>
#include "VertexBufferObject.h"
#include "ResourceManager.h"
#include "Camera.h"

bool Mesh::s_bComputeNormals = false;

Mesh::Mesh() : IResource()
{
}

bool Mesh::Load(const std::string& name, GLenum usage)
{
    bool ret = false;

    _name = name;
    _usage = usage;

    if (name.find(".obj") != std::string::npos)
        ret = LoadOBJ(name);
    else
    {
        std::cerr << "Format for " << name << " is not supported" << std::endl;
        return false;
    }

    if (!ret)
    {
        std::cerr << "[Error] Cannot load mess " << name << std::endl;
        return false;
    }

    if (s_bComputeNormals)
        ComputeNormals();
    ComputeTangents();
    ComputeBoundingBox();

    return true;
}


void Mesh::ComputeBoundingBox()
{
    m_BBox.min = vec3(100000.0f, 100000.0f, 100000.0f);
    m_BBox.max = vec3(-100000.0f, -100000.0f, -100000.0f);

    for (auto &shape : _shapes)
    {
        auto tPosition = shape.mesh.vbo->getPosition();
        for (int i = 0; i < (int)tPosition.size(); i++)
            m_BBox.Add(tPosition[i]);
    }
}


Mesh::~Mesh()
{
    Clear();
}

void Mesh::Clear()
{
    for (auto &shape : _shapes)
        if (shape.mesh.vbo != nullptr)
        {
            delete shape.mesh.vbo;
            shape.mesh.vbo = nullptr;
        }
    _shapes.clear();
    _materials.clear();
}

// Load an Wavefront 3D (.obj) object
bool Mesh::LoadOBJ(const std::string& filename)
{
    ResourceManager& res = ResourceManager::get();

    Clear();

    std::string err = tinyobj::LoadObj(_shapes, _materials, filename.c_str(), Util::Basename(filename).c_str());
    if (!err.empty())
    {
        std::cerr << err << std::endl;
        return false;
    }

    //tinyobj::PrintInfo(_shapes, _materials);

    cout << "shapes.size() = " << _shapes.size() << endl;

    // Allocate VBOs
    int group = 0;
    for (auto &shape : _shapes)
    {
        shape.mesh.vbo = new VertexBufferObject();

        auto &tPosition	= shape.mesh.vbo->getPosition();
        auto &tNormal = shape.mesh.vbo->getNormal();
        auto &tTexcoord	= shape.mesh.vbo->getTexcoord();
        std::size_t i = 0;

        // positions
        for (i = 0; i < shape.mesh.positions.size(); i += 3)
            tPosition.push_back({shape.mesh.positions[i + 0], shape.mesh.positions[i + 1], shape.mesh.positions[i + 2]});

        // normals
        for (i = 0; i < shape.mesh.normals.size(); i += 3)
            tNormal.push_back({shape.mesh.normals[i + 0], shape.mesh.normals[i + 1], shape.mesh.normals[i + 2]});

        // texcoords
        for (i = 0; i < shape.mesh.texcoords.size(); i += 2)
            tTexcoord.push_back({shape.mesh.texcoords[i + 0], shape.mesh.texcoords[i + 1]});

        GLfloat max = 0;
        for (auto index : shape.mesh.indices)
                if (index > max)
                    max = index;
        //std::cout << "max: " << max << std::endl;
        if (tPosition.size() < max) tPosition.resize(max);
        if (tNormal.size() < max) tNormal.resize(max);
        if (tTexcoord.size() < max) tTexcoord.resize(max);

        //cout << "Position.size() = " << tPosition.size() << endl;
        //cout << "Normal.size() = " << tNormal.size() << endl;
        //cout << "Texcoord.size() = " << tTexcoord.size() << endl;

        cout << "[" << group << "] "
            << "materials = " << shape.mesh.material_ids.size() << " "
            << "positions = " << tPosition.size() << " "
            << "normals = " << tNormal.size() << " "
            << "texcoords = '" << tTexcoord.size() << "' "
            << "name = " << shape.name <<
            endl;

        if (!shape.mesh.vbo->Create(_usage))
        {
            std::cerr << "VBO[" << group << "].Create() failed" << std::endl;
            assert(false);
            return false;
        }

        group++;
    }

    // Materials
    for (auto &mat : _materials)
    {
        if (mat.ambientTexName.length() > 0)
            mat.textures.push_back(mat.ambientTex = dynamic_cast<decltype(mat.ambientTex)>(res.LoadResource(ResourceManager::TEXTURE2D, Util::Basename(filename) + mat.ambientTexName)));
        if (mat.diffuseTexName.length() > 0)
            mat.textures.push_back(mat.diffuseTex = dynamic_cast<decltype(mat.diffuseTex)>(res.LoadResource(ResourceManager::TEXTURE2D, Util::Basename(filename) + mat.diffuseTexName)));
        if (mat.specularTexName.length() > 0)
            mat.textures.push_back(mat.specularTex = dynamic_cast<decltype(mat.specularTex)>(res.LoadResource(ResourceManager::TEXTURE2D, Util::Basename(filename) + mat.specularTexName)));
        if (mat.normalTexName.length() > 0)
            mat.textures.push_back(mat.normalTex = dynamic_cast<decltype(mat.normalTex)>(res.LoadResource(ResourceManager::TEXTURE2D, mat.normalTexName)));
    }

    return true;
}

void Mesh::ComputeNormals()
{
    for (auto &shape : _shapes)
    {
        std::vector<vec3>&	tPosition	= shape.mesh.vbo->getPosition();
        std::vector<vec3>&	tNormal		= shape.mesh.vbo->getNormal();

        tNormal.assign(tNormal.size(), vec3(0.0f, 0.0f, 0.0f));
        std::vector<int> tNormalCount(tNormal.size(), 0);

        for (std::size_t index = 0; index < shape.mesh.indices.size(); index += 3)
        {
            auto &ind0 = shape.mesh.indices[index + 0];
            auto &ind1 = shape.mesh.indices[index + 1];
            auto &ind2 = shape.mesh.indices[index + 2];

            vec3 v0 = tPosition[ind0];
            vec3 v1 = tPosition[ind1];
            vec3 v2 = tPosition[ind2];

            vec3 vect10 = v0 - v1;
            vec3 vect12 = v2 - v1;

            vec3 vNormal = Cross(vect12, vect10);
            vNormal.normalize();

            tNormal[ind0] += vNormal;
            tNormal[ind1] += vNormal;
            tNormal[ind2] += vNormal;

            tNormalCount[ind0] ++;
            tNormalCount[ind1] ++;
            tNormalCount[ind2] ++;
        }

        for (int i = 0; i < (int)tNormal.size(); i++)
            if (tNormalCount[i] > 0)
                tNormal[i] /= (float)tNormalCount[i];
    }
}

void Mesh::ComputeTangents()
{
    for (auto &shape : _shapes)
    {
        std::vector<vec3>&	tPosition	= shape.mesh.vbo->getPosition();
        std::vector<vec3>&	tNormal		= shape.mesh.vbo->getNormal();
        std::vector<vec2>&	tTexcoord	= shape.mesh.vbo->getTexcoord();
        std::vector<vec3>&	tTangent	= shape.mesh.vbo->getTangent();

        auto max = std::max(std::max(tPosition.size(), tNormal.size()), tTexcoord.size());
        tTangent.resize(max);

        for (std::size_t index = 0; index < shape.mesh.indices.size(); index += 3)
        {
            auto &ind0 = shape.mesh.indices[index + 0];
            auto &ind1 = shape.mesh.indices[index + 1];
            auto &ind2 = shape.mesh.indices[index + 2];

            vec3 vTangent;

            vec3 v0 = tPosition[ind0];
            vec3 v1 = tPosition[ind1];
            vec3 v2 = tPosition[ind2];

            vec3 vect10 = v0 - v1;
            vec3 vect12 = v2 - v1;

            if (ind0 < tTexcoord.size() && ind1 < tTexcoord.size() && ind2 < tTexcoord.size())
            {
                float deltaT10 = tTexcoord[ind0].t - tTexcoord[ind1].t;
                float deltaT12 = tTexcoord[ind2].t - tTexcoord[ind1].t;
                vTangent = deltaT12 * vect10 - deltaT10 * vect12;
                vTangent.normalize();
                tTangent[ind0] = tTangent[ind1] = tTangent[ind2] = vTangent;
            }
        }
    }
}

void Mesh::Draw(Shader &shader, int group, bool forceDraw, bool ignoreMat)
{
    _numberOfNonVisibleGroups = 0;
    if (group < 0) // all groups
        for (GLuint grp = 0; grp < _shapes.size(); grp++)
            DrawGroup(shader, grp, forceDraw, ignoreMat);
    else
        DrawGroup(shader, group, forceDraw, ignoreMat);

    //LOG_INFO("%s: Number of non-visible groups: %d / %d", _name.c_str(), _numberOfNonVisibleGroups, _shapes.size());
}

void Mesh::DrawGroup(Shader &shader, GLuint group, bool forceDraw, bool ignoreMat)
{
    if (group >= GetGroupCount())
    {
        assert(group < GetGroupCount());
        LOG_WARN("ERROR: %s: group(%d) < GetGroupCount(%d)", _name.c_str(), group, GetGroupCount());
        return;
    }
    auto &shape = _shapes[group];
    std::vector<Texture2D *> *textures = nullptr;

    if (!forceDraw)
    {
        BoundingBox bb = shape.mesh.vbo->GetBoundingBox();
        bb.Translate(_translation);
        if (!Camera::get().ContainsBoundingBox(bb))
        {
            //LOG_INFO("%s: Group %d '%s' not visible", _name.c_str(), group, shape.name.c_str());
            _numberOfNonVisibleGroups++;
            return;
        }
    }

    shape.mesh.vbo->Enable(shader);

    if (shape.mesh.material_ids.size() > 0 && shape.mesh.material_ids[0] >= 0 && (std::size_t)shape.mesh.material_ids[0] < _materials.size())
    {
        tinyobj::material_t &mat = _materials[shape.mesh.material_ids[0]];
        if (!ignoreMat)
        {
            shader.SetMatAmbient(vec4(mat.ambient[0], mat.ambient[1], mat.ambient[2], 0.0f));
            shader.SetMatDiffuse(vec4(mat.diffuse[0], mat.diffuse[1], mat.diffuse[2], 0.0f));
            shader.SetMatSpecular(vec4(mat.specular[0], mat.specular[1], mat.specular[2], 0.0f));
            shader.SetMatEmission(vec4(mat.emission[0], mat.emission[1], mat.emission[2], 0.0f));
            shader.SetMatShininess(mat.shininess);
        }
        textures = &mat.textures;
    }
    int slot = 0;
    if (textures)
    {
        for (auto &texture : *textures)
        {
            if (texture)
            {
                texture->Bind(slot);
                shader.SetTexActive(true, slot);
                slot++;
            }
        }
    }

    GL_CHECK(glDrawElements(GL_TRIANGLES, (GLsizei)shape.mesh.indices.size(), GL_UNSIGNED_SHORT, &shape.mesh.indices[0]));

    if (textures)
    {
        for (auto &texture : *textures)
        {
            shader.SetTexActive(false, slot);
            texture->Unbind(--slot);
        }
    }

    if (_drawBBox)
        shape.mesh.vbo->DrawBBox(shader);

    shape.mesh.vbo->Disable(shader);
}

void Mesh::EnableBBoxDraw()
{
    _drawBBox = true;
    for (auto &shape : _shapes)
        shape.mesh.vbo->CreateBBoxVBOs();
}

int Mesh::GetGroupByName(const std::string& name)
{
    for (std::size_t i = 0; i <_shapes.size(); ++i)
        if (_shapes[i].name == name)
            return i;
    return -1;
}
