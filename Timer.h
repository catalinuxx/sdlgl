#pragma once

// -------------------------------
// Gestion du temps
// -------------------------------

#include "Singleton.h"

class Timer : public Singleton<Timer>
{
public:
    void Start();
    void Idle();

    inline float getCurrentTime()	const
    {
        return fCurrentTime;   // Temps courant
    }
    inline float getElapsedTime()	const
    {
        return fElapsedTime;   // Temps entre 2 frames
    }
    inline int getFPSCounter()		const
    {
        return nFPS;   // FPS
    }

private:
    friend class Singleton<Timer>;
    Timer();

    float	fStartTime;		// date du syst�me lors de l'initialisation de la base de temps
    float	fCurrentTime;	// Temps courant remis � jour � chaque frame
    float	fElapsedTime;
    int		nFPS;
};


