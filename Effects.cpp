#include "Util.h"
#include "Effects.h"
#include "ResourceManager.h"
#include "VarManager.h"
#include "Camera.h"
#include "Scenes/SceneManager.h"
#include"Timer.h"

mat4 modelView, projection;

#define POSTFX "postfx1"
//#define POSTFX "simple"

void Effects::init()
{
    if (!VarManager::get().getb("enable_effects"))
        return;
    ResourceManager& res = ResourceManager::get();
    m_pVignette = (Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "vignette.jpeg");

    m_pBruit = (Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "bruit_gaussien.jpg");
//	m_pBruit2 = (Texture2D*)res.LoadResource(ResourceManager::TEXTURE2D, "bruit_gaussien2.jpg");

    ResourceManager::get().LoadResource(ResourceManager::SHADER, "anaglyph1");
    ResourceManager::get().LoadResource(ResourceManager::SHADER, POSTFX);
    ResourceManager::get().LoadResource(ResourceManager::SHADER, "blur1");
    ResourceManager::get().LoadResource(ResourceManager::SHADER, "bright1");
    //ResourceManager::get().LoadResource(ResourceManager::SHADER, "depthblur1");

    randomCoeffNoise = 0.0f;
    randomCoeffFlash = 0.0f;

    m_fT = 0;
    m_fPeriod = 1.0f / 24.0f;
}

void Effects::reshapeFBO(int newwidth , int newheight)
{
    if (!VarManager::get().getb("enable_effects"))
        return;
    // Recreate FBO
    fboScreen.Create(FrameBufferObject::FBO_2D_COLOR, newwidth, newheight, "EffectScreen");
    fboDepth.Create(FrameBufferObject::FBO_2D_DEPTH, newwidth, newheight, "EffectDepth");
    fboAnaglyph[0].Create(FrameBufferObject::FBO_2D_COLOR, newwidth, newheight, "EffectAnag[0]");
    fboAnaglyph[1].Create(FrameBufferObject::FBO_2D_COLOR, newwidth, newheight, "EffectAnag[1]");

    fboDepthScreenTemp.Create(FrameBufferObject::FBO_2D_COLOR, newwidth, newheight, "EffectDepthScreenTemp");
    fboDepthScreen.Create(FrameBufferObject::FBO_2D_COLOR, newwidth, newheight, "EffectDepthScreen");

    int w = newwidth / 4, h = newheight / 4;
    fboBloomFinal.Create(FrameBufferObject::FBO_2D_COLOR, w, h, "EffectBloomFinal");
    fboBloomFinalTemp.Create(FrameBufferObject::FBO_2D_COLOR, w, h, "EffectBloomFinalTemp");
}

void Effects::displaySceneWithAnaglyph(void)
{
    VarManager& var = VarManager::get();
    ResourceManager& res = ResourceManager::get();

    Camera::get().SaveCamera();
    GLfloat eye_offset = var.getf("cam_anaglyph_offset");
    GLfloat tEyePos[2] = {eye_offset / 2, -eye_offset};
    for (int i = 0; i < 2; i++)
    {
        Camera::get().MoveAnaglyph(tEyePos[i]);
        Camera::get().RenderLookAt(modelView, projection);

        fboAnaglyph[i].Begin();
        {
            GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
            SceneManager::get().Render();
        }
        fboAnaglyph[i].End();
    }

    Camera::get().RestoreCamera();

    // Display a quad on the screen
    Shader* pAnag = res.getShader("anaglyph1");
    pAnag->Activate(modelView, projection);
    {
        fboAnaglyph[0].Bind(0);
        pAnag->UniformTexture("texLeftEye", 0);
        fboAnaglyph[1].Bind(1);
        pAnag->UniformTexture("texRightEye", 1);

        Util::get().DrawQuadAtScreen(*pAnag);

        fboAnaglyph[1].Unbind(1);
        fboAnaglyph[0].Unbind(0);
    }
    pAnag->Deactivate();
}

void Effects::displaySceneWithoutAnaglyph(void)
{
    VarManager& var = VarManager::get();
    ResourceManager& res = ResourceManager::get();

    //Camera::get().RenderLookAt(modelView, projection);
    Camera::get().SelfRenderLookAt();

    if (var.getb("enable_effects"))
    {
        // RENDU DE LA SCENE NORMALE
        if (!var.getb("enable_pdc"))
        {
            fboScreen.Begin();
            {
                GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
                SceneManager::get().Render();
            }
            fboScreen.End();
        }
        else
        {
            fboDepth.Begin();
            {
                GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
                SceneManager::get().Render();
            }
            fboDepth.End();

            fboScreen.Begin();
            {
                GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
                SceneManager::get().Render();
            }
            fboScreen.End();
        }

        // RENDU DE LA TEXTURE BLOOMESQUE
        if (var.getb("enable_bloom"))
        {
            GenerateBloomTexture();
        }

        // RENDU POUR LE FLOU DE PROFONDEUR
        if (var.getb("enable_pdc"))
        {
            GenerateDepthBlurTexture();
        }

        // Final rendering with effects
        Shader* pShdFX = res.getShader(POSTFX);
        int id = 0;
        if (pShdFX->Activate(modelView, projection))
        {
            if (!var.getb("enable_pdc"))
            {
                fboScreen.Bind(id);
            }
            else
            {
                fboDepthScreen.Bind(id);
            }
            pShdFX->UniformTexture(TEX_UNIFORM, id++);
            pShdFX->Uniform("enable_bloom", var.getb("enable_bloom"));
            pShdFX->Uniform("enable_vignette", var.getb("enable_vignette"));
            pShdFX->Uniform("enable_noise", var.getb("enable_noise"));
            //pShdFX->Uniform("enable_pdc", var.getb("enable_pdc"));
            pShdFX->Uniform("enable_underwater", var.getb("enable_underwater"));

            if (var.getb("enable_underwater"))
            {
                Texture2D* texWater = res.getTexture2D("terrain_water_NM.jpg");
                texWater->Bind(id);
                pShdFX->UniformTexture("texWaterNoiseNM", id++);
                pShdFX->Uniform("screnWidth", var.geti("win_width"));
                pShdFX->Uniform("screnHeight", var.geti("win_height"));
                pShdFX->Uniform("noise_tile", 0.05f);
                pShdFX->Uniform("noise_factor", 0.02f);
                pShdFX->Uniform("time", Timer::get().getCurrentTime());
            }

            if (var.getb("enable_bloom"))
            {
                fboBloomFinal.Bind(id);
                pShdFX->UniformTexture("texBloom", id++);
                pShdFX->Uniform("bloom_factor", 4.0f);
            }

            if (var.getb("enable_noise"))
            {
                m_pBruit->Bind(id);
                pShdFX->UniformTexture("texBruit", id++);
                randomCoeffNoise = (float)(rand() % 1000) / 1000.0f;
                pShdFX->Uniform("randomCoeffNoise", randomCoeffNoise);
                pShdFX->Uniform("randomCoeffFlash", randomCoeffFlash);
            }

            if (var.getb("enable_vignette"))
            {
                this->m_pVignette->Bind(id);
                pShdFX->UniformTexture("texVignette", id++);
            }

            Util::get().DrawQuadAtScreen(*pShdFX);

            if (var.getb("enable_vignette"))
            {
                this->m_pVignette->Unbind(--id);
            }

            if (var.getb("enable_noise"))
            {
                m_pBruit->Unbind(--id);
            }

            if (var.getb("enable_bloom"))
            {
                fboBloomFinal.Unbind(--id);
            }

            if (var.getb("enable_underwater"))
            {
                Texture2D* texWater = res.getTexture2D("terrain_water_NM.jpg");
                texWater->Unbind(--id);
            }

            if (!var.getb("enable_pdc"))
            {
                fboScreen.Unbind(--id);
            }
            else
            {
                fboDepthScreen.Unbind(--id);
            }
            pShdFX->Deactivate();
        }
    }
    else
    {
        SceneManager::get().Render();
    }
}

void Effects::GenerateBloomTexture()
{
    ResourceManager& res = ResourceManager::get();

    // RENDU DES SOURCES LUMINEUSES
    Shader* pShdBright = res.getShader("bright1");
    fboBloomFinal.Begin();
    {
        GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

        pShdBright->Activate(modelView, projection);
        {
            fboScreen.Bind(0);
            pShdBright->UniformTexture(TEX_UNIFORM, 0);
            pShdBright->Uniform("threshold", 0.95f);
            Util::get().DrawQuadAtScreen(*pShdBright);
            fboScreen.Unbind(0);
        }
        pShdBright->Deactivate();
    }
    fboBloomFinal.End();


    // ON BLUR LES SOURCES LUMINEUSES HORIZONTALEMENT
    Shader* pShdBlur = res.getShader("blur1");
    fboBloomFinalTemp.Begin();
    {
        GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

        pShdBlur->Activate(modelView, projection);
        {
            fboBloomFinal.Bind(0);
            pShdBlur->UniformTexture(TEX_UNIFORM, 0);
            pShdBlur->Uniform("size", vec2((float)fboBloomFinal.getWidth(), (float)fboBloomFinal.getHeight()));
            pShdBlur->Uniform("horizontal", true);
            pShdBlur->Uniform("kernel_size", 10);
            Util::get().DrawQuadAtScreen(*pShdBlur);
            fboBloomFinal.Unbind(0);
        }
        pShdBlur->Deactivate();

    }
    fboBloomFinalTemp.End();


    // ON BLUR LES SOURCES LUMINEUSES VERTICALEMENT
    fboBloomFinal.Begin();
    {
        GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

        pShdBlur->Activate(modelView, projection);
        {
            fboBloomFinalTemp.Bind(0);
            pShdBlur->UniformTexture(TEX_UNIFORM, 0);
            pShdBlur->Uniform("size", vec2((float)fboBloomFinalTemp.getWidth(), (float)fboBloomFinalTemp.getHeight()));
            pShdBlur->Uniform("horizontal", false);
            Util::get().DrawQuadAtScreen(*pShdBlur);
            fboBloomFinalTemp.Unbind(0);
        }
        pShdBlur->Deactivate();

    }
    fboBloomFinal.End();
}



void Effects::GenerateDepthBlurTexture()
{
    VarManager& var = VarManager::get();
    ResourceManager& res = ResourceManager::get();

    //FLOU VERTICAL

    fboDepthScreenTemp.Begin();
    {
        GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

        Shader* pShdDB = res.getShader("depthblur1");
        pShdDB->Activate(modelView, projection);
        {
            pShdDB->Uniform("screenWidth", var.geti("win_width"));
            pShdDB->Uniform("screenHeight", var.geti("win_height"));
            pShdDB->Uniform("bHorizontal", false);

            fboScreen.Bind(0);
            pShdDB->UniformTexture(TEX_UNIFORM, 0);

            fboDepth.Bind(1);
            pShdDB->UniformTexture(TEX_UNIFORM, 1);

            Util::get().DrawQuadAtScreen(*pShdDB);

            fboDepth.Unbind(1);
            fboScreen.Unbind(0);
        }
        pShdDB->Deactivate();

    }
    fboDepthScreenTemp.End();

    // FLOU HORIZONTAL
    fboDepthScreen.Begin();
    {
        GL_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

        Shader* pShdDB = res.getShader("depthblur1");
        pShdDB->Activate(modelView, projection);
        {
            pShdDB->Uniform("screenWidth", var.geti("win_width"));
            pShdDB->Uniform("screenHeight", var.geti("win_height"));
            pShdDB->Uniform("bHorizontal", true);

            fboDepthScreenTemp.Bind(0);
            pShdDB->UniformTexture(TEX_UNIFORM, 0);

            fboDepth.Bind(1);
            pShdDB->UniformTexture("texDepth", 1);

            //Util::get().DrawQuadAtScreen(*pShdDB);

            fboDepth.Unbind(1);
            fboDepthScreenTemp.Unbind(0);
        }
        pShdDB->Deactivate();

    }
    fboDepthScreen.End();
}




void Effects::Render()
{
    VarManager& var = VarManager::get();

    modelView = mat4();
    projection = mat4();
    mat4 modelViewBk = Camera::get().GetModelView();
    mat4 projectionBk = Camera::get().GetProjection();

    if (var.getb("enable_anaglyph"))
    {
        displaySceneWithAnaglyph();
    }
    else
    {
        displaySceneWithoutAnaglyph();
    }

    Camera::get().SetModelView(modelViewBk);
    Camera::get().SetProjection(projectionBk);
}

void Effects::idle()
{
    VarManager& var = VarManager::get();
    if (var.getb("enable_noise"))
    {
        this->m_fT += Timer::get().getElapsedTime();
        if (m_fT > m_fPeriod )
        {
            m_fT = 0;
            randomCoeffFlash = (float)(rand() % 1000) / 1000.0f;
        }
    }
}

void Effects::Clear()
{
    fboScreen.Destroy();
    fboDepth.Destroy();
    fboDepthScreen.Destroy();
    fboDepthScreenTemp.Destroy();
    fboAnaglyph[0].Destroy();
    fboAnaglyph[1].Destroy();
    fboBloomFinal.Destroy();
    fboBloomFinalTemp.Destroy();
}
