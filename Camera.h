#pragma once

#include "Mathlib.h"
#include "Singleton.h"
#include "Frustum.h"

// -------------------------------
// Gestion de la vue
// -------------------------------

class Camera : public Singleton<Camera>
{
public:
    enum TYPE {FREE, DRIVEN};

private:
    friend class Singleton<Camera>;
    Camera();

    mat4 modelView, projection;

    vec3	vEye;		// Position de la cam�ra
    vec3	vCenter;	// Position du centre de vis�e
    vec3	vViewDir;	// Vecteur Devant
    vec3	vLeftDir;	// Vecteur Gauche
    vec3	vUp;		// Vecteur Up

    float	fAngleX;			// Angle de vue Teta
    float	fAngleY;			// Angle de vue Phi

    float	fJoyAngleX;			// Angle de vue Teta
    float	fJoyAngleY;			// Angle de vue Phi

    TYPE	eType;				// type de la camera


    vec3	tSaveVectors[5];
    float	tSaveFloats[2];
    bool	bSaved;

    Frustum _frustum;

public:
    inline TYPE  getType()				const
    {
        return eType;
    }
    inline const vec3& getEye()			const
    {
        return vEye;
    }
    inline const vec3& getCenter()		const
    {
        return vCenter;
    }
    inline const vec3& getViewDir()		const
    {
        return vViewDir;
    }
    inline float getAngleX()			const
    {
        return fAngleX;
    }
    inline float getAngleY()			const
    {
        return fAngleY;
    }

public:
    void SaveCamera();
    void RestoreCamera();

    void Refresh();

// Fixer un param�tre :
    void setType(TYPE t)
    {
        eType = t;
        Refresh();
    }
    void setEye(vec3 vPos)
    {
        vEye = vPos;
        Refresh();
    }
    void setCenter(vec3 vPos)
    {
        vCenter = vPos;
        Refresh();
    }
    void setAngleX(float Angle)
    {
        fAngleX = Angle;
        Refresh();
    }
    void setAngleY(float Angle)
    {
        fAngleY = Angle;
        Refresh();
    }
    void setJoyAngleX(float Angle)
    {
        fJoyAngleX = Angle;
        Refresh();
    }
    void setJoyAngleY(float Angle)
    {
        fJoyAngleY = Angle;
        Refresh();
    }
    void setAngle(float AngleX, float AngleY)
    {
        fAngleX = AngleX;
        fAngleY = AngleY;
        if (fAngleX < 0.0f)
            fAngleX += M_PI * 2;
        else
        {
            if (fAngleX > M_PI * 2)
                fAngleX -= M_PI * 2;
        }
        if (fAngleY < 0 || fAngleY > M_PI)
            fAngleY -= AngleY;
        Refresh();
    }

    void Translate(vec3 vector)
    {
        vEye += vector;
        Refresh();
    }
    void RotateX(float AngleX)
    {
        fAngleX += AngleX;
        if (fAngleX < 0.0f)
            fAngleX += M_PI * 2;
        else
        {
            if (fAngleX > M_PI * 2)
                fAngleX -= M_PI * 2;
        }
        Refresh();
    }
    void RotateY(float AngleY)
    {
        fAngleY += AngleY;
        if (fAngleY < 0 || fAngleY > M_PI)
            fAngleY -= AngleY;
        Refresh();
    }
    void Rotate(float AngleX, float AngleY)
    {
        fAngleX += AngleX;
        fAngleY += AngleY;
        if (fAngleX < 0.0f)
            fAngleX += M_PI * 2;
        else
            if (fAngleX > M_PI * 2) fAngleX -= M_PI * 2;
        if (fAngleY < 0 || fAngleY > M_PI)
            fAngleY -= AngleY;
        Refresh();
    }

    void PlayerMoveForward(float factor);	// Avancer/Reculer
    void PlayerMoveStrafe(float factor);	// Strafe Gauche/Droit
    void TranslateForward(float factor);	// Avancer/Reculer
    void TranslateStrafe(float factor);		// Strafe Gauche/Droit
    void MoveAnaglyph(float factor);

    void RenderLookAt(mat4 &modelView, mat4 &perspective, bool inverty = false, float planey = 0.0f);
    void SelfRenderLookAt(bool inverty = false, float planey = 0.0f);

    void RenderLookAtToCubeMap(mat4 &modelView, mat4 &projection, const vec3& eye, unsigned int nFace);
    mat4 GetModelView() { return modelView; };
    mat4 GetProjection() { return projection; };
    void SetModelView(const mat4 &mat) { modelView = mat; };
    void SetProjection(const mat4 &mat) { projection = mat; };

    void Perspective(float fovY, float aspect, float zNear, float zFar)
    {
        projection.perspective(fovY, aspect, zNear, zFar);
    }

    bool ContainsPoint(const vec3& point) const
    {
        return _frustum.ContainsPoint(point);
    }
    bool ContainsBoundingBox(const BoundingBox& bbox) const
    {
        return _frustum.ContainsBoundingBox(bbox);
    }
    int ContainsSphere(const vec3& center, float radius) const
    {
        return _frustum.ContainsSphere(center, radius);
    }

};
