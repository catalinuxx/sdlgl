#version 100

precision lowp float;

attribute vec3 Position;
attribute vec2 TexCoord;

uniform mat4 ModelView;
uniform mat4 Projection;

varying vec2 FragTexCoordOut;

void main()
{
    FragTexCoordOut = TexCoord;
    gl_Position = Projection * ModelView * vec4(Position, 1.0);
}
