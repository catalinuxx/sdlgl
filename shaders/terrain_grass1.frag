#version 100

precision lowp float;

uniform vec4 LightAmbient;
uniform vec4 LightDiffuse;
uniform vec4 MatAmbient;
uniform vec4 MatDiffuse;
uniform vec4 MatSpecular;

uniform sampler2D Tex;

//varying vec4 FragFrontColor;
varying vec4 FragDestinationColor;
varying vec2 FragTexCoordOut;
varying vec4 FragTexCoordOut1;

//uniform sampler2D texDiffuse;


// SHADOW MAPPING //
//uniform int depth_map_size;
//uniform sampler2DShadow texDepthMapFromLight0;
//uniform sampler2DShadow texDepthMapFromLight1;
#define Z_TEST_SIGMA 0.0001
////////////////////

float ShadowMapping(vec4 vVertexFromLightView);

void main (void)
{
	//gl_FrontColor = FragFrontColor;

	vec4 cBase = texture2D(Tex, FragTexCoordOut);
	if(cBase.a < 0.4) discard;

	vec4 cAmbient = LightAmbient * MatAmbient;
	vec4 cDiffuse = LightDiffuse * MatDiffuse * FragDestinationColor;

//	float shadow = ShadowMapping(FragTexCoordOut1);

	gl_FragColor = cAmbient * cBase + cDiffuse * cBase;
	gl_FragColor.a = FragDestinationColor.a;
}


float ShadowMapping(vec4 vVertexFromLightView)
{
	float fShadow = 0.0;

	float tOrtho[2];
	tOrtho[0] = 20.0;
	tOrtho[1] = 100.0;


	bool ok = false;
	int id = 0;
	vec3 vPixPosInDepthMap;

	for(int i=0; i<2; i++)
	{
		vPixPosInDepthMap = vec3(vVertexFromLightView.xy/tOrtho[i], vVertexFromLightView.z) / (vVertexFromLightView.w);
		vPixPosInDepthMap = (vPixPosInDepthMap + 1.0) * 0.5;					// de l'intervale [-1 1] � [0 1]

		if(vPixPosInDepthMap.x >= 0.0 && vPixPosInDepthMap.y >= 0.0 && vPixPosInDepthMap.x <= 1.0 && vPixPosInDepthMap.y <= 1.0)
		{
			id = i;
			i = 2;
			ok = true;
		}
	}

	if(!ok) {
		return 1.0;
	}

    fShadow = 1.0;
	return fShadow;
}
