#version 100

precision lowp float;

attribute vec3 Position;
attribute vec4 Color;
attribute vec3 Normal;
attribute vec2 TexCoord0;

uniform mat4 ModelView;
uniform mat4 Projection;

varying vec3 FragNormal, FragViewVec, FragLightVec;
varying vec4 FragDestinationColor;
varying vec2 FragTexCoordOut;

void main(void)
{
    FragTexCoordOut = TexCoord0;
	vec3 vertex = (ModelView*vec4(Position, 1.0)).xyz;
	FragViewVec = -vertex;
	//FragLightVec = gl_LightSource[0].position.xyz - vertex;
	FragLightVec = vec3(1.0,1.0,1.0);
	FragDestinationColor = Color;
	FragNormal = Normal; //gl_NormalMatrix * Normal; ???
	gl_Position = Projection * ModelView * vec4(Position, 1.0);
}
