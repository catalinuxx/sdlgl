#version 100

precision lowp float;

uniform sampler2D Tex;
uniform bool TexActive;

uniform vec4 MatDiffuse;

varying vec4 FragDestinationColor;
varying vec2 FragTexCoordOut;

void main(void)
{
    if (TexActive)
    {
        gl_FragColor = texture2D(Tex, FragTexCoordOut);
    }
    else
    {
        gl_FragColor = FragDestinationColor;
        //gl_FragColor = MatDiffuse;
    }
}
