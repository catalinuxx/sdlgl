#version 100

precision lowp float;

/*struct gl_LightSourceParameters
{
   vec4 ambient;              // Aclarri
   vec4 diffuse;              // Dcli
   vec4 specular;             // Scli
   vec4 position;             // Ppli
   vec4 halfVector;           // Derived: Hi
   vec3 spotDirection;        // Sdli
   float spotExponent;        // Srli
   float spotCutoff;          // Crli
                              // (range: [0.0,90.0], 180.0)
   float spotCosCutoff;       // Derived: cos(Crli)
                              // (range: [1.0,0.0],-1.0)
   float constantAttenuation; // K0
   float linearAttenuation;   // K1
   float quadraticAttenuation;// K2
};*/

uniform vec4 LightAmbient;
uniform vec4 LightDiffuse;
uniform vec4 LightSpecular;
uniform vec4 MatAmbient;
uniform vec4 MatDiffuse;
uniform vec4 MatSpecular;
uniform float MatShininess;

varying vec4 FragDestinationColor;
varying vec2 FragTexCoordOut;
varying vec4 FragTexCoordOut1;

varying vec4 vPixToLightTBN[1];	// Vecteur du pixel courant � la lumi�re
varying vec3 vPixToEyeTBN;		// Vecteur du pixel courant � l'oeil
varying vec3 vVertexMV;
varying vec3 vNormalMV;
varying vec3 vPixToLightMV;
varying vec3 vLightDirMV;

uniform sampler2D texDiffuse;
uniform sampler2D texNormalHeightMap;

#define MODE_PHONG		0
#define MODE_BUMP		1
#define MODE_PARALLAX	2
#define MODE_RELIEF		3

uniform int mode;
uniform float parallax_factor;
uniform float relief_factor;
uniform float tile_factor;


// SHADOW MAPPING //
uniform int depth_map_size;
uniform int enable_shadow_mapping;	// 0->no  1->shadow mapping  2->shadow mapping + projected texture
//uniform sampler2DShadow texDepthMapFromLight;
//uniform sampler2D texDiffuseProjected;
#define Z_TEST_SIGMA 0.0001
////////////////////

#define LIGHT_DIRECTIONAL		0.0
#define LIGHT_OMNIDIRECTIONAL	1.0
#define LIGHT_SPOT				2.0

float ShadowMapping(vec4 vVertexFromLightView, out vec3 vPixPosInDepthMap);
vec4  Phong(vec2 uv, vec3 vNormalTBN, vec3 vEyeTBN, vec4 vLightTBN);
vec4  NormalMapping(vec2 uv, vec3 vPixToEyeTBN, vec4 vPixToLightTBN, bool bParallax);
float ReliefMapping_RayIntersection(in vec2 A, in vec2 AB);
vec4  ReliefMapping(vec2 uv);





void main (void)
{
	//gl_FragDepth = gl_FragCoord.z;
	vec4 vPixToLightTBNcurrent = vPixToLightTBN[0];

	vec4 color = vec4(1.0, 0.0, 0.0, 1.0);


	if(mode == MODE_PHONG)
		color = Phong(FragTexCoordOut*tile_factor, vec3(0.0, 0.0, 1.0), vPixToEyeTBN, vPixToLightTBNcurrent);

	else if(mode == MODE_RELIEF)
		color = ReliefMapping(FragTexCoordOut*tile_factor);

	else if(mode == MODE_BUMP)
		color = NormalMapping(FragTexCoordOut*tile_factor, vPixToEyeTBN, vPixToLightTBNcurrent, false);

	else if(mode == MODE_PARALLAX)
		color = NormalMapping(FragTexCoordOut*tile_factor, vPixToEyeTBN, vPixToLightTBNcurrent, true);


	gl_FragColor = color;
}







float ReliefMapping_RayIntersection(in vec2 A, in vec2 AB)
{
	const int num_steps_lin = 10;
	const int num_steps_bin = 15;

	float linear_step = 1.0 / (float(num_steps_lin));
	float depth = 0.0; // current depth position

	// best match found (starts with last position 1.0)
	float best_depth = 1.0;
	float step = linear_step;

	// search from front to back for first point inside the object
	for(int i=0; i<num_steps_lin-1; i++){
		depth += step;
		float h = 1.0 - texture2D(texNormalHeightMap, A+AB*depth).a;

		if (depth >= h) { // h est dans la heightmap
			best_depth = depth; // store best depth
			i = num_steps_lin-1;
		}

	}


	// l'intersection se situe entre (depth) et (depth-step);
	// on se place donc � (depth - step/2) pour commencer
	step = linear_step/2.0;
	depth = best_depth - step;

	// recherche par dichotomie
	for(int i=0; i<num_steps_bin; i++)
	{
		float h = 1.0 - texture2D(texNormalHeightMap, A+AB*depth).a;

		step /= 2.0;
		if (depth >= h) {
			best_depth = depth;
			depth -= step;
		}
		else {
			best_depth = depth;
			depth += step;
		}
	}

	return best_depth;
}


vec4 ReliefMapping(vec2 uv)
{
	vec4 vPixToLightTBNcurrent = vPixToLightTBN[0];
	vec3 viewVecTBN = normalize(vPixToEyeTBN);

	// size and start position of search in texture space
	vec2 A = uv;
	vec2 AB = relief_factor * vec2(-viewVecTBN.x, viewVecTBN.y)/viewVecTBN.z;

	float h = ReliefMapping_RayIntersection(A, AB);

	vec2 uv_offset = h * AB;


	vec3 p = vVertexMV;
	vec3 v = normalize(p);

	// compute light direction
	p += v*h*viewVecTBN.z;


	float near = 0.1;
	float far = 800.0;
	vec2 planes;
	planes.x = -far/(far-near);
	planes.y = -far*near/(far-near);
	//gl_FragDepth =((planes.x*p.z+planes.y)/-p.z);



	return NormalMapping(uv+uv_offset, vPixToEyeTBN, vPixToLightTBNcurrent, false);
}





vec4 NormalMapping(vec2 uv, vec3 vPixToEyeTBN, vec4 vPixToLightTBN, bool bParallax)
{
	vec3 lightVecTBN = normalize(vPixToLightTBN.xyz);
	vec3 viewVecTBN = normalize(vPixToEyeTBN);

	vec2 vTexCoord = uv;
	if(bParallax) {
		// Calculate offset, scale & biais
		float height = texture2D(texNormalHeightMap, uv).a;
		vTexCoord = uv + ((height-0.5)* parallax_factor * (vec2(viewVecTBN.x, -viewVecTBN.y)/viewVecTBN.z));
	}

	// on trouve la normale pertub�e dans l'espace TBN
	vec3 normalTBN = normalize(texture2D(texNormalHeightMap, vTexCoord).xyz * 2.0 - 1.0);

//// ECLAIRAGE :
	return Phong(vTexCoord, normalTBN, vPixToEyeTBN, vPixToLightTBN);
}






vec4 Phong(vec2 uv, vec3 vNormalTBN, vec3 vEyeTBN, vec4 vLightTBN)
{

	float att = 1.0;

	vec3 L = normalize(vLightTBN.xyz);
	vec3 N = normalize(vNormalTBN.xyz);
	vec3 V = normalize(vEyeTBN.xyz);


//// ECLAIRAGE :
	vec4 base = texture2D(texDiffuse, uv);	// Couleur diffuse

	float iDiffuse = max(dot(L, N), 0.0);	// Intensit� diffuse
	float iSpecular = pow(clamp(dot(reflect(-L, N), V), 0.0, 1.0), MatShininess );


	vec4 cAmbient = LightAmbient * MatAmbient;
	vec4 cDiffuse = LightDiffuse * MatDiffuse * iDiffuse;
	vec4 cSpecular = LightSpecular * MatSpecular * iSpecular;

	vec4 color = cAmbient * base + (cDiffuse * base + cSpecular) * att;
	color.a = base.a;

	return color;
}

float ShadowMapping(vec4 vVertexFromLightView, out vec3 vPixPosInDepthMap)
{
	float fShadow = 0.0;

	vec2 tOffset[3*3];
	tOffset[0] = vec2(-1.0, -1.0); tOffset[1] = vec2(0.0, -1.0); tOffset[2] = vec2(1.0, -1.0);
	tOffset[3] = vec2(-1.0,  0.0); tOffset[4] = vec2(0.0,  0.0); tOffset[5] = vec2(1.0,  0.0);
	tOffset[6] = vec2(-1.0,  1.0); tOffset[7] = vec2(0.0,  1.0); tOffset[8] = vec2(1.0,  1.0);


	vPixPosInDepthMap = vVertexFromLightView.xyz/vVertexFromLightView.w;	// homog�nisation
	vPixPosInDepthMap = (vPixPosInDepthMap + 1.0) * 0.5;					// de l'intervale [-1 1] � [0 1]

    fShadow = 1.0;
	return fShadow;
}
