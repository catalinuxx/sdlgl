#version 100

precision lowp float;

attribute vec3 Position;
attribute vec4 Color;
attribute vec3 Normal;
attribute vec2 TexCoord;

uniform mat4 ModelView;
uniform mat4 Projection;
uniform mat3 NormalMatrix;
uniform vec3 LightPosition;
uniform mat4 TextureMatrix;

varying vec4 FragDestinationColor;
varying vec4 FragFrontColor;
varying vec2 FragTexCoordOut;
varying vec4 FragTexCoordOut1;


uniform float time;
uniform float lod_metric;

void main(void)
{
	vec4 pos = vec4(Position, 1.0);
	FragTexCoordOut = TexCoord;
	FragDestinationColor = Color;

	vec4 vertex = pos;
	vec4 vertexMV = ModelView * pos;
	vec3 normalMV = NormalMatrix * Normal;

	if(Normal.y < 0.0) {
		normalMV = -normalMV;
		vertex.x += 0.5*cos(time) * cos(vertex.x) * sin(vertex.x);
		vertex.z += 0.5*sin(time) * cos(vertex.x) * sin(vertex.x);
	}

	gl_Position = Projection * ModelView * vertex;

	// on multiplie par la matrice de la lumi�re : position du Vertex dans le rep�re de la lumi�re
	FragTexCoordOut1 = TextureMatrix * pos;
}
