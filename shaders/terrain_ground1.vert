#version 100

precision lowp float;

attribute vec3 Position;
attribute vec3 Normal;
attribute vec3 Tangent;

uniform mat4 ModelView;
uniform mat4 Projection;
uniform mat3 NormalMatrix;
uniform vec3 LightPosition;
uniform mat4 TextureMatrix;

varying vec2 FragTexCoordOut;

varying vec4 vPixToLightTBN;		// Vecteur du pixel courant � la lumi�re
varying vec3 vPixToEyeTBN;			// Vecteur du pixel courant � l'oeil
varying vec3 vPosition;
varying vec3 vPositionNormalized;

// Bounding Box du terrain
uniform vec3 bbox_min;
uniform vec3 bbox_max;


void main(void)
{
	vec4 pos = vec4(Position, 1.0);
	gl_Position = Projection * ModelView * pos;

	// Position du vertex
	vPosition = Position;

	// Position du vertex si le terrain est compris entre 0.0 et 1.0
	vPositionNormalized = (Position - bbox_min.xyz) / (bbox_max.xyz - bbox_min.xyz);

	// Coordonn�es de texture
	FragTexCoordOut = vPositionNormalized.xz;
	FragTexCoordOut.y = -FragTexCoordOut.y;

	// Calcul de l'espace TBN
	vec3 n = normalize(NormalMatrix * Normal);
	vec3 t = normalize(NormalMatrix * Tangent);
	vec3 b = cross(t, n);

	vec4 vLightPosMV = vec4(LightPosition, 1.0);			// Position (ou direction) de la lumi�re dans la MV
	vec3 vVertexMV = vec3(ModelView * pos);	// Position du vertex dans la MV

	vec3 tmpVec = -vLightPosMV.xyz;					// Lumi�re directionelle

	// Vecteur lumi�re dans l'espace TBN
	vPixToLightTBN.x = dot(tmpVec, t);
	vPixToLightTBN.y = dot(tmpVec, b);
	vPixToLightTBN.z = dot(tmpVec, n);
	vPixToLightTBN.w = vLightPosMV.w;	// ponctuelle ou directionnelle

	// Vecteur vue dans l'espace TBN
	tmpVec = -vVertexMV;
	vPixToEyeTBN.x = dot(tmpVec, t);
	vPixToEyeTBN.y = dot(tmpVec, b);
	vPixToEyeTBN.z = dot(tmpVec, n);
}





