#version 100

precision lowp float;

uniform vec4 LightAmbient;
uniform vec4 LightDiffuse;
uniform vec4 LightSpecular;
uniform vec4 MatAmbient;
uniform vec4 MatDiffuse;
uniform vec4 MatSpecular;
uniform float MatShininess;

varying vec4 FragDestinationColor;
varying vec2 FragTexCoordOut;

varying vec4 vPixToLightTBN;	// Vecteur du pixel courant � la lumi�re
varying vec3 vPixToEyeTBN;		// Vecteur du pixel courant � l'oeil
varying vec3 vPosition;
varying vec3 vPositionNormalized;

uniform sampler2D texDiffuseMap;
uniform sampler2D texNormalHeightMap;
uniform sampler2D texDiffuse0;
uniform sampler2D texDiffuse1;
uniform sampler2D texDiffuse2;
uniform sampler2D texWaterCaustics;

uniform float parallax_factor;
uniform float detail_scale;
uniform float diffuse_scale;

uniform bool water_reflection_rendering;
uniform float water_height;
uniform vec3 fog_color;
uniform float time;

// Bounding Box du terrain
uniform vec3 bbox_min;
uniform vec3 bbox_max;


vec4 NormalMapping(vec2 uv, vec3 vPixToEyeTBN, vec4 vPixToLightTBN, bool bParallax);
vec4 ReliefMapping(vec2 uv);
vec4 CausticsColor();
float CausticsAlpha();
bool isUnderWater();

void main (void)
{
	// Clip plane dans le cas du rendu de la r�flexion
	if (water_reflection_rendering)
		if(isUnderWater())
			discard;

	vec4 vPixToLightTBNcurrent = vPixToLightTBN;

	gl_FragColor = NormalMapping(FragTexCoordOut, vPixToEyeTBN, vPixToLightTBNcurrent, false);
	//gl_FragColor = texture2D(texWaterCaustics, FragTexCoordOut);

	/*if(isUnderWater())
	{
		float alpha = CausticsAlpha();
		gl_FragColor = (1.0-alpha) * gl_FragColor + alpha * CausticsColor();
	}*/
}

bool isUnderWater()
{
	return (vPosition.y < water_height);
}

float CausticsAlpha()
{
	return (water_height - vPosition.y) / (2.0*(water_height - bbox_min.y));
}

vec4 CausticsColor()
{
	vec2 uv0 = FragTexCoordOut*100.0;
	uv0.s -= time*0.1;
	uv0.t += time*0.1;
	vec4 color0 = texture2D(texWaterCaustics, uv0);

	vec2 uv1 = FragTexCoordOut*100.0;
	uv1.s += time*0.1;
	uv1.t += time*0.1;
	vec4 color1 = texture2D(texWaterCaustics, uv1);

	return (color0 + color1) / 2.0;
}



vec4 NormalMapping(vec2 uv, vec3 vPixToEyeTBN, vec4 vPixToLightTBN, bool bParallax)
{
	vec3 lightVecTBN = normalize(vPixToLightTBN.xyz);
	vec3 viewVecTBN = normalize(vPixToEyeTBN);

	vec2 uv_detail = uv * detail_scale;
	vec2 uv_diffuse = uv * diffuse_scale;

	// on trouve la normale pertub�e dans l'espace TBN
	vec3 normalTBN = texture2D(texNormalHeightMap, uv_detail).rgb * 2.0 - 1.0;
	normalTBN = normalize(normalTBN);
//	vec3 normalTBN = vec3(0.0, 0.0, 1.0);

//// ECLAIRAGE :
	// Couleur diffuse
	vec4 tBase[3];
	tBase[0] = texture2D(texDiffuse0, uv_diffuse);
	tBase[1] = texture2D(texDiffuse1, uv_diffuse);
	tBase[2] = texture2D(texDiffuse2, uv_diffuse);
	vec4 DiffuseMap = texture2D(texDiffuseMap, uv);

	vec4 cBase;
	// Calcul du la couleur :
	if(vPosition.y < water_height)
		cBase = tBase[0];
	else {
		cBase = mix(mix(tBase[1], tBase[0], DiffuseMap.r), tBase[2], DiffuseMap.g);
	}


	float iDiffuse = max(dot(lightVecTBN.xyz, normalTBN), 0.0);	// Intensit� diffuse
	float iSpecular = 0.0;

	if(isUnderWater())
		iSpecular = pow(clamp(dot(reflect(-lightVecTBN.xyz, normalTBN), viewVecTBN), 0.0, 1.0), MatShininess)/2.0;


	/////////////////////////
	// SHADOW MAPS
	float distance_max = 200.0;
	float shadow = 1.0;
	float distance = length(vPixToEyeTBN);
	if(distance < distance_max) {
		shadow = 1.0 - (1.0-shadow) * (distance_max-distance) / distance_max;
	}
	/////////////////////////

	vec4 cAmbient = LightAmbient * MatAmbient;
	vec4 cDiffuse = LightDiffuse * MatDiffuse * iDiffuse * shadow;
	vec4 cSpecular = LightSpecular * MatSpecular * iSpecular * shadow;

	return cAmbient * cBase + cDiffuse * cBase + cSpecular;
}
