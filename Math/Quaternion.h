#pragma once

#include "MathLib.h"
#include <iostream>

/**
    Quaternion class for representing rotations.
*/

template <class Scalar>
class Quaternion
{
private:
    vec4 _vec;

public:

#define W _vec.w
#define X _vec.x
#define Y _vec.y
#define Z _vec.z

    Scalar GetX() {return X;};
    Scalar GetY() {return Y;};
    Scalar GetZ() {return Z;};
    Scalar GetW() {return W;};

    void SetW(Scalar val) {X = val;}


    typedef vec4    Base;
    typedef vec3    Vec3;
    typedef vec4    Vec4;
    typedef mat4   Matrix;


    /// construct from 4 Scalars (default)
    Quaternion(Scalar _w=1.0, Scalar _x=0.0, Scalar _y=0.0, Scalar _z=0.0)
        : _vec(_w, _x, _y, _z) {}

    /// construct from 3D point (pure imaginary quaternion)
    Quaternion(const Vec3& _p)
        : _vec(0, _p[0], _p[1], _p[2]) {}

    /// construct from 4D vector
    Quaternion(const Vec4& _p)
        : _vec(_p[0], _p[1], _p[2], _p[3]) {}

    /// construct from rotation axis and angle (in radians)
    Quaternion(Vec3 _axis, Scalar _angle)
    {
        _axis.normalize();
        Scalar theta = 0.5 * _angle;
        Scalar sin_theta = sin(theta);
        W = cos(theta);
        X = sin_theta * _axis[0];
        Y = sin_theta * _axis[1];
        Z = sin_theta * _axis[2];
    }

    /// construct from rotation matrix (only valid for rotation matrices!)
    template <class MatrixT>
    Quaternion(const MatrixT& _rot)
    {
        init_from_matrix( _rot);
    }


    /// identity rotation
    void identity()
    {
        W=1.0;
        X=Y=Z=0.0;
    }


    /// conjugate quaternion
    Quaternion conjugate() const
    {
        return Quaternion(W, -X, -Y, -Z);
    }


    /// invert quaternion
    Quaternion invert() const
    {
        return conjugate() / (X*X + Y*Y + Z*Z + W*W);
    }


    /// quaternion * quaternion
    Quaternion operator*(const Quaternion& _q) const
    {
        return Quaternion(W*_q.W - X*_q.X - Y*_q.Y - Z*_q.Z,
                          W*_q.X + X*_q.W + Y*_q.Z - Z*_q.Y,
                          W*_q.Y - X*_q.Z + Y*_q.W + Z*_q.X,
                          W*_q.Z + X*_q.Y - Y*_q.X + Z*_q.W);
    }


    /// quaternion *= quaternion
    Quaternion& operator*=(const Quaternion& _q)
    {
        return *this = *this * _q;
    }


    /// rotate vector
    template <class Vec3T>
    Vec3T rotate(const Vec3T& _v) const
    {
        Quaternion q = *this * Quaternion(0,_v[0],_v[1],_v[2]) * conjugate();
        return Vec3T(q._vec[1], q._vec[2], q._vec[3]);
    }


    /// get rotation axis and angle (only valid for unit quaternions!)
    void axis_angle(Vec3& _axis, Scalar& _angle) const
    {
        if (fabs(W) > 0.999999)
        {
            _axis  = Vec3(1,0,0);
            _angle = 0;
        }
        else
        {
            _angle = 2.0 * acos(W);
            _axis  = Vec3(X, Y, Z).normalize();
        }
    }



    /// cast to rotation matrix
    Matrix rotation_matrix() const
    {
        Scalar
        ww(W*W), xx(X*X), yy(Y*Y), zz(Z*Z), wx(W*X),
        wy(W*Y), wz(W*Z), xy(X*Y), xz(X*Z), yz(Y*Z);

        Matrix m;

        m[0] = ww + xx - yy - zz;
        m[4] = 2.0*(xy + wz);
        m[8] = 2.0*(xz - wy);

        m[1] = 2.0*(xy - wz);
        m[5] = 2.0*(xy + wz);
        m[9] = 2.0*(yz + wx);

        m[2] = 2.0*(xz + wy);
        m[6] = 2.0*(yz - wx);
        m[10] = ww - xx - yy + zz;

        m[3] = m[7] = m[11] = m[12] = m[13] = m[14] = 0.0;

        m[15] = 1.0;

        return m;
    }



    /*
    /// get matrix for mult from right (Qr = q*r)
    Matrix right_mult_matrix() const
    {
      Matrix m;
      m(0,0) =  W; m(0,1) = -X; m(0,2) = -Y; m(0,3) = -Z;
      m(1,0) =  X; m(1,1) =  W; m(1,2) = -Z; m(1,3) =  Y;
      m(2,0) =  Y; m(2,1) =  Z; m(2,2) =  W; m(2,3) = -X;
      m(3,0) =  Z; m(3,1) = -Y; m(3,2) =  X; m(3,3) =  W;
      return m;
    }


    /// get matrix for mult from left (lQ = l*q)
    Matrix left_mult_matrix() const
    {
      Matrix m;
      m(0,0) =  W; m(0,1) = -X; m(0,2) = -Y; m(0,3) = -Z;
      m(1,0) =  X; m(1,1) =  W; m(1,2) =  Z; m(1,3) = -Y;
      m(2,0) =  Y; m(2,1) = -Z; m(2,2) =  W; m(2,3) =  X;
      m(3,0) =  Z; m(3,1) =  Y; m(3,2) = -X; m(3,3) =  W;
      return m;
    }
    */
    /// get matrix for mult from right (p*q = Qp)
    Matrix right_mult_matrix() const
    {
        Matrix m;
        m[0] = W;
        m[1] = -X;
        m[2] = -Y;
        m[3] = -Z;
        m[1*4] =  X;
        m[1*4+1] =  W;
        m[1*4+2] =  Z;
        m[1*4+3] = -Y;
        m[2*4+0] =  Y;
        m[2*4+1] = -Z;
        m[2*4+2] =  W;
        m[2*4+3] =  X;
        m[3*4+0] =  Z;
        m[3*4+1] =  Y;
        m[3*4+2] = -X;
        m[3*4+3] =  W;
        return m;
    }


    /// get matrix for mult from left (q*p = Qp)
    Matrix left_mult_matrix() const
    {
        Matrix m;
        m[0*4+0] =  W;
        m[0*4+1] = -X;
        m[0*4+2] = -Y;
        m[0*4+3] = -Z;
        m[1*4+0] =  X;
        m[1*4+1] =  W;
        m[1*4+2] = -Z;
        m[1*4+3] =  Y;
        m[2*4+0] =  Y;
        m[2*4+1] =  Z;
        m[2*4+2] =  W;
        m[2*4+3] = -X;
        m[3*4+0] =  Z;
        m[3*4+1] = -Y;
        m[3*4+2] =  X;
        m[3*4+3] =  W;
        return m;
    }


    /// get quaternion from rotation matrix
    template<class MatrixT>
    void init_from_matrix( const MatrixT& _rot)
    {
        Scalar trace = _rot(0,0) + _rot(1,1) + _rot(2,2);
        if( trace > 0.0 )
        {
            Scalar s = 0.5 / sqrt(trace + 1.0);
            W = 0.25 / s;
            X = ( _rot(2,1) - _rot(1,2) ) * s;
            Y = ( _rot(0,2) - _rot(2,0) ) * s;
            Z = ( _rot(1,0) - _rot(0,1) ) * s;
        }
        else
        {
            if ( _rot(0,0) > _rot(1,1) && _rot(0,0) > _rot(2,2) )
            {
                Scalar s = 2.0 * sqrt( 1.0 + _rot(0,0) - _rot(1,1) - _rot(2,2));
                W = (_rot(2,1) - _rot(1,2) ) / s;
                X = 0.25 * s;
                Y = (_rot(0,1) + _rot(1,0) ) / s;
                Z = (_rot(0,2) + _rot(2,0) ) / s;
            }
            else if (_rot(1,1) > _rot(2,2))
            {
                Scalar s = 2.0 * sqrt( 1.0 + _rot(1,1) - _rot(0,0) - _rot(2,2));
                W = (_rot(0,2) - _rot(2,0) ) / s;
                X = (_rot(0,1) + _rot(1,0) ) / s;
                Y = 0.25 * s;
                Z = (_rot(1,2) + _rot(2,1) ) / s;
            }
            else
            {
                Scalar s = 2.0 * sqrt( 1.0 + _rot(2,2) - _rot(0,0) - _rot(1,1) );
                W = (_rot(1,0) - _rot(0,1) ) / s;
                X = (_rot(0,2) + _rot(2,0) ) / s;
                Y = (_rot(1,2) + _rot(2,1) ) / s;
                Z = 0.25 * s;
            }
        }
    }


    /// quaternion exponential (for unit quaternions)
    Quaternion exponential() const
    {
        Vec3   n(X,Y,Z);
        Scalar theta(n.normalize());
        Scalar sin_theta = sin(theta);
        Scalar cos_theta = cos(theta);

        if( theta > 1e-6 )
            n *= sin_theta/theta;
        else
            n = Vec3(0,0,0);

        return Quaternion( cos_theta, n[0], n[1], n[2]);
    }


    /// quaternion logarithm (for unit quaternions)
    Quaternion logarithm() const
    {
        // clamp to valid input
        double w = W;
        if( w > 1.0) w = 1.0;
        else if( w < -1.0) w = -1.0;

        Scalar theta_half = acos(w);

        Vec3   n(X,Y,Z);
        Scalar n_norm( n.normalize());

        if( n_norm > 1e-6 )
            n *= theta_half/n_norm;
        else
            n = Vec3(0,0,0);

        return Quaternion( 0, n[0], n[1], n[2]);
    }

    void print_info()
    {
        // get axis, angle and matrix
        Vec3 axis;
        Scalar angle;
        this->axis_angle( axis, angle);
        Matrix m;
        m = this->rotation_matrix();

        std::cerr << "quaternion : " << (*this)      << std::endl;
        std::cerr << "length     : " << this->norm() << std::endl;
        std::cerr << "axis, angle: " << axis << ", " << angle*180.0/M_PI << "\n";
        std::cerr << "rot matrix :\n";
        std::cerr << m << std::endl;
    }


#undef W
#undef X
#undef Y
#undef Z
};


typedef Quaternion<float>  Quaternionf;
typedef Quaternion<double> Quaterniond;

