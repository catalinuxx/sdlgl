#define ACG_DualQuaternion_C

#include "DualQuaternion.h"
#include <iostream>

//-----------------------------------------------------------------------------

/// Default constructor ( constructs an identity dual quaternion )
template <typename Scalar>
DualQuaternion<Scalar>::DualQuaternion()
{
    *this = DualQuaternion::identity();
}

//-----------------------------------------------------------------------------

/// Copy constructor
template <typename Scalar>
DualQuaternion<Scalar>::DualQuaternion(const DualQuaternion& _other)
{
    real_ = _other.real_;
    dual_ = _other.dual_;
}

//-----------------------------------------------------------------------------

/// Construct from given real,dual parts
template <typename Scalar>
DualQuaternion<Scalar>::DualQuaternion(const Quaternion<Scalar>& _real, const Quaternion<Scalar>& _dual)
{
    real_ = _real;
    dual_ = _dual;
}

//-----------------------------------------------------------------------------

/// Construct from 8 scalars
template <typename Scalar>
DualQuaternion<Scalar>::DualQuaternion(Scalar _Rw, Scalar _Rx, Scalar _Ry, Scalar _Rz,
                                       Scalar _Dw, Scalar _Dx, Scalar _Dy, Scalar _Dz)
{
    real_ = Quaternion<Scalar>(_Rw, _Rx, _Ry, _Rz);
    dual_ = Quaternion<Scalar>(_Dw, _Dx, _Dy, _Dz);
}

//-----------------------------------------------------------------------------

/// Construct from a rotation given as quaternion
template <typename Scalar>
DualQuaternion<Scalar>::DualQuaternion(Quaternion<Scalar> _rotation)
{
    real_ = _rotation;
    dual_ = Quaternion<Scalar>(0.0,0.0,0.0,0.0);
}

//-----------------------------------------------------------------------------

/// Construct from a translatation given as a vector
template <typename Scalar>
DualQuaternion<Scalar>::DualQuaternion(const Vec3& _translation)
{
    real_.identity();
    dual_ = Quaternion<Scalar>( 0.0, 0.5 * _translation[0], 0.5 * _translation[1], 0.5 * _translation[2] );
}

//-----------------------------------------------------------------------------

/// Construct from a translation+rotation
template <typename Scalar>
DualQuaternion<Scalar>::DualQuaternion(const Vec3& _translation, const Quaternion<Scalar>& _rotation)
{

    real_ = _rotation;
    dual_ = Quaternion<Scalar>( 0.0, 0.5 * _translation[0], 0.5 * _translation[1], 0.5 * _translation[2] );

    dual_ *= real_;
}

//-----------------------------------------------------------------------------

/// Construct from a rigid transformation given as matrix
template <typename Scalar>
DualQuaternion<Scalar>::DualQuaternion(const Matrix& _transformation)
{
    real_ = Quaternion<Scalar>(_transformation); //the quaternion constructor ignores the translation
    dual_ = Quaternion<Scalar>(0.0, 0.5 * _transformation.mat[3], 0.5 * _transformation.mat[1*4+3], 0.5 * _transformation.mat[2*4+3]);

    dual_ *= real_;
}

//-----------------------------------------------------------------------------

/// identity dual quaternion [ R(1, 0, 0, 0), D(0,0,0,0) ]
template <typename Scalar>
DualQuaternion<Scalar> DualQuaternion<Scalar>::identity()
{

    Quaternion<Scalar> real;
    real.identity();

    Quaternion<Scalar> dual = Quaternion<Scalar>(0.0, 0.0, 0.0, 0.0);

    return DualQuaternion( real, dual );
}

//-----------------------------------------------------------------------------

/// zero dual quaternion [ R(0, 0, 0, 0), D(0,0,0,0) ]
template <typename Scalar>
DualQuaternion<Scalar> DualQuaternion<Scalar>::zero()
{

    Quaternion<Scalar> real = Quaternion<Scalar>(0.0, 0.0, 0.0, 0.0);
    Quaternion<Scalar> dual = Quaternion<Scalar>(0.0, 0.0, 0.0, 0.0);

    return DualQuaternion( real, dual );
}

//-----------------------------------------------------------------------------

/// conjugate dual quaternion
template <typename Scalar>
DualQuaternion<Scalar> DualQuaternion<Scalar>::conjugate() const
{
    return DualQuaternion( real_.conjugate(), dual_.conjugate() );
}

//-----------------------------------------------------------------------------

/// invert dual quaternion
template <typename Scalar>
DualQuaternion<Scalar> DualQuaternion<Scalar>::invert() const
{

    double sqrLen0 = real_.sqrnorm();
    double sqrLenE = 2.0 * (real_ | dual_);

    if ( sqrLen0 > 0.0 )
    {

        double invSqrLen0 = 1.0/sqrLen0;
        double invSqrLenE = -sqrLenE/(sqrLen0*sqrLen0);

        DualQuaternion conj = conjugate();

        conj.real_ = invSqrLen0 * conj.real_;
        conj.dual_ = invSqrLen0 * conj.dual_ + invSqrLenE * conj.real_;

        return conj;

    }
    else
        return DualQuaternion::zero();
}

//-----------------------------------------------------------------------------

/// normalize dual quaternion
template <typename Scalar>
void DualQuaternion<Scalar>::normalize()
{

    const double magn = 1.0/real_.norm();
    const double magnSqr = 1.0/real_.sqrnorm();

    // normalize rotation
    real_ *= magn;
    dual_ *= magn;

    // normalize the rest
    dual_ -= ((real_| dual_)* magnSqr) * real_;

}

//-----------------------------------------------------------------------------

/// dual quaternion comparison
template <typename Scalar>
bool DualQuaternion<Scalar>::operator==(const DualQuaternion& _other) const
{
    return (_other.real_ == real_) && (_other.dual_ == dual_);
}

//-----------------------------------------------------------------------------

/// dual quaternion comparison
template <typename Scalar>
bool DualQuaternion<Scalar>::operator!=(const DualQuaternion& _other) const
{
    return (_other.real_ != real_) || (_other.dual_ != dual_);
}

//-----------------------------------------------------------------------------

/// addition
template <typename Scalar>
DualQuaternion<Scalar> DualQuaternion<Scalar>::operator+(const DualQuaternion& _other) const
{
    return DualQuaternion( real_ + _other.real_, dual_ + _other.dual_ );
}

//-----------------------------------------------------------------------------

/// addition
template <typename Scalar>
DualQuaternion<Scalar>& DualQuaternion<Scalar>::operator+=(const DualQuaternion& _other)
{
    real_ = real_ + _other.real_;
    dual_ = dual_ + _other.dual_;

    return (*this);
}

//-----------------------------------------------------------------------------

/// substraction
template <typename Scalar>
DualQuaternion<Scalar> DualQuaternion<Scalar>::operator-(const DualQuaternion& _other) const
{
    return DualQuaternion( real_ - _other.real_, dual_ - _other.dual_ );
}

//-----------------------------------------------------------------------------

/// substraction
template <typename Scalar>
DualQuaternion<Scalar>& DualQuaternion<Scalar>::operator-=(const DualQuaternion& _other)
{
    real_ -= _other.real_;
    dual_ -= _other.dual_;

    return (*this);
}

//-----------------------------------------------------------------------------

/// dualQuaternion * dualQuaternion
template <typename Scalar>
DualQuaternion<Scalar> DualQuaternion<Scalar>::operator*(const DualQuaternion& _q) const
{
    return DualQuaternion( real_ * _q.real_, real_ * _q.dual_ + dual_ * _q.real_ );
}

//-----------------------------------------------------------------------------

/// dualQuaternion *= dualQuaternion
template <typename Scalar>
DualQuaternion<Scalar>& DualQuaternion<Scalar>::operator*=(const DualQuaternion& _q)
{
    dual_  = real_ * _q.dual_ + dual_ * _q.real_;
    real_ *= _q.real_;

    return (*this);
}

//-----------------------------------------------------------------------------

/// dualQuaternion * scalar
template <typename Scalar>
DualQuaternion<Scalar> DualQuaternion<Scalar>::operator*(const Scalar& _scalar) const
{
    DualQuaternion q;

    q.real_ = real_ * _scalar;
    q.dual_ = dual_ * _scalar;

    return q;
}

//-----------------------------------------------------------------------------

template <typename Scalar>
Scalar& DualQuaternion<Scalar>::operator [](const unsigned int& b)
{
    if ( b < 4 )
    {
        return real_[b];
    }
    else if ( b < 8 )
    {
        return dual_[b - 4];
    }
    else
    {
        // Invalid operation, write error and return anything.
        std::cerr << "Error in Dualquaternion operator[], index b out of range [0...7]" << std::endl;
        return real_[0];
    }
}


//-----------------------------------------------------------------------------

/// linear interpolation of dual quaternions. Result is normalized afterwards.
template <typename Scalar> template<typename VectorType>
DualQuaternion<Scalar> DualQuaternion<Scalar>::interpolate(VectorType& _weights, const std::vector<DualQuaternion>& _dualQuaternions)
{
    if ( (_weights.size() != _dualQuaternions.size()) || (_weights.size() == 0) )
    {
        std::cerr << "Cannot interpolate dual quaternions ( weights: " << _weights.size() << ", DQs: " << _dualQuaternions.size() << std::endl;
        return DualQuaternion::zero();
    }

    // Find max weight for pivoting to that quaternion,
    // so shortest arc is taken (see: 'coping antipodality' in the paper )
    unsigned int pivotID = 0;

    for (unsigned int i=1; i<_dualQuaternions.size(); i++)
        if (_weights[pivotID] < _weights[i])
            pivotID = i;

    DualQuaternion pivotDQ = _dualQuaternions[ pivotID ];
    Quaternion<Scalar> pivotQ = pivotDQ.real_;

    DualQuaternion res = DualQuaternion::zero();

    for (unsigned int i=0; i<_dualQuaternions.size(); i++)
    {

        DualQuaternion currentDQ = _dualQuaternions[i];
        Quaternion<Scalar> currentQ = currentDQ.real_;

        // Make sure dot product is >= 0
        if ( ( currentQ | pivotQ ) < 0.0 )
            _weights[i] = -_weights[i];

        res += _dualQuaternions[i] * _weights[i];
    }

    res.normalize();

    return res;
}

//-----------------------------------------------------------------------------

/// transform a given point with this dual quaternion
template <typename Scalar>
vec3 DualQuaternion<Scalar>::transform_point(const Vec3& _point) const
{
    ///TODO check if this is a unit dual quaternion

    // for details about the calculation see the paper (algorithm 1)
    Vec3 p(_point);

    double r  = real_[0];
    Vec3   rv = Vec3(real_[1], real_[2], real_[3]);

    double d  = dual_[0];
    Vec3   dv = Vec3(dual_[1], dual_[2], dual_[3]);

    Vec3 tempVec;
    tempVec.cross(rv, p);
    tempVec += r * p;

    Vec3 tempVec1;
    tempVec1.cross(rv, tempVec);
    p+=2.0 * tempVec1;

    Vec3 t;
    t.cross(dv, rv);
    t+= d * rv;
    t+= -r*dv;
    p+=-2.0*t;

    return p;
}

//-----------------------------------------------------------------------------

/// transform a given point with this dual quaternion
template <typename Scalar>
vec3 DualQuaternion<Scalar>::transform_vector(const Vec3& _point) const
{
    ///TODO check if this is a unit dual quaternion

    // for details about the calculation see the paper (algorithm 1)

    Vec3 p(_point);

    double r  = real_[0];
    Vec3   rv = Vec3(real_[1], real_[2], real_[3]);

    Vec3   dv = Vec3(dual_[1], dual_[2], dual_[3]);

    Vec3 tempVec;
    tempVec.cross(rv, p);
    tempVec += r * p;

    Vec3 tempVec1;
    tempVec1.cross(rv, tempVec);
    p+=2.0*tempVec1;

    return p;
}

//-----------------------------------------------------------------------------

/// print some info about the DQ
template <typename Scalar>
void DualQuaternion<Scalar>::printInfo()
{
    std::cerr << "Real Part:" << std::endl;
    real_.print_info();
    std::cerr << "Dual Part:" << std::endl;
    dual_.print_info();
}
