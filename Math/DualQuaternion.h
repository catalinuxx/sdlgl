#pragma once

#include "Quaternion.h"
#include <vector>

/**
    \brief DualQuaternion class for representing rigid motions in 3d

    This is an implementation of:

    techreport{kavan-06-dual,
    author = "Ladislav Kavan and Steven Collins and Carol O'Sullivan and Jiri Zara",
    series = "Technical report TCD-CS-2006-46, Trinity College Dublin",
    title = "{D}ual {Q}uaternions for {R}igid {T}ransformation {B}lending",
    url = "http://www.cgg.cvut.cz/~kavanl1/",
    year = "2006"
    }
*/

template <class Scalar>
class DualQuaternion
{
public:

    typedef vec3        Vec3;
    typedef vec4        Vec4;
    typedef mat4       Matrix;

    /// real and dual quaternion parts
    Quaternion<Scalar> real_;
    Quaternion<Scalar> dual_;


    // Constructors
    //

    /// Default constructor ( constructs an identity dual quaternion )
    DualQuaternion();

    /// Copy constructor
    DualQuaternion(const DualQuaternion& _other);

    /// Construct from given real,dual parts
    DualQuaternion(const Quaternion<Scalar>& _real, const Quaternion<Scalar>& _dual);

    /// Construct from 8 scalars
    DualQuaternion(Scalar _Rw, Scalar _Rx, Scalar _Ry, Scalar _Rz,
                   Scalar _Dw, Scalar _Dx, Scalar _Dy, Scalar _Dz);

    /// Construct from a rotation given as quaternion
    DualQuaternion(Quaternion<Scalar> _rotation);

    /// Construct from a translatation given as a vector
    DualQuaternion(const Vec3& _translation);

    /// Construct from a translation+rotation
    DualQuaternion(const Vec3& _translation, const Quaternion<Scalar>& _rotation);

    /// Construct from a rigid transformation given as matrix
    DualQuaternion(const Matrix& _transformation);

    // default quaternions

    /// identity dual quaternion [ R(1, 0, 0, 0), D(0,0,0,0) ]
    static DualQuaternion identity();

    /// zero dual quaternion [ R(0, 0, 0, 0), D(0,0,0,0) ]
    static DualQuaternion zero();

    // Operators
    //

    /// conjugate dual quaternion
    DualQuaternion conjugate() const;

    /// invert dual quaternion
    DualQuaternion invert() const;

    /// normalize dual quaternion
    void normalize();

    /// dual quaternion comparison
    bool operator==(const DualQuaternion& _other) const;
    bool operator!=(const DualQuaternion& _other) const;

    /// addition
    DualQuaternion operator+(const DualQuaternion& _other) const;
    DualQuaternion& operator+=(const DualQuaternion& _other);

    /// substraction
    DualQuaternion operator-(const DualQuaternion& _other) const;
    DualQuaternion& operator-=(const DualQuaternion& _other);

    /// dualQuaternion * dualQuaternion
    DualQuaternion operator*(const DualQuaternion& _q) const;

    /// dualQuaternion * scalar
    DualQuaternion operator*(const Scalar& _scalar) const;

    /// dualQuaternion *= dualQuaternion
    DualQuaternion& operator*=(const DualQuaternion& _q);

    /// Access as one big vector
    Scalar& operator [](const unsigned int& b);

    /// linear interpolation of dual quaternions. Result is normalized afterwards
    template <typename VectorType>
    static DualQuaternion interpolate(VectorType& _weights, const std::vector<DualQuaternion>& _dualQuaternions);

    /// Transform a point with the dual quaternion
    Vec3 transform_point(const Vec3& _point) const;

    /// Transform a vector with the dual quaternion
    Vec3 transform_vector(const Vec3& _point) const;

    /// print some info about the DQ
    void printInfo();

};


typedef DualQuaternion<float>  DualQuaternionf;
typedef DualQuaternion<double> DualQuaterniond;
