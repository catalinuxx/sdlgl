#include "Mesh.h"
#include "SplineGL.h"
#include "ResourceManager.h"
#include "Interpol3D.h"
#include <assert.h>
#include "Util.h"
#include "Camera.h"

float _step = 0.01f;

SplineGL::SplineGL()
{
    m_pSpline = new Interpol3D();
    m_t = 0.0f;
    m_fSpeed = 0.01f;
    m_bFinished = false;

    ResourceManager& res = ResourceManager::get();
    _sphere = (Mesh*)res.LoadResource(ResourceManager::MESH, "sphere.obj");

    {
        auto &tPosition	= _vbo.getPosition();
        for (float t = 0.0f; t < 1.0f; t += _step)
        {
            tPosition.push_back(vec3());
        }
        _vbo.Create(GL_DYNAMIC_DRAW);
    }
}

bool SplineGL::isValid() const
{
    return m_pSpline->isValid();
}

void SplineGL::BuildSplines(bool closed, float speed)
{
    if (closed)
        m_pSpline->Close();
    m_pSpline->BuildSplines();
    m_t = 0.0f;
    m_bFinished = false;
    m_fSpeed = speed;
}


vec3 SplineGL::getPoint()
{
    return m_pSpline->getPoint(m_t);
}

const std::vector<vec3>&	SplineGL::getControlPoints()	const
{
    return m_pSpline->getControlPoints();
}

SplineGL::~SplineGL()
{
    if (m_pSpline)
    {
        delete m_pSpline;
        m_pSpline = NULL;
    }
}

bool SplineGL::AddPoint(const vec3& pt)
{
    return m_pSpline->AddPoint(pt);
}

bool SplineGL::DeleteLastPoint()
{
    return m_pSpline->DeleteLastPoint();
}

bool SplineGL::Clear()
{
    return m_pSpline->Clear();
}


void SplineGL::Idle(float fElapsedTime)
{
    assert(m_pSpline);

    m_bFinished = false;
    m_t += m_fSpeed * fElapsedTime;

    if (m_t > 1.0f)
    {
        m_t = 0.0f;
        m_bFinished = true;
    }
}

void SplineGL::DrawGL(Shader &shader)
{
    assert(m_pSpline);

    if (m_pSpline->isValid())
    {
        shader.SetModelView(Camera::get().GetModelView());
        std::size_t i = 0;
        for (float t = 0.0f; t < 1.0f; t += _step, i++)
            _vbo.getPosition()[i] = m_pSpline->getPoint(t);
        assert(_vbo.getPosition().size() == i);
        _vbo.Enable(shader);
        _vbo.BufferPosition();
        GL_CHECK(glDrawArrays(GL_LINE_STRIP, 0, _vbo.getPosition().size()));
        _vbo.Disable(shader);

        const std::vector<vec3>& tPoints = m_pSpline->getControlPoints();
        for (unsigned int i = 0; i < tPoints.size(); i++)
        {
            if (_sphere)
            {
                mat4 sca;
                sca.scale(0.1, 0.1, 0.1);
                mat4 trans = Camera::get().GetModelView() * mat4(tPoints[i].x, tPoints[i].y, tPoints[i].z) * sca;
                shader.SetModelView(trans);
                _sphere->Draw(shader, -1, true, true);
            }
        }
    }
}
