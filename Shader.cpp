#include "Util.h"
#include "Shader.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cstring>
#include "glInfo.h"

bool	Shader::s_bInitialized = false;
Shader*	Shader::s_pCurrent = NULL;

const int MAX_LOG_STRING = 1024;
char logstring[MAX_LOG_STRING];

const std::vector<std::string> DefUniforms =
{
    {LIGHT_POSITION_UNIFORM},
    {"LightAmbient"},
    {"LightDiffuse"},
    {"LightSpecular"},
    {"LightSpotDirection"},
    {"TextureMatrix"},
};

GLuint Shader::loadShader(const std::string &filestr)
{
    bool vertexshader = false;
    bool fragmentshader = false;
    bool geometryshader = false;

    if (filestr.find(".vert") != std::string::npos)
    {
        vertexshader = true;
    }
    else
    {
        if (filestr.find(".frag") != std::string::npos)
        {
            fragmentshader = true;
        }
#ifdef USE_GEOMETRY_SHADER
        else
        {
            if (filestr.find(".geo") != std::string::npos)
            {
                geometryshader = true;
            }
        }
#endif
    }

    if (!vertexshader && !fragmentshader && !geometryshader )
        return 0;

    std::string s;
    if (Util::ReadAllFile(filestr, s) < 0)
        return 0;

    GLchar * source = new GLchar[4 * (s.size() / 4 + 1)];
    if (source == 0)
        return 0;

    unsigned int i;
    for (i = 0; i < s.size(); ++i)
    {
        source[i] = s[i];
    }
    source[i] = '\0';

    GLuint so = 0;
    if (vertexshader)
        GL_CHECK(so = glCreateShader(GL_VERTEX_SHADER));
    if (fragmentshader)
        GL_CHECK(so = glCreateShader(GL_FRAGMENT_SHADER));
#ifdef USE_GEOMETRY_SHADER
    if (geometryshader)
        GL_CHECK(so = glCreateShader(GL_GEOMETRY_SHADER));
#endif
    GL_CHECK(glShaderSource(so, 1, (const GLchar**) &source, 0));

    delete [] source;

    return so;
}

bool Shader::compileShader(GLuint object, const std::string &name)
{
    if (object == 0)
        return false;

    GL_CHECK(glCompileShader(object));

    int status;
    GL_CHECK(glGetShaderiv(object, GL_COMPILE_STATUS, &status));
    if (status == 0)
    {
        int length = 0;
        GL_CHECK(glGetShaderiv(object, GL_INFO_LOG_LENGTH, &length));
        if (length > 0)
        {
            GLsizei minlength = std::min(MAX_LOG_STRING, length);
            GL_CHECK(glGetShaderInfoLog(object, minlength, 0, logstring));
            LOG_INFO("%s: %s", name.c_str(), logstring);
            exit(1);
        }
        return false;
    }
    return true;
}

GLuint Shader::linkShaders(GLuint *object, const unsigned int &nb, const std::string &name)
{
    if (object == 0)
        return 0;

    GLuint po = 0;
    GL_CHECK(po = glCreateProgram());

    for (unsigned int i = 0; i < nb; ++i)
    {
        if (object[i] > 0)
            GL_CHECK(glAttachShader(po, object[i]));
    }

    GL_CHECK(glLinkProgram(po));

    int status;
    GL_CHECK(glGetProgramiv(po, GL_LINK_STATUS, &status));
    if (status == 0)
    {
        int length = 0;
        GL_CHECK(glGetProgramiv(po, GL_INFO_LOG_LENGTH, &length));
        if (length > 0)
        {
            GLsizei minlength = std::min(MAX_LOG_STRING, length);
            GL_CHECK(glGetProgramInfoLog(po, minlength, 0, logstring));
            LOG_WARN("%s: %s", name.c_str(), logstring);
            exit(1);
        }
        return 0;
    }
    return po;
}

/*
void Shader::PrintSupportedExtensions()
{
    std::stringstream extensions = std::stringstream((char*)(glGetString(GL_EXTENSIONS)));
    std::cout << "Supported extensions :" << std::endl;
    while(!extensions.eof())
    {
        std::string str("end");
        extensions >> str;
        std::cout << "- " << str << std::endl;
    }
}

bool Shader::isExtensionSupported(const std::string &ext)
{
    std::string extensions = std::string((char*)(glGetString(GL_EXTENSIONS)));
    if(extensions.find(ext) != std::string::npos)
        return true;
    std::cerr << "WARNING : Extension \"" << ext << "\" is not supported" << std::endl;
    return false;
}
*/

bool Shader::Init()
{
    s_bInitialized = true;
    s_pCurrent = NULL;

#ifndef HAVE_GLES
    if (! glInfo::get().isExtensionSupported("GL_ARB_shading_language_100") )
        s_bInitialized = false;
#endif // HAVE_GLES

    return s_bInitialized;
}


bool Shader::Load(const std::string &name, const std::string &extensions)
{
    if (!s_bInitialized)
    {
        if (!Init())
        {
            LOG_WARN("Shader Init() failed");
            return false;
        }
    }

    if (!extensions.empty())
    {
        std::stringstream ss( extensions );
        std::string it;
        while (std::getline(ss, it, ' '))
        {
            glInfo::get().isExtensionSupported(it);
        }
    }

    m_strName = name;

    GLuint so[3];
    memset(so, 0, sizeof(GLuint) * 3);

    std::string s1 = name + ".vert";
    so[0] = loadShader(s1);
    if (so[0] == 0)
        LOG_WARN("WARN: loading shader %s failed", s1.c_str());
    if (so[0] && !compileShader(so[0], s1))
    {
        LOG_WARN("ERROR: compiling shader failed (exiting...)", s1.c_str());
        return false;
    }

    std::string s2 = name + ".frag";
    so[1] = loadShader(s2);
    if (so[1] == 0)
    {
        LOG_WARN("WARN: loading shader failed\n", s2.c_str());
    }
    if (so[1] && !compileShader(so[1], s2))
    {
        LOG_WARN("ERROR: compiling shader %s failed (exiting...)\n", s2.c_str());
        return false;
    }

#ifdef USE_GEOMETRY_SHADER
    std::string s3 = name + ".geo";
    so[2] = loadShader(s3);
    if (so[2] == 0)
        LOG_WARN("WARN: loading shader %s failed", s3.c_str());
    if (so[2] && !compileShader(so[2]))
    {
        LOG_WARN("ERROR: compiling shader %s failed (exiting...)", s3.c_str());
        return false;
    }
#endif

    m_nProgram = linkShaders(so, 3, name);
    if (so[0])
        GL_CHECK(glDeleteShader(so[0]));
    if (so[1])
        GL_CHECK(glDeleteShader(so[1]));
    if (so[2])
        GL_CHECK(glDeleteShader(so[1]));

    _vertexPos = GetAttribLocation(VERTEX_ATTRIB_NAME);
    _normalPos = GetAttribLocation(NORMAL_ATTRIB_NAME);
    _tangentPos = GetAttribLocation(TANGENT_ATTRIB_NAME);
    _colorPos = GetAttribLocation(COLOR_ATTRIB_NAME);
    _texcoordPos = GetAttribLocation(TEXCOORD_ATTRIB_NAME);

    _normalMatrixPos = GetUniformLocation(NORMAL_MATRIX_UNIFORM);
    for (auto &unif : DefUniforms)
    {
        if (GetUniformLocation(unif) >= 0)
            LOG_INFO("%s: %s -> %d", m_strName.c_str(), unif.c_str(), _locations[unif]);
    }

    assert(_vertexPos >= 0);
    LOG_INFO("%s: vertexPos:%d normalPos:%d tangentPos:%d colorPos:%d texCoordPos:%d",
        m_strName.c_str(), _vertexPos, _normalPos, _tangentPos, _colorPos, _texcoordPos);

    return true;
}


bool Shader::Activate(const mat4& modelView, const mat4 &projection)
{
    if (!m_nProgram)
    {
        LOG_WARN("Cannot load %s shader", m_strName.c_str());
        return false;
    }

    s_pCurrent = this;
    GL_CHECK(glUseProgram(m_nProgram));

    SetTexActive(false);
    SetTexInv(true);

    SetModelView(modelView);
    SetProjection(projection);

    return true;
}

void Shader::Deactivate()
{
    s_pCurrent = NULL;
    GL_CHECK(glUseProgram(0));
}

// Le shader doit �tre activ� avant
void Shader::UniformTexture(const std::string &ext, GLint slot)
{
    GL_CHECK(glActiveTexture(GL_TEXTURE0 + slot));
//	if(pTex) pTex->Bind(slot);
    GLint id = GetUniformLocation(ext);
    if (Util::get().LastGLError())
        LOG_WARN("%s: Could not find uniform texture location %s. slot: %d. id(%d)", m_strName.c_str(), ext.c_str(), slot, id);
    GL_CHECK(glUniform1i(id, slot));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

// Le shader doit �tre activ� avant
void Shader::Uniform(const std::string &ext, GLint value)
{
    GLint id = GetUniformLocation(ext);
    if (Util::get().LastGLError() || id < 0)
        return;
    GL_CHECK(glUniform1i(id, value));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

// Le shader doit �tre activ� avant
void Shader::Uniform(const std::string &ext, GLfloat value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniform1f(id, value));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

// Le shader doit �tre activ� avant
void Shader::Uniform(const std::string &ext, const vec2 &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniform2fv(id, 1, value));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

void Shader::Uniform(const std::string &ext, const glm::vec2 &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniform2fv(id, 1, glm::value_ptr(value)));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

void Shader::Uniform(const std::string &ext, const vec3 &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniform3fv(id, 1, value));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

void Shader::Uniform(const std::string &ext, const glm::vec3 &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniform3fv(id, 1, glm::value_ptr(value)));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

void Shader::Uniform(const std::string &ext, const vec4 &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniform4fv(id, 1, value));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

void Shader::Uniform(const std::string &ext, const glm::vec4 &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniform4fv(id, 1, glm::value_ptr(value)));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

void Shader::Uniform(const std::string &ext, const mat3 &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniformMatrix3fv(id, 1, GL_FALSE, value.mat));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

void Shader::Uniform(const std::string &ext, const mat4 &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniformMatrix4fv(id, 1, GL_FALSE, value.mat));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

void Shader::Uniform(const std::string &ext, const glm::mat4 &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniformMatrix4fv(id, 1, GL_FALSE, glm::value_ptr(value)));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

void Shader::Uniform(const std::string &ext, const bool &value)
{
    GLint id = GetUniformLocation(ext);
    if (id < 0)
        return;
    GL_CHECK(glUniform1i(id, value));
    if (Util::get().LastGLError() != GL_NO_ERROR)
        LOG_WARN("%s: Error setting uniform %s", m_strName.c_str(), ext.c_str());
}

GLint Shader::GetUniformLocation(const std::string &ext)
{
    auto search = _locations.find(ext);
    if (search != _locations.end())
        return search->second;

    GLint id = 0;
    GL_CHECK(id = glGetUniformLocation(m_nProgram, ext.c_str()));
    if (id < 0)
        LOG_WARN("%s: Could not find uniform location %s. id(%d)", m_strName.c_str(), ext.c_str(), id);
    _locations[ext] = id;
    return id;
}

GLint Shader::GetAttribLocation(const std::string &ext)
{
    auto search = _locations.find(ext);
    if (search != _locations.end())
        return search->second;
    GLint id = 0;
    GL_CHECK(id = glGetAttribLocation(m_nProgram, ext.c_str()));
    if (id < 0)
        LOG_WARN("%s: Could not find attrib location %s. id(%d)", m_strName.c_str(), ext.c_str(), id);
    _locations[ext] = id;
    return id;
}

void Shader::SetTexActive(bool val, int slot)
{
    if (val != _texactive || !_texactiveSet)
    {
        Uniform(TEX_UNIFORM, slot);
        Uniform(TEXACTIVE_UNIFORM, val);

        _texactive = val;
        _tex = slot;
        _texactiveSet = true;
    }
}

void Shader::SetTexInv(bool val, int slot)
{
    if (val != _texinv || !_texinvSet)
    {
        Uniform(TEX_UNIFORM, slot);
        Uniform(TEXINV_UNIFORM, val);
        _texinv = val;
        _texinvSet = true;
    }
}

void Shader::SetModelView(const mat4 &modelView)
{
    Uniform(MODEL_VIEW_UNIFORM, modelView);
    this->modelView = modelView;
    SetNormalMatrix(modelView);
}

void Shader::SetNormalMatrix(const mat4 &modelView)
{
    if (_normalMatrixPos >= 0)
    {
        mat4 normalMatrix;
        modelView.inverse(normalMatrix);
        Uniform(NORMAL_MATRIX_UNIFORM, mat3(normalMatrix.transpose()));
    }
}

void Shader::SetProjection(const mat4 &projection)
{
    Uniform(PROJECTION_UNIFORM, projection);
    this->projection = projection;
}
