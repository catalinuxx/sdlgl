#include "Util.h"
#include "Timer.h"

#include <SDL.h>
#include "Util.h"

#define GETTIME() ((float)SDL_GetTicks()/1000)

Timer::Timer()
{
    Start();
}

void Timer::Start()
{
    fStartTime = GETTIME();
    fCurrentTime = 0.0f;
    fElapsedTime = 0.0f;
    nFPS = 0;
}

void Timer::Idle()
{
    static GLint TempFPS = 0;	// Temp FPS
    static GLfloat fPreviousFPSTime = 0.0f;

    GLfloat fNewCurrentTime = GETTIME() - fStartTime;

    fElapsedTime = fNewCurrentTime - fCurrentTime;

    ++TempFPS;

    // If a second passed
    if (fNewCurrentTime - fPreviousFPSTime > 1.0f)
    {
        fPreviousFPSTime = fNewCurrentTime;
        nFPS = TempFPS;
        TempFPS = 0;
    }

    fCurrentTime = fNewCurrentTime;
}
