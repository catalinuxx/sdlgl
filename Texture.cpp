#include "Util.h"
#include "Texture.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "ImageTools.h"

bool Texture::s_bGenerateMipmaps = false;

bool Texture::LoadFile(GLenum target, const std::string& name)
{
    unsigned int w, h, d;
    GLubyte* ptr = ImageTools::OpenImage(name, w, h, d);
    if (!ptr)
    {
        std::cerr << "[Error] Cannot load texture " << name << std::endl;
        return false;
    }

    _w = w;
    _h = h;
    _d = d;
    LoadData(target, ptr, w, h, d);

    delete[] ptr;
    return true;
}

void Texture::LoadData(GLenum target, GLubyte* ptr, unsigned int w, unsigned int h, unsigned int d)
{
    _w = w;
    _h = h;
    _d = d;
    GL_CHECK(glTexImage2D(target, 0, d == 3 ? GL_RGB : GL_RGBA, w, h, 0, d == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, ptr));
}

void Texture::Gen()
{
    if (m_nHandle)
        GL_CHECK(glDeleteTextures(1, &m_nHandle));
    GL_CHECK(glGenTextures(1, &m_nHandle));
}

bool Texture::Load(const std::string&)
{
    Clear();

    Gen();

    if (m_nHandle == 0)
    {
        LOG_WARN("Invalid texture id");
        return false;
    }

    return true;
}

Texture::~Texture()
{
    Clear();
}

void Texture::Clear()
{
    if (m_nHandle)
    {
        GL_CHECK(glDeleteTextures(1, &m_nHandle));
        m_nHandle = 0;
    }
}

void Texture::Bind() const
{
    if (!m_nHandle) return;
    GL_CHECK(glBindTexture(getTextureType(), m_nHandle));
}

void Texture::Bind(GLuint slot) const
{
    if (!m_nHandle) return;
    GL_CHECK(glActiveTexture(GL_TEXTURE0 + slot));
    //GL_CHECK(glEnable(getTextureType()));
    GL_CHECK(glBindTexture(getTextureType(), m_nHandle));
}

void Texture::Unbind() const
{
    GL_CHECK(glBindTexture(getTextureType(), 0));
}

void Texture::Unbind(GLuint slot) const
{
    GL_CHECK(glActiveTexture(GL_TEXTURE0 + slot));
    GL_CHECK(glBindTexture(getTextureType(), 0));
}
