#include "Util.h"
#include "VertexBufferObject.h"

VertexBufferObject::VertexBufferObject()
{
    m_nVBOid = 0;
}

VertexBufferObject::~VertexBufferObject()
{
    Clear();
}

void VertexBufferObject::Clear()
{
    if (m_nVBOid)
    {
        GL_CHECK(glDeleteBuffers(1, &m_nVBOid));
        if (Util::get().LastGLError())
            m_nVBOid = 0;
        m_nVBOid = 0;
    }

    m_tDataPosition.clear();
    m_tDataNormal.clear();
    m_tDataTexcoord.clear();
    m_tDataTangent.clear();
    m_tDataColor.clear();
}

/*
- GL_STREAM_DRAW
- GL_DYNAMIC_DRAW
- GL_STATIC_DRAW
*/
bool VertexBufferObject::Create(GLenum usage)
{
    assert(m_nVBOid == 0);
    m_nVBOid = 0;

    GLsizeiptr	nSizePosition = 0;
    GLsizeiptr	nSizeNormal = 0;
    GLsizeiptr	nSizeTexcoord = 0;
    GLsizeiptr	nSizeTangent = 0;
    GLsizeiptr	nSizeColor = 0;

    if (!m_tDataPosition.empty())
    {
        nSizePosition = m_tDataPosition.size() * sizeof(vec3);
    }
    else
    {
        std::cerr << "[ERROR] No position data !" << std::endl;
        return false;
    }

    if (!m_tDataNormal.empty())
    {
        nSizeNormal	= m_tDataNormal.size() * sizeof(vec3);
    }

    if (!m_tDataTexcoord.empty())
    {
        nSizeTexcoord = m_tDataTexcoord.size() * sizeof(vec2);
    }

    if (!m_tDataTangent.empty())
    {
        nSizeTangent = m_tDataTangent.size() * sizeof(vec3);
    }

    if (!m_tDataColor.empty())
    {
        nSizeColor = m_tDataColor.size() * sizeof(vec4);
    }

    m_nVBO_OffsetPosition	= 0;
    m_nVBO_OffsetNormal		= m_nVBO_OffsetPosition + nSizePosition;
    m_nVBO_OffsetTexcoord	= m_nVBO_OffsetNormal + nSizeNormal;
    m_nVBO_OffsetTangent	= m_nVBO_OffsetTexcoord + nSizeTexcoord;
    m_nVBO_OffsetColor  	= m_nVBO_OffsetTangent + nSizeTangent;

    GL_CHECK(glGenBuffers(1, &m_nVBOid));
    if (m_nVBOid == 0)
    {
        std::cerr << "[ERROR] Init VBO failed !" << std::endl;
        return false;
    }

    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_nVBOid));

    //usage = GL_DYNAMIC_DRAW;

    GL_CHECK(glBufferData(GL_ARRAY_BUFFER, nSizePosition + nSizeNormal + nSizeTexcoord + nSizeTangent + nSizeColor, 0, usage));

    GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, m_nVBO_OffsetPosition,	nSizePosition,	(const GLvoid*)(&m_tDataPosition[0])));

    if (m_tDataNormal.size())
        GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, m_nVBO_OffsetNormal,	nSizeNormal,	(const GLvoid*)(&m_tDataNormal[0])));

    if (m_tDataTexcoord.size())
        GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, m_nVBO_OffsetTexcoord,	nSizeTexcoord,	(const GLvoid*)(&m_tDataTexcoord[0])));

    if (m_tDataTangent.size())
        GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, m_nVBO_OffsetTangent,	nSizeTangent,	(const GLvoid*)(&m_tDataTangent[0])));

    if (m_tDataColor.size())
        GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, m_nVBO_OffsetColor,	nSizeColor,	(const GLvoid*)(&m_tDataColor[0])));

    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));

    ComputeBoundingBox();

    return true;
}

void VertexBufferObject::BufferPosition()
{
    GLsizeiptr nSizePosition = m_tDataPosition.size() * sizeof(vec3);
    GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, m_nVBO_OffsetPosition,	nSizePosition,	(const GLvoid*)(&m_tDataPosition[0])));
    ComputeBoundingBox();
}

void VertexBufferObject::BufferTexCoord()
{
    GLsizeiptr nSizeTexCoord = m_tDataTexcoord.size() * sizeof(vec2);
    GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, m_nVBO_OffsetTexcoord,	nSizeTexCoord,	(const GLvoid*)(&m_tDataTexcoord[0])));
}

void VertexBufferObject::BufferColor()
{
    GLsizeiptr nSizeColor = m_tDataColor.size() * sizeof(vec4);
    GL_CHECK(glBufferSubData(GL_ARRAY_BUFFER, m_nVBO_OffsetColor,	nSizeColor,	(const GLvoid*)(&m_tDataColor[0])));
}

void VertexBufferObject::Enable(Shader &shader)
{
    if (m_nVBOid)	Enable_VBO(shader);		// avec Vertex Buffer Object
    else			Enable_VA();		// avec Vertex Array
}

void VertexBufferObject::Disable(Shader &shader)
{
    if (m_nVBOid)	Disable_VBO(shader);		// avec Vertex Buffer Object
    else			Disable_VA();		// avec Vertex Array
}

void VertexBufferObject::Enable_VA()
{
#ifndef HAVE_GLES
    unsigned int slot = 0;
    GL_CHECK(glEnableClientState(GL_VERTEX_ARRAY));
    GL_CHECK(glVertexPointer(3, GL_FLOAT, 0, &(m_tDataPosition[0].x)));

    if (!m_tDataNormal.empty())
    {
        GL_CHECK(glEnableClientState(GL_NORMAL_ARRAY));
        GL_CHECK(glNormalPointer(GL_FLOAT, 0, &(m_tDataNormal[0].x)));
    }

    if (!m_tDataTexcoord.empty())
    {
        GL_CHECK(glClientActiveTexture(GL_TEXTURE0 + slot++));
        GL_CHECK(glEnableClientState(GL_TEXTURE_COORD_ARRAY));
        GL_CHECK(glTexCoordPointer(2, GL_FLOAT, 0, &(m_tDataTexcoord[0].s)));
    }

    if (!m_tDataTangent.empty())
    {
        GL_CHECK(glClientActiveTexture(GL_TEXTURE0 + slot++));
        GL_CHECK(glEnableClientState(GL_TEXTURE_COORD_ARRAY));
        GL_CHECK(glTexCoordPointer(3, GL_FLOAT, 0, &(m_tDataTangent[0].x)));
    }
#endif // HAVE_GLES
}

void VertexBufferObject::Enable_VBO(Shader &shader)
{
    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_nVBOid));

    // enable and send vertex position attribute data
    GL_CHECK(glEnableVertexAttribArray(shader.GetVertexPos()));
    GL_CHECK(glVertexAttribPointer(shader.GetVertexPos(), 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)m_nVBO_OffsetPosition));
    if (!m_tDataNormal.empty() && shader.HasNormal())
    {
        GL_CHECK(glEnableVertexAttribArray(shader.GetNormalPos()));
        GL_CHECK(glVertexAttribPointer(shader.GetNormalPos(), 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)m_nVBO_OffsetNormal));
    }
    if (!m_tDataTangent.empty() && shader.HasTangent())
    {
        GL_CHECK(glEnableVertexAttribArray(shader.GetTangentPos()));
        GL_CHECK(glVertexAttribPointer(shader.GetTangentPos(), 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)m_nVBO_OffsetTangent));
    }
    if (!m_tDataColor.empty() && shader.HasColor())
    {
        GL_CHECK(glEnableVertexAttribArray(shader.GetColorPos()));
        GL_CHECK(glVertexAttribPointer(shader.GetColorPos(), 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)m_nVBO_OffsetColor));
    }
    if (!m_tDataTexcoord.empty() && shader.HasTexCoord())
    {
        GL_CHECK(glEnableVertexAttribArray(shader.GetTexCoordPos()));
        GL_CHECK(glVertexAttribPointer(shader.GetTexCoordPos(), 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)m_nVBO_OffsetTexcoord));
    }
}

void VertexBufferObject::Disable_VA()
{
#ifndef HAVE_GLES
    unsigned int slot = 0;

    if (!m_tDataTexcoord.empty())
    {
        GL_CHECK(glClientActiveTexture(GL_TEXTURE0 + slot++));
        GL_CHECK(glDisableClientState(GL_TEXTURE_COORD_ARRAY));
    }

    if (!m_tDataTangent.empty())
    {
        GL_CHECK(glClientActiveTexture(GL_TEXTURE0 + slot++));
        GL_CHECK(glDisableClientState(GL_TEXTURE_COORD_ARRAY));
    }

    if (!m_tDataNormal.empty())
    {
        GL_CHECK(glDisableClientState(GL_NORMAL_ARRAY));
    }

    GL_CHECK(glDisableClientState(GL_VERTEX_ARRAY));
#endif // HAVE_GLES
}

void VertexBufferObject::Disable_VBO(Shader &shader)
{
    if (!m_tDataNormal.empty() && shader.HasNormal())
        GL_CHECK(glDisableVertexAttribArray(shader.GetNormalPos()));
    if (!m_tDataTangent.empty() && shader.HasTangent())
        GL_CHECK(glDisableVertexAttribArray(shader.GetTangentPos()));
    if (!m_tDataTexcoord.empty() && shader.HasTexCoord())
        GL_CHECK(glDisableVertexAttribArray(shader.GetTexCoordPos()));
    if (!m_tDataColor.empty() && shader.HasColor())
        GL_CHECK(glDisableVertexAttribArray(shader.GetColorPos()));
    GL_CHECK(glDisableVertexAttribArray(shader.GetVertexPos()));

    GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, 0));
}

void VertexBufferObject::ComputeBoundingBox()
{
    _BBox.min = vec3(100000.0f, 100000.0f, 100000.0f);
    _BBox.max = vec3(-100000.0f, -100000.0f, -100000.0f);
    for (int i = 0; i < (int)m_tDataPosition.size(); i++)
        _BBox.Add(m_tDataPosition[i]);

    if (_haveBBoxVbos)
        CreateBBoxVBOs(true);
}

void VertexBufferObject::CreateBBoxVBOs(bool update)
{
    if (!update && _bboxVbo1 && _bboxVbo2 && _bboxVbo3) // No need to recreate
        return;

    _haveBBoxVbos = true;

    if (!_bboxVbo1)
        _bboxVbo1.reset(new VertexBufferObject());
    if (!_bboxVbo2)
        _bboxVbo2.reset(new VertexBufferObject());
    if (!_bboxVbo3)
        _bboxVbo3.reset(new VertexBufferObject());

    if (_bboxVbo1)
    {
        _bboxVbo1->getPosition() =
        {
            {_BBox.min.x, _BBox.min.y, _BBox.min.z},
            {_BBox.max.x, _BBox.min.y, _BBox.min.z},
            {_BBox.max.x, _BBox.min.y, _BBox.max.z},
            {_BBox.min.x, _BBox.min.y, _BBox.max.z},
        };
        if (!update)
            _bboxVbo1->Create(GL_DYNAMIC_DRAW);
        else
            _bboxVbo1->BufferPosition();
    }
    if (_bboxVbo2)
    {
        _bboxVbo2->getPosition() =
        {
            {_BBox.min.x, _BBox.max.y, _BBox.min.z},
            {_BBox.max.x, _BBox.max.y, _BBox.min.z},
            {_BBox.max.x, _BBox.max.y, _BBox.max.z},
            {_BBox.min.x, _BBox.max.y, _BBox.max.z},
        };
        if (!update)
            _bboxVbo2->Create(GL_DYNAMIC_DRAW);
        else
            _bboxVbo2->BufferPosition();
    }
    if (_bboxVbo3)
    {
        _bboxVbo3->getPosition() =
        {
            {_BBox.min.x, _BBox.min.y, _BBox.min.z},
            {_BBox.min.x, _BBox.max.y, _BBox.min.z},
            {_BBox.max.x, _BBox.min.y, _BBox.min.z},
            {_BBox.max.x, _BBox.max.y, _BBox.min.z},
            {_BBox.max.x, _BBox.min.y, _BBox.max.z},
            {_BBox.max.x, _BBox.max.y, _BBox.max.z},
            {_BBox.min.x, _BBox.min.y, _BBox.max.z},
            {_BBox.min.x, _BBox.max.y, _BBox.max.z},
        };
        if (!update)
            _bboxVbo3->Create(GL_DYNAMIC_DRAW);
        else
            _bboxVbo3->BufferPosition();
    }
}

void VertexBufferObject::DrawBBox(Shader &shader)
{
    shader.SetMatDiffuse(vec4(1,0,0,1));
    if (_bboxVbo1)
    {
        _bboxVbo1->Enable(shader);
        GL_CHECK(glDrawArrays(GL_LINE_LOOP, 0, 4));
        _bboxVbo1->Disable(shader);
    }
    if (_bboxVbo2)
    {
        _bboxVbo2->Enable(shader);
        GL_CHECK(glDrawArrays(GL_LINE_LOOP, 0, 4));
        _bboxVbo2->Disable(shader);
    }
    if (_bboxVbo3)
    {
        _bboxVbo3->Enable(shader);
        GL_CHECK(glDrawArrays(GL_LINES, 0, 8));
        _bboxVbo3->Disable(shader);
    }
}
