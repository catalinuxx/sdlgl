#pragma once

template <class T>
class Singleton
{
public :
    inline static T& get()
    {
        static T instance;
        return instance;
    }

protected :
    Singleton() {}

private :
    /* Explicitly disallow copying. */
    Singleton(const Singleton&) = delete;
    Singleton& operator= (const Singleton&) = delete;
};
